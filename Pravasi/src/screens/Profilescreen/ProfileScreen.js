import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
    TextInput,
  FlatList,
  Button,
  TouchableOpacity,
  SafeAreaView,
  ScrollView
} from "react-native";
import AppHeader from '../../components/AppHeader'
import ContactInformationProfile from '../../components/ContactInformationProfile'
import MainBottom from '../../components/MainBottom'

class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <SafeAreaView style={styles.complete_view}>
            <ScrollView>
                <AppHeader {...this.props} />

                <View style={styles.headinView}>
                    <Text style={styles.txt_profile}>Nidheesh Kommath </Text>
                    <Text style={styles.txt_profile_short}>@nidheeshkommath</Text>
                </View>


                <View style={styles.profile_top}>
                    <View style={styles.image_circle_view}>
                        <Image
                            style={styles.image_circle}
                            source={require('../../assest/Icons/Profile/img_avatar.png')}
                        />
                    </View>
                    
                    <View style={styles.insta_view}>
                        <View style={styles.insta_view_row1}>
                            <Text style={styles.row1_txt}>150</Text>
                            <Text style={styles.row1_txt}>165</Text>
                            <Text style={styles.row1_txt}>190</Text>
                        </View>
                        <View style={styles.insta_view_row2}>
                            <Text style={styles.row2_txt}>Points</Text>
                            <Text style={styles.row2_txt}>Followers</Text>
                            <Text style={styles.row2_txt}>Following</Text>
                        </View>
                        <View style={styles.insta_view_row3}>
                            <View style={styles.follow_view}>
                                <Text style={styles.follow_txt}>Follow</Text></View>
                            <View style={styles.message_view}>
                                <Text style={styles.message_txt}>Message</Text></View>
                        </View>

                    </View>                    
                </View>


                <View style={styles.informatn_view}>
                    <Text style={styles.txt_infomatn}>User Information </Text>
                </View>

                <View style={styles.short_para}>
                    <Text style={styles.txt_2}>Lorem ipsum is a name for a common type of placeholder text. Also known as filler or dummy text, this is simply copy that serves to fill a space without actually saying anything meaningful. It's essentially nonsense text, but gives an idea of what real words will look like in the final product.</Text>
                </View>

                <View style={styles.informatn_view}>
                    <Text style={styles.txt_infomatn}>Contact Information </Text>
                </View>

                <ContactInformationProfile />


            </ScrollView>
            <MainBottom {...this.props} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

    complete_view: {
        flex: 1,
        backgroundColor: "white",
    },

    headinView: {
        paddingHorizontal: "10%",
        marginTop: 10,
        marginBottom: 20,
    },

    txt_profile: {
        fontWeight: "bold",
        fontSize: 18,
        color: "black"
    },

    txt_profile_short: {
        fontSize: 10,
        color:"grey"
    },

    profile_top: {
        
        // backgroundColor:"red",
        flexDirection: 'row',
        justifyContent:"space-between",
        paddingHorizontal: "10%",
        marginBottom:20
    },

    image_circle_view: {
        flex: 1,
        flexDirection:"column",
        paddingHorizontal: "5%",
        // backgroundColor:"green"
    },

    image_circle: {
        height: 70,
        width: 70,
        borderRadius: 100,   
    },

    insta_view: {
        flex: 2.5,
        flexDirection: "column",
        // backgroundColor:"white"
    },

    insta_view_row1: {
        flexDirection: "row",
        justifyContent: "space-around",
    },

    row1_txt: {
        fontWeight:"bold"
    },

    insta_view_row2: {
        flexDirection: "row",
        justifyContent: "space-around",
        // backgroundColor:"blue"
    },

    row2_txt: {
        fontSize: 12,
        color: "grey",
        marginLeft:15   
    },

    insta_view_row3: {
        flexDirection: "row",
        justifyContent: "space-around",
        // backgroundColor: "orange",
        top:10
    },

    follow_view: {
        flexDirection: 'column',
        height: 25,
        width: 80,
        borderRadius: 50,
        backgroundColor: "#070782",
        justifyContent: "center" 
    },
    
    follow_txt: {
        fontSize: 10,
        color: "white",
        fontWeight:"600",
        flexDirection: "column",
        justifyContent: "center",
        alignSelf: "center"
    },


    message_view: {
        flexDirection: "column",
        height: 25,
        width: 80,
        borderRadius: 50,
        backgroundColor: "#070782",
        justifyContent: "center" 
    },

    message_txt: {
        fontSize: 10,
        fontWeight:"600",
        color:"white",
        flexDirection: "column",
        justifyContent: "center",
        alignSelf: "center"
    },

    informatn_view: {
        marginTop: 20,
        paddingHorizontal: "10%",
    },

    txt_infomatn: {
        fontWeight: "bold",
        fontSize: 15,
        color: "black"
    },

    short_para: {
        alignItems: "center",
        justifyContent: "center",
        marginTop: 5, 
    },

    txt_2: {
        paddingHorizontal: "10%",
        paddingVertical: 20,
        color: "grey",
        lineHeight:20,
        fontSize: 12,
        justifyContent: "center",
        alignSelf:"center"
    },

})

export default ProfileScreen;
