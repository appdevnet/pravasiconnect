import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Platform,
  Dimensions,
  AsyncStorage,
  FlatList,
  Alert
} from "react-native";
import axios from 'react-native-axios';
import AppHeader from '../../components/AppHeader'
import ImagePicker from 'react-native-image-picker';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import img_icon from '../../assest/iconss/img_icon.png';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

import AwesomeAlert from 'react-native-awesome-alerts';

import {connect} from 'react-redux';
import { setLoginData,startLoader,stopLoader,SetUserId,SetToken,setLocation } from '../../redux/action'

const options = {
  title: 'Select Avatar',
  maxHeight:windowHeight,
  maxWidth:windowHeight,
  quality	:1,
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class CreatePostImageScreen extends Component {
  constructor(props) {
    super(props);
      this.state = {
        showAlert:"",
        primaryImage:"",
        image2:"",
        image3:"",
        image4:"",
        file:"",
          filepath: {
        data: '',
        path: '',
        type:'',
        filename:'',
      },
      fileData: '',
      fileUri: ''
        
    };
    }  
    
    componentDidMount=()=>{
      console.log(this.props.route?.params?.data,"create post params values")
    }
    
    OpenPicker=(type)=>
   {
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = { uri: response.uri };

        console.log("imagefile",response.assets[0].uri)
        this.setState({
          path:response.assets[0].uri,
          type:response.assets[0].type,
          filename:response.assets[0].fileName
        })
        imageData={
          uri:response.assets[0].uri,
          type:response.assets[0].type,
          name:response.assets[0].fileName
        }
        if(type=="primary"){
          this.setState({
            primaryImage:imageData
          })
        }
        else if(type=="image2"){
          this.setState({
            image2:imageData
          })
        }
        else if(type=="image3"){
          this.setState({
            image3:imageData
          })
        }
        else{
          this.setState({
            image4:imageData
          })
        }
        this.setState();
      }
    });
  }
  submitPost(){
    data = this.props.route.params.data /* Data from create post */
    
    console.log("primaryimg",this.state.primaryImage);
    console.log("image2",[this.state.image2,this.state.image3,this.state.image4]);
    //console.log("image3",this.state.image3);
    //console.log("image4",this.state.image4);

    this.props.startLoader()
    const url="https://pravasiconnect.websitepreview.in/api/add-post"

    const Data= new FormData();

    Data.append('latitude', this.props.route.params.data.latitude)
    Data.append('longitude', this.props.route.params.data.longitude)
    Data.append('category_id', this.props.route.params.data.category)
    Data.append('user_id', this.props.userID)
    Data.append('title', this.props.route.params.data.post_title)
    Data.append('description', this.props.route.params.data.post_description)
    Data.append('Mobile', this.props.route.params.data.contact_number)
    Data.append('Website_link',this.props.route.params.data.website)
    Data.append('image', this.state.primaryImage)

    Data.append('secondary_images[]',[this.state.image2,this.state.image3,this.state.image4])
    
    console.log(Data)

    fetch(url, {
      method: 'POST',
      headers: {
       Accept: 'application/json',
       Authorization: 'Bearer ' + this.props.Token,
       'Content-Type': 'multipart/form-data',
       enctype: 'multipart/form-data',
      },
      body:Data,
      }) 
      .then((response) => response.json())
      .then((response) => {
        this.props.stopLoader()
        console.log(response,"image upload responseeee");

         if(response.status==true)
         {
           this.setState({
             message:response.data.message
           })
           this.props.navigation.navigate('thankyouscreen')

         }
         else
         {
           
          this.setState({showAlert:true})
         }


      })
      .catch((error) => {
        console.log(error);
      });


  }


  render() {
    return (
        <SafeAreaView style={[styles.complete_view]}>
            <AppHeader isNotify={false} {...this.props} />
            
          
                <View style={styles.headinView}>
          <Text style={styles.headnTxt}>Upload Images</Text>
                </View>

                <View style={[styles.container,]}>
                
                  
                    <View style={[styles.ImageOneView,]}>
                      {this.state.primaryImage!=""?(
                        <TouchableOpacity activeOpacity={0.5} onPress={()=> this.OpenPicker("primary")} >
                        <Image resizeMode="cover" source={{uri:this.state.primaryImage.uri}} style={styles.imageOne} />
                        </TouchableOpacity>
                      ):(
                        <>
                        <Image source={img_icon} style={{justifyContent:"center",alignSelf:"center",marginVertical: 10,}}
                        />

                        <Text style={{ justifyContent: "center", alignSelf: "center" }}>Choose an Image</Text>
                        <TouchableOpacity style={styles.BtnStyle} onPress={()=> this.OpenPicker("primary")}>
                        <Text style={styles.BtnText}>UPLOAD IMAGE</Text>
                        </TouchableOpacity> 
                        </> 
                      )}                
                    </View>
                    
                </View>

                <View style={styles.headinView}>
          <Text style={styles.headnTxt}>Upload Secondary Images</Text>
                </View>

                <View style={{ flexDirection: "row",width:"100%", height:"20%",}}>
                  
                <TouchableOpacity style={styles.secondaryImageView} onPress={()=> this.OpenPicker("image2")}>
                  {
                    this.state.image2!=""?(
                      <Image resizeMode="cover" source={{uri:this.state.image2.uri}} style={styles.imageOne} /> 
                    ):(
                      <Image source={img_icon} style={styles.secondaryDefaultImage}
                      />
                    )
                  } 
                    </TouchableOpacity>
                <TouchableOpacity style={styles.secondaryImageView} onPress={()=> this.OpenPicker("image3")}>
                  {
                    this.state.image3!=""?(
                      <Image resizeMode="cover" source={{uri:this.state.image3.uri}} style={styles.imageOne} /> 
                    ):(
                      <Image source={img_icon} style={styles.secondaryDefaultImage}
                      />
                    )
                  } 
                    </TouchableOpacity>
                <TouchableOpacity style={styles.secondaryImageView} onPress={()=> this.OpenPicker("image4")}>
                  {
                    this.state.image4!=""?(
                      <Image resizeMode="cover" source={{uri:this.state.image4.uri}} style={styles.imageOne} /> 
                    ):(
                      <Image source={img_icon} style={styles.secondaryDefaultImage}
                      />
                    )
                  } 
                    </TouchableOpacity>

            </View>

            <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title="Error!"
          message="Post Creation Failed"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          progressColor="#fff"
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="No, cancel"
          confirmText="OK"
          confirmButtonColor="#0D3369"
          onCancelPressed={() => {
            console.log('pattichee')
          }}
          onConfirmPressed={() => {
            this.setState({showAlert:false})
          }}
        />
            
            <TouchableOpacity style={{marginVertical:windowHeight/10.44}} onPress={()=>this.submitPost()}>
          <View style={styles.create_post_btn}>
            <Text style={styles.create_post_txt}>POST NOW</Text>
            </View>
          </TouchableOpacity>
         
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

    complete_view: {
        flex: 1,
        backgroundColor:"#fff"
    },
  
    headinView: {
        marginTop: windowHeight / 40,
        marginHorizontal: windowHeight / 20,
        
    },

    headnTxt: {
        fontWeight: "bold",
        fontSize: 20,
        color: "black"
    },

    container: {
      width: windowWidth,
      height: windowHeight/3,
      
    },

    ImageOneView: {
        width: "80%",
        height: "70%",
        justifyContent: "center",
        alignSelf: "center",
        marginVertical: '10%',
        backgroundColor: "#EBF1FF",
        borderColor: "grey",
        borderWidth: 0.2,
        borderRadius: 12,
        
    },

    imageOne: {
        width: "100%",
        height: "100%",
        justifyContent: "center",
        alignSelf:"center",
        borderWidth: 0.2,
        borderRadius: 12,

    },

    BtnStyle:{
      backgroundColor:'#2051AE',
      width:100,
      height: 30,
      marginTop: 10,
      justifyContent: "center",
      alignSelf:"center",
      borderRadius: 60,  
    },
    
    BtnText:{
      color:'#fff',
      fontSize:10,
      alignSelf:'center',
      fontWeight: 'bold',
      justifyContent:'center',
    },

    secondaryImageView: {
        width: "26%",
        height: "65%",
        justifyContent: "center",
        alignSelf:"center",
        marginLeft: "5%",
        backgroundColor: "#EBF1FF",
        borderWidth: 0.2,
        borderColor: "grey",
        borderRadius: 7,
    },

    imagetwo: {
        width: "100%",
        height: "100%",
        justifyContent: "center",
        alignSelf:"center",
        borderWidth: 0.2
    },

    secondaryDefaultImage: {
        justifyContent: "center",
        alignSelf: "center",
        marginVertical: 10,
        height: windowHeight / 40,
        width: windowWidth / 20

    },

    create_post_btn: {

      width: windowWidth/1,
      height: windowHeight/13,
      backgroundColor:"#070545",
      marginTop:windowHeight/20
  },

  create_post_txt: {
    fontSize: 20,
    fontWeight: "bold",
    alignSelf:"center",
    paddingVertical:windowHeight/45,
    color:"white"
  }

    
  
})

//export default CreatePostImageScreen;

const mapStateToProps = (state) => {
  return {
  isLoggedIn:state.global.isLoggedIn,
  location:state.global.location,
  loginData:state.global.loginData,
  userID:state.global.userID,
  Token:state.global.Token
  };
 };
  
 export default connect(mapStateToProps, {startLoader,stopLoader})(
  CreatePostImageScreen);
