import React, { Component } from 'react';  
import { StyleSheet, View,Dimensions,Image,TouchableOpacity,Text,Alert,AsyncStorage, Platform } from 'react-native';  
import MapView, { PROVIDER_DEFAULT, PROVIDER_GOOGLE } from 'react-native-maps';  
import { Marker } from 'react-native-maps';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'; 
import icon from '../../assest/Icons/Location/paper-plane.png'; 
import { ScrollView } from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import { setLocation,startLoader,stopLoader } from '../../redux/action'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import Toast from '../../components/Toast';
import Root from '../../components/Root';
import AwesomeAlert from 'react-native-awesome-alerts';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import Geolocation from '@react-native-community/geolocation';

 class MapScreen extends Component {  

  constructor(props) {
    super(props);
    this.state = {
      ready: false,  
      where: {lat:null, lng:null},  
      locate: {lat:10.850516, lng:76.271080},  
      error: null,
      Check:"",
      showAlert:false,


    };

   
  }

  componentDidMount=()=>{
    this.props.startLoader()
    Geolocation.getCurrentPosition(
			position => {
				const location = JSON.stringify(position);

				//this.setState({ location });
        this.setState({
          locate: {lat:position.coords.latitude, lng:position.coords.longitude},
          currentLat:position.coords.latitude,
          currentLong:position.coords.longitude,
        })

        this.props.stopLoader()

        //alert(location)
        console.log(location)
			},
			error => Alert.alert(error.message),
			{ enableHighAccuracy: true, timeout: 40000, maximumAge: 1000 }
		);
  }


  storeData = async () => {
    const data={latitude:this.state.locate.lat,longitude:this.state.locate.lng,place:this.state.Check}
    try {
      this.props.setLocation(data)
      await AsyncStorage.setItem('location',JSON.stringify(data))
    } catch (e) {
      // saving error
    }
  }



onRegionChange(region) {
    this.setState({ region });
  }

  MapCheck=()=>{
     //console.log(this.state.Check)
    if(this.state.Check=="")
    {
      {this.setState({
        showAlert:true
      })}
      
       //Alert.alert("Please Confirm Your Location")
        
    }
    else{
      //Alert.alert("You are in "+this.state.Check)
      this.storeData()
      this.props.navigation.navigate('abroadservice')
      
    
    
    }
  }
  render() {  
    return (  

      
      <View style={styles.MainContainer}>  

      
          <MapView  
            //mapType={Platform.OS == "android" ? "none" : "standard"}
            
          style={[styles.mapStyle,]}  
          provider={Platform.OS== "ios"?PROVIDER_DEFAULT:PROVIDER_GOOGLE}
          showsUserLocation={true}  
          onRegionChange={()=>this.onRegionChange()}
          zoomEnabled={true}  
          zoomControlEnabled={true}  
          scrollEnabled={true}
          region={{  
            latitude: this.state.locate.lat,   
            longitude: this.state.locate.lng,  
            latitudeDelta: 0.0922,  
            longitudeDelta: 0.0421,  
          }}>  
  
          <Marker  
            draggable={true}  
            coordinate={{ latitude: this.state.locate.lat, longitude: this.state.locate.lng }}  
            onDragEnd={e => {
              this.setState({
          locate:{
            lat:e.nativeEvent.coordinate.latitude,
            lng:e.nativeEvent.coordinate.longitude
          },
          // region:details.geometry.location
        });
              console.log('dragEnd', e.nativeEvent.coordinate);
            }}
            title={"location"}  
            description={"My Location"}   
          />  
        </MapView>  
        
       <View style={{height:windowHeight/1.55,marginTop:windowHeight/10,marginLeft:windowWidth/15,width:windowWidth/1.14,}}>
        <GooglePlacesAutocomplete
      //style={{marginLeft:windowWidth/15,width:windowWidth/1.14,color:'black',}}
      placeholder='Search Your Location'
      minLength={2} // minimum length of text to search
      autoFocus={false}
      textInputProps={'red'}
      returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      listViewDisplayed='auto'    // true/false/undefined
      fetchDetails={true}
      renderDescription={row => row.description} // custom description render
      onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
        console.log(data, details.geometry.location);
        this.setState({
          locate:details.geometry.location,
          region:details.geometry.location,
          Check:details.formatted_address
          
        });
      }}
      
      getDefaultValue={() => ''}

      getCurrentLocation={(value)=>{console.log(value,"loc")}}
      
      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyAt-VI6pfAJgUV7EarWa_vVN4S9T1bj8NU',
        language: 'en', // language of the results
        types: '(cities)' // default: 'geocode'
      }}
  
       //currentLocation={true}
       //currentLocationLabel='Current location'
      
      
      styles={{
        container: {
          flex: 1,
          
        },

        textInputContainer: {
          width: '80%',
          color:'#000',
          borderWidth:0.5,
          borderColor:'grey',
          alignSelf: 'center',
          justifyContent:'center',
          backgroundColor: '#F6F8FC',
        },
        textInput:{
          color:'#000',
          backgroundColor:'#F6F8FC',
          
        },

        description: {
          fontWeight: 'bold',
          color:'#000',
          
        },
        predefinedPlacesDescription: {
          color: '#1faadb'
        }
      }}
      
      // currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
      // currentLocationLabel="Current location"
      nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
      GoogleReverseGeocodingQuery={{
        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
      }}
      GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        rankby: 'distance',
        types: 'food'
      }}

      filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
       //predefinedPlaces={[homePlace, workPlace]}

       enablePoweredByContainer={false}

      debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
       renderRightButton={()  => <Image source={icon} style={{height:windowHeight/40,width:windowWidth/22,marginRight:windowHeight/43,marginLeft:windowWidth/35,marginTop:windowHeight/70}}/>}
    //   renderRightButton={() => <Text>Custom text after the input</Text>}

   
    />
    </View>
    <View style={{marginBottom:windowHeight/30,marginLeft:windowWidth/15,}}>
<TouchableOpacity style={[styles.locationBtn,]} onPress={()=> this.MapCheck()}>
        <Text style={styles.locationText}>SET YOUR LOCATION</Text>
      </TouchableOpacity>
</View>
<AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title="Oops"
          message="Please chooose a location"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          progressColor="#fff"
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="No, cancel"
          confirmText="submit"
          confirmButtonColor="#0D3369"
          onCancelPressed={() => {
            console.log('pattichee')
          }}
          onConfirmPressed={() => {
            this.setState({showAlert:false})
          }}
        />

          
      </View>  
      
      
    );  
  }  
}


// export default MapScreen;
  
const styles = StyleSheet.create({  
  MainContainer: {  
    position: 'absolute',  
    flex: 1, 
    alignItems: 'center',  
    justifyContent: 'center',  
  },  
  mapStyle: {  
    position: 'absolute',  
    top: 0, 
    flex: 1,
     height: windowHeight,
    width:windowWidth,
    left: 0,  
    right: 0,  
    bottom: 0,  
  }, 
  
  locationBtn: {
    width: windowWidth/1.5,
    borderRadius: 60,
    height: windowHeight/20,
    alignSelf: "center",
    justifyContent: "center",
    marginTop: windowHeight/15,
    backgroundColor: "#070545",
    
  },

  locationText: {
    justifyContent: "center",
    alignSelf: "center",
    fontSize: 17,
    fontWeight: "bold",
    color: "white"
  },
}); 
const mapStateToProps = (state) => {
  return {
  };
};

export default connect(mapStateToProps, {setLocation,startLoader,stopLoader})(
  MapScreen,
);