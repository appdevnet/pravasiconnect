import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Platform,
  Dimensions,
  AsyncStorage,
  FlatList,
  Alert
} from "react-native";
import axios from 'react-native-axios';
import AppHeader from '../../components/AppHeader'
import { connect } from 'react-redux';
import { setLoginData, startLoader, stopLoader } from '../../redux/action'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class CreatePostSubCategoryScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      GridListItems: [
          { key: "Jobs",icon: require('../../assest/Icons/AvailableJobs/Layer1.png'),screen:"CreatePost" },
          { key: "Rooms",icon: require('../../assest/Icons/AbroadServices/room.png') },
          { key: "Sales/Rent",icon: require('../../assest/Icons/AbroadServices/sales_rent.png') },
          { key: "Restaurants",icon: require('../../assest/Icons/AbroadServices/restaurants.png') },
          { key: "Exchange",icon: require('../../assest/Icons/AbroadServices/Exchange.png') },
          { key: "Saloons",icon: require('../../assest/Icons/AbroadServices/saloons.png') },
          { key: "Taxi",icon: require('../../assest/Icons/AbroadServices/taxi.png') },
          { key: "Shopping Mall",icon: require('../../assest/Icons/AbroadServices/shopping_mall.png') },
          { key: "Supermarket",icon: require('../../assest/Icons/AbroadServices/supermarket.png') },
        ]
    };
  }

  GetListItem(item) {
    Alert.alert(item);
  }

  componentDidMount = ()=>{
    console.log(this.props.route.params.category)
    console.log(this.props.route.params.category_id)
    //console.log(this.props.route.params.otp)
    this.GetSubCategory()

 }

//  getTokenData = async () => {
//     try {
//       const value = await AsyncStorage.getItem('token')
//       console.log(value)
//       const val=JSON.parse(value)
//       if(value !== null) {
//         console.log(val.token,value)
//         console.log(val.customerid,value)

//         this.setState({
//           token:val.token,customer_id:val.customerid
//         });

//         this.GetSubCategory()
        
//       }
//     } catch(e) {
//       // error reading value
//     }
//   }

  GetSubCategory = () => {
    
    this.props.startLoader()

    const url="https://pravasiconnect.websitepreview.in/api/get-sub-category"
    
    const params={
      category_id:this.props.route.params.category_id
    }

    const header = {
      'Content-Type': 'application/json',
      'X-Requested-With': `XMLHttpRequest`,
      }


      console.log(params)
      axios.post(url,params,header)
        .then((response) => {
         this.props.stopLoader()
        console.log(response.data);


        if(response.data.status==true)
        {
           this.setState({
            GridListItems:response.data.data,
            image:response.data.data.image,
           })
        }



    })
    .catch((error) => {
      console.log(error);
    });
  }

  render() {
    return (
      <SafeAreaView style={[styles.complete_view]}>
        <AppHeader isNotify = {false} {...this.props} />
        <ScrollView
        showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled">
          
          <View style={styles.headinView}>
          <Text style={styles.headnTxt}>Choose SubCategory</Text>
          </View>
          
          <View style={styles.container}>
          {this.state.GridListItems==""?<Text>{this.props.navigation.navigate('CreatePost',{main_category_id:this.props.route.params.category_id})}</Text>:
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={ this.state.GridListItems }
              renderItem={({ item }) => 
                          <View>
                  <TouchableOpacity onPress={()=>this.props.navigation.navigate('CreatePost',{main_category_id:this.props.route.params.category_id,sub_category_id:item.id})}
                    style={styles.listView}>
                                    <Image
                                        resizeMode={'contain'}
                                    style={{ height: 25, width: 25, marginTop:windowHeight/40 }}
                                    source={{uri:item.image}}
                                />
                            <Text style={styles.listViewText} > {item.title} </Text>
                            

                  </TouchableOpacity>
                  <View style={{height: 1,width:"85%",backgroundColor:"#d1c5c5",justifyContent:"center",alignSelf:"center",marginTop: 15,}}></View>
                          </View>
                        }
                        numColumns={1}
                    />
                    }
                </View>

        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

    complete_view: {
      flex: 1,
      backgroundColor: "white",
  },
  
  headinView: {
      
      marginTop: windowHeight/30,
      marginBottom: windowHeight/70,
      marginHorizontal: windowHeight/20
  },

  headnTxt: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
    },

    container: {
      width: windowWidth,
      height: windowHeight/1,
      // justifyContent: "center",
      // alignItems: 'center',
      backgroundColor: "#fff"
    },
    
  listView: {
      flexDirection:"row",
      marginTop: windowHeight / 50, 
      marginLeft: windowWidth/10,
      // justifyContent: 'center',
      // alignItems: 'center',
      // height: windowHeight/8,
      width:"80%",
      
      borderColor: "#d9d1d0",
      
      
      
      // backgroundColor:"red"
    },

    listViewText: {
      fontSize: 15,
      color:"black",
      fontWeight: "500",
      justifyContent: 'center',
      marginLeft: 20,
      marginTop:windowHeight/40
 }

})

// export default CreatePostSubCategoryScreen;
const mapStateToProps = (state) => {
  return {
    isLoggedIn:state.global.isLoggedIn,
    location:state.global.location
  };
};

export default connect(mapStateToProps, {setLoginData,startLoader,stopLoader})(
  CreatePostSubCategoryScreen,
);