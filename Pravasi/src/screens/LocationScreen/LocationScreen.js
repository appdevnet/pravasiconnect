import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Alert
} from "react-native";
import Root from '../../components/Root';
import icon from '../../assest/Icons/Location/paper-plane.png'; 
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'; 
import AppHeaderBack from '../../components/AppHeaderBack'

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class LocationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        
            <SafeAreaView style={{ backgroundColor: '#fff', flex: 1 }}>
            <AppHeaderBack {...this.props}/> 
            <View style={styles.headinView}>
                
                    <Text style={styles.locationText}>Location</Text>
            </View>
            
            <View style={{height:windowHeight/1.55,marginTop:windowHeight/30,width:windowWidth/1,}}>
        <GooglePlacesAutocomplete
      //style={{marginLeft:windowWidth/15,width:windowWidth/1.14,color:'black',}}
      placeholder='Search Your Location'
      minLength={2} // minimum length of text to search
      autoFocus={false}
      returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      listViewDisplayed='auto'    // true/false/undefined
      fetchDetails={true}
      renderDescription={row => row.description} // custom description render
      onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
       
        this.setState({
          latitude:details.geometry.location.lat,
          longitude:details.geometry.location.lng,
          place_name:details.formatted_address
        });
          
           //console.log(data, this.state.latitude)
          //  this.props.navigation.state.params.returnData(details);this.props.navigation.goBack()

          this.props.navigation.goBack({"details":details})
          
      }}
      
      getDefaultValue={() => ''}

      getCurrentLocation={(value)=>{console.log(value,"loc")}}
      
      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyAt-VI6pfAJgUV7EarWa_vVN4S9T1bj8NU',
        language: 'en', // language of the results
        types: '(cities)' // default: 'geocode'
      }}
  
       //currentLocation={true}
       //currentLocationLabel='Current location'
      
      
      styles={{
        container: {
          flex: 1,
          
        },

        textInputContainer: {
          width: '80%',
          color:'#000',
          borderWidth:0.5,
          borderColor:'grey',
          alignSelf: 'center',
          justifyContent:'center',
          backgroundColor: '#fff',
        },
        description: {
          fontWeight: 'bold'
        },
        predefinedPlacesDescription: {
          color: '#1faadb'
        }
      }}
      
      // currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
      // currentLocationLabel="Current location"
      nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
      GoogleReverseGeocodingQuery={{
        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
      }}
      GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        rankby: 'distance',
        types: 'food'
      }}

      filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
       //predefinedPlaces={[homePlace, workPlace]}

       enablePoweredByContainer={false}

      debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
    //    renderRightButton={()  => <Image source={icon} style={{height:windowHeight/40,width:windowWidth/22,marginRight:windowHeight/43,marginLeft:windowWidth/35,marginTop:windowHeight/70}}/>}
    //   renderRightButton={() => <Text>Custom text after the input</Text>}

   
    />
    </View>
            </SafeAreaView>
      
    );
  }
}

const styles = StyleSheet.create({
    headinView: {
    flexDirection:'row',
    paddingHorizontal: windowWidth/10,
    marginTop: windowHeight/200,
    marginBottom: windowWidth/20,
    
    },
    
    locationText: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black",
    },
    
    image_size: {
        height: windowHeight/60,
       width:windowHeight/60
    },
})

export default LocationScreen;
