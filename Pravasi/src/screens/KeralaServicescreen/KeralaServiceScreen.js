import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Platform,
  Dimensions,
  AsyncStorage,
  FlatList,
  Alert
} from "react-native";
import AppHeader from '../../components/AppHeader'
import MainBottom from '../../components/MainBottom'
import axios from 'react-native-axios';
import { connect } from 'react-redux';
import sample from '../../assest/Icons/Navigation/sample.png'
import {startLoader,stopLoader} from '../../redux/action'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class KeralaServiceScreen extends Component {
  constructor(props) {
    super(props);
      this.state = {
        GridListItems: [
        { key: "Real Estate",icon: require('../../assest/Icons/KeralaServices/Real_Estate.png'),screen:null },
        { key: "Photography",icon: require('../../assest/Icons/KeralaServices/photography.png') },
        { key: "Tourism",icon: require('../../assest/Icons/KeralaServices/Tourism.png') },
        { key: "Rent a Car",icon: require('../../assest/Icons/KeralaServices/Rent_car.png') },
        { key: "Event Planners",icon: require('../../assest/Icons/KeralaServices/Event_planners.png') },
        { key: "Grocery",icon: require('../../assest/Icons/KeralaServices/Grocery.png') },
        { key: "Book a Cab",icon: require('../../assest/Icons/AbroadServices/taxi.png'),screen:'bookcabscreen' },
        { key: "Handyman",icon: require('../../assest/Icons/KeralaServices/handyman.png') },
        { key: "Home Nurse",icon: require('../../assest/Icons/KeralaServices/home_nurse.png') },
      ]
    };
  }
  GetGridViewItem(item) {
    Alert.alert(item);
  }


  getTokenData = async () => {
    try {
      console.log("token valuee")
      const value = await AsyncStorage.getItem('token')
      .then(
      console.log("token valuee")
      )
      const val=JSON.parse(value)
      if(value !== null) {
        console.log(val.token,value)
        console.log(val.customerid,value)

        this.setState({
          token:val.token,customer_id:val.customerid
        });

        this.GetKeralaCategory()
        this.GetKeralaBanner()
        
      }
    } catch(e) {
      console.log(e.message)
      // error reading value
    }
  }

  componentDidMount=()=>{
    
    // this.getTokenData()
    this.GetKeralaCategory()
        this.GetKeralaBanner()
    
  }

  GetKeralaCategory=()=>{
    this.props.startLoader()
    const url="https://pravasiconnect.websitepreview.in/api/get-categories"
    
    const params={
      type:2,
      parent:true
    }

    const header = {
      'Content-Type': 'application/json',
      'X-Requested-With': `XMLHttpRequest`,
      }


      console.log(params)
      axios.post(url,params,header)
      .then((response) => {
        this.props.stopLoader()
        console.log(response,"api GetKeralaCategory");
       
        if(response.data.status==true)
        {
           this.setState({
            GridListItems:response.data.data,
             image:response.data.data.image,
           })

    

        }
      
      })
      .catch((error) => {
        console.log(error);
      });


  }


  GetKeralaBanner=()=>{
    this.props.startLoader()
    const url="https://pravasiconnect.websitepreview.in/api/get-banners"

    const header = {
      'Content-Type': 'application/json',
      'X-Requested-With': `XMLHttpRequest`,
      }


      axios.post(url,header)
      .then((response) => {
        this.props.stopLoader()
        console.log("Banner==",response);

         //response.data.data[0].banner.banner_image[0].image
         if(response.data.status==true)
         {
           this.setState({
             kerala_banner_image:response.data.data[1].banner.banner_image[0].image
           })

           console.log("json_banner",JSON.stringify(this.state.kerala_banner_image))

         }
      })
      .catch((error) => {
        console.log(error);
      });


  }

  render() {
    console.log(this.state.GridListItems)
    return (
      <SafeAreaView style={styles.complete_view}>
      <AppHeader isNotify={true} {...this.props} />
            
                
            
            <View style={{position:"relative",width:windowWidth,backgroundColor:"#F6F8FC"}}>
          <Image
            resizeMode="cover"
                    style={{height:windowHeight/2.5,width:windowWidth,marginTop:windowHeight/200}}
                    source={{uri:this.state.kerala_banner_image}}
                />
                <TouchableOpacity style={styles.BtnStyle} onPress={()=> this.props.navigation.navigate(this.props.isLoggedIn?"localBusinessDirectory":'login')}>
           <Text style={styles.BtnText}>REGISTER A BUSINESS</Text>
       </TouchableOpacity>
            </View>

            <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled">
                
            <View style={styles.headinView}>
          <Text style={styles.headnTxt}>Available Services in Kerala</Text>
                </View>
                
            <View style={styles.container}>
            <FlatList 
            showsVerticalScrollIndicator={false}
            data={ this.state.GridListItems }
            renderItem={ ({item}) =>
                <TouchableOpacity style={styles.GridViewContainer}
                onPress={()=> this.props.navigation.navigate('availablejobscreen',{category:item.title,category_id:item.id})}>
                <Image
                  resizeMode={'contain'}
                  style={{ height: windowHeight/31, width: windowWidth/13,marginTop: 4   }}
                        source={{uri:item.image}}
                    />
               <Text style={styles.GridViewTextLayout} > {item.title} </Text>
              </TouchableOpacity> }
            numColumns={3}
         />
       </View>
        </ScrollView>
        <MainBottom {...this.props} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

    complete_view: {
      flex: 1,
      backgroundColor: "white",
    },

    headinView: {
      marginTop: windowHeight/30,
      marginBottom: windowHeight/70,
      marginHorizontal: windowHeight/24.5
  },

  headnTxt: {
    fontWeight: "bold",
    fontSize: 15,
    color: "black"
    },
  
  container: {
    width: windowWidth,
    height: windowHeight/2.9,
      justifyContent: "center",
      alignItems: 'center',
      backgroundColor: "#fff"
  },
  
  GridViewContainer: {
      marginTop: windowHeight/55,
      justifyContent: 'center',
      alignItems: 'center',
      height: windowHeight/10,
      width:windowWidth/3.8,
      margin: windowWidth/50,
      borderColor: "#d9d1d0",
      borderWidth: 1,
      borderRadius:12,
      backgroundColor:"#fff"
},
GridViewTextLayout: {
      fontSize: 10,
      color:"#0e0440",
      fontWeight: "500",
      justifyContent: 'center',
      padding: 10,
 },

 BtnStyle:{
  backgroundColor:'#0D3369',
  width:windowWidth/3,
  height:windowHeight/28,
  marginLeft:windowWidth/7,
  position:'absolute',
  marginTop:windowHeight/4,
  textAlign:'center',
  alignItems:'center',
  borderRadius:60,
  justifyContent:'center',
},

BtnText:{
  color:'#fff',
  textAlign:'center',
  fontSize:10,
  alignItems:'center',
  fontWeight:'bold',
  justifyContent:'center',
},
    
})


// export default KeralaServiceScreen;
const mapStateToProps = (state) => {
  return {
    isLoggedIn:state.global.isLoggedIn
  };
};

export default connect(mapStateToProps, {startLoader,stopLoader})(
  KeralaServiceScreen,
);