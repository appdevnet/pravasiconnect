import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  Dimensions,
  Alert,
  TouchableOpacity,
  SafeAreaView,
  AsyncStorage
} from "react-native";
import AppHeaderBack from '../../components/AppHeaderBack';
import axios from 'axios';




const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class ChangePasswordscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {

      email:"",
      ema:true,
     
      
      
    };
  }


     componentDidMount = ()=>{
        console.log(this.props.route.params.cus_id)
        console.log(this.props.route.params.otp)

     }


  reset=()=>{
    const {email}=this.state;

    if(email=="")
    {
      // alert("The Email is either empty or Wrong")
      this.setState({ema:false});
      return false;
    }
    else
    {

      // alert("Password Resetted");
      // this.props.navigation.replace('login');
      return true;
    }

  }

  Change_password=()=>{
     
    const url= "https://pravasiconnect.websitepreview.in/api/forgot-password-update";

    if(this.reset())
    {
      const params={
        otp:this.props.route.params.otp,
        customer_id:this.props.route.params.cus_id,
        new_password:this.state.email
      }

      const header = {
        'Content-Type': 'application/json',
        'X-Requested-With': `XMLHttpRequest`,
        }
        console.log(params)
       
        axios.post(url,params,header)
        .then((response) => {
          console.log(response);


          if(response.data.status)
          {
             this.setState({message:response.data.data.message,
            })
             Alert.alert(this.state.message)
             
             this.props.navigation.navigate('login');
            }
            else{
             Alert.alert("Invalid OTP or Password Must be atleast 6 digits");
    
            }
  
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }





  render() {
    const { navigation } = this.props;

    //const id=navigation.getParam('userid');
    
    return (
      <SafeAreaView style={{backgroundColor:'#fff',flex:1}}>
      <AppHeaderBack {...this.props} />
        <View style={[styles.headinView,{flexDirection:'row'}]}>
          <Text style={styles.resetHeading}>Change Password</Text>
          
        </View>

        <View style={styles.container}>
          <View style={[styles.inputView,{borderColor:this.state.ema?null:"red",borderWidth:this.state.ema?0:1}]}>
        <TextInput
          style={styles.TextInput}
          placeholder={this.state.ema?"New Password":"Provide the Password"}
          placeholderTextColor={this.state.ema?"#A5A5A5":"red"}
          color='black'
          keyboardType='email-address'
          onChangeText={text => this.setState({email:text,ema:true})}
          
        />
          </View>
          
          <TouchableOpacity style={styles.loginBtn}
          onPress={()=> this.Change_password()}>
        <Text style={styles.loginText}>SUBMIT</Text>
          </TouchableOpacity>
          

        <View style={[styles.socialtxt]}>
        {/* <TouchableOpacity onPress={() => navigation.navigate('login')}>
            <Text>Back to <TouchableOpacity
            onPress={() => navigation.navigate('login')} >
              <Text style={styles.registrbtn}>SIGN IN</Text></TouchableOpacity></Text></TouchableOpacity> */}
          </View>

        
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headinView: {
    paddingHorizontal: windowWidth/20,
    marginTop: windowHeight/70,
    marginBottom: windowHeight/35,
    marginHorizontal: windowWidth/20
  },

  resetHeading: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
  },

  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: windowHeight/20,
    
  },

  inputView: {
    backgroundColor: "#EBF1FF",
    borderRadius: 12,
    width: windowWidth/1.1,
    height: windowHeight/15,
    marginBottom: windowHeight/20,

  },
 
  TextInput: {
    height: windowHeight/15,
    paddingHorizontal: 10,
    marginHorizontal: 10,
  },

  loginBtn: {
    width: windowWidth/1.2,
    borderRadius: 20,
    height: windowHeight/16,
    alignSelf: "center",
    justifyContent: "center",
    marginTop: windowHeight/50,
    backgroundColor: "#070545",
    
  },

  loginText: {
    justifyContent: "center",
    alignSelf: "center",
    fontSize: 17,
    fontWeight: "bold",
    color: "white"
  },

  socialtxt: {
    paddingTop: windowHeight/2.6,
    fontSize:15,
  },

  registrbtn: {
    color: "#070545",
    fontSize: 15,
    top:3,
    fontWeight: "bold"
    
  }
});
export default ChangePasswordscreen;
