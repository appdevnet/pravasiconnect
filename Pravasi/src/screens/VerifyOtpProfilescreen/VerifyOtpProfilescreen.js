import React, { Component } from 'react';
import { View, Text,AsyncStorage,TextInput,TouchableOpacity,StyleSheet,SafeAreaView,Dimensions } from 'react-native';
import AppHeaderBack from '../../components/AppHeaderBack';
import axios from 'axios';
import Toast from '../../components/Toast';
import Root from '../../components/Root';
import { Alert } from 'react-native';
var qs = require('qs');

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class VerifyOtpProfilescreen extends Component {
  constructor(props) {
    super(props);
    this.state = {

        email:"",
        ema:true,
        timer: 2*60,
    };
  }

  componentDidMount(){
    console.log(this.props.route.params.isotp,"kittypoyi")
    this.getTokenData();
    //this.CheckMobileOtp();
   this.interval = setInterval(
     () => this.setState((prevState)=> ({ timer: prevState.timer - 1 })),
     1000

     
     
   );  
 }

 getTokenData = async () => {
 try {
    const value = await AsyncStorage.getItem('token')
    const valu = await AsyncStorage.getItem('location')
    console.log(valu)
    console.log(value)
    const val=await JSON.parse(value)
    const va=await JSON.parse(valu)
    if(value !== null) {
      //console.log(val.token,"ttooo")
      //console.log(val.customerid,"idddd")
      console.log(va.place)

      this.setState({
        token:val.token,customer_id:val.customerid,
        place:va.place
      });
      //this.CheckEmailOtp()
    }
  } catch(e) {
    // error reading value
  }
}
 
 componentDidUpdate(){
   if(this.state.timer === 1){ 
     clearInterval(this.interval);

   }
 }
 
 componentWillUnmount(){
  clearInterval(this.interval);
 }

 CheckEmailOtp=()=>{
     if(this.props.route.params.isotp==false)
     {
    
        const params={
          otp:this.state.email,
          customer_id:this.state.customer_id
        }
    
        fetch("https://pravasiconnect.websitepreview.in/api/verify-email",
        {
          method: 'POST',
          headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest',
          Authorization: 'Bearer '+this.state.token,
          },
          body: qs.stringify(params),
          })
  
          .then((response) => response.json())
          .then((response) => {
          console.log(response);

          if(response.status==true)
          {
            Alert.alert(response.message)
            this.props.navigation.navigate("userprofilescreen")
          }

        })
        .catch((error) => {
          console.log(error);
        });
     }
     else if(this.props.route.params.isotp==true)
     {
       
        const params={
          otp:this.state.email,
          customer_id:this.state.customer_id
        }
        fetch("https://pravasiconnect.websitepreview.in/api/verify-mobile",
        {
          method: 'POST',
          headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest',
          Authorization: 'Bearer '+this.state.token,
          },
          body: qs.stringify(params),
          })
  
          .then((response) => response.json())
          .then((response) => {
          console.log("rrr",response);

          if(response.status==true)
          {
            Alert.alert(response.message)
            this.props.navigation.navigate("userprofilescreen")
          }

        })
        .catch((error) => {
          console.log(error);
        });
        

     } 

 }

 reset=()=>{
   const {email}=this.state;

   if(email=="")
   {
     // alert("The Email is either empty or Wrong")
     this.setState({ema:false});
     return false;
   }
   else
   {

     // alert("Password Resetted");
     // this.props.navigation.replace('login');
     return true;
   }

 }

  render() {
    return (
        <SafeAreaView style={{backgroundColor:'#fff',flex:1}}>
        <AppHeaderBack {...this.props} />
          <View style={[styles.headinView,{flexDirection:'row'}]}>
            <Text style={styles.resetHeading}>Verify OTP</Text>
            <Text style={{marginLeft:windowWidth/4,marginTop:windowHeight/110}}>Verify Within: {this.state.timer}s</Text>
          </View>
  
          <View style={styles.container}>
            <View style={[styles.inputView,{borderColor:this.state.ema?null:"red",borderWidth:this.state.ema?0:1}]}>
          <TextInput
            style={styles.TextInput}
            placeholder={this.state.ema?"OTP":"Provide the OTP"}
            placeholderTextColor={this.state.ema?"#A5A5A5":"red"}
            color='black'
            keyboardType='email-address'
            onChangeText={text => this.setState({email:text,ema:true})}
            
          />
            </View>
            
            <TouchableOpacity style={styles.loginBtn}
            onPress={()=> this.CheckEmailOtp()}>
          <Text style={styles.loginText}>VERIFY</Text>
            </TouchableOpacity>
            
  
          <View style={[styles.socialtxt]}>
          {/* <TouchableOpacity onPress={() => navigation.navigate('login')}>
              <Text>Back to <TouchableOpacity
              onPress={() => navigation.navigate('login')} >
                <Text style={styles.registrbtn}>SIGN IN</Text></TouchableOpacity></Text></TouchableOpacity> */}
            </View>
  
          
          </View>
        </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
    headinView: {
      paddingHorizontal: windowWidth/20,
      marginTop: windowHeight/70,
      marginBottom: windowHeight/35,
      marginHorizontal: windowWidth/20
    },
  
    resetHeading: {
      fontWeight: "bold",
      fontSize: 20,
      color: "black"
    },
  
    container: {
      alignItems: "center",
      justifyContent: "center",
      marginTop: windowHeight/20,
      
    },
  
    inputView: {
      backgroundColor: "#EBF1FF",
      borderRadius: 12,
      width: windowWidth/1.1,
      height: windowHeight/15,
      marginBottom: windowHeight/20,
  
    },
   
    TextInput: {
      height: windowHeight/15,
      paddingHorizontal: 10,
      marginHorizontal: 10,
    },
  
    loginBtn: {
      width: windowWidth/1.2,
      borderRadius: 20,
      height: windowHeight/16,
      alignSelf: "center",
      justifyContent: "center",
      marginTop: windowHeight/50,
      backgroundColor: "#070545",
      
    },
  
    loginText: {
      justifyContent: "center",
      alignSelf: "center",
      fontSize: 17,
      fontWeight: "bold",
      color: "white"
    },
  
    socialtxt: {
      paddingTop: windowHeight/2.6,
      fontSize:15,
    },
  
    registrbtn: {
      color: "#070545",
      fontSize: 15,
      top:3,
      fontWeight: "bold"
      
    }
  });

export default VerifyOtpProfilescreen;
