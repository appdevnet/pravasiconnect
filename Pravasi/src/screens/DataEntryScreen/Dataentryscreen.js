import React, { Component } from 'react';
import { View, Text,StyleSheet,SafeAreaView,ScrollView,FlatList,Image,TouchableOpacity,Dimensions,AsyncStorage } from 'react-native';
import AppHeader from '../../components/AppHeader';
import MainBottom from '../../components/MainBottom';
import axios from 'react-native-axios';

import {connect} from 'react-redux';
import { setLoginData,startLoader,stopLoader,SetUserId,SetToken,setLocation } from '../../redux/action'

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;



class Dataentryscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {

      PostDetail: [
        // { key: 1, image: "https://images.unsplash.com/photo-1554415707-6e8cfc93fe23?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bGVhcm5pbmclMjBjb21wdXRlcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&w=1000&q=80",title:"P&D Commuincations",description:"Lorem ipsum is a name for a common type of placeholder text. Also known as filler or dummy text, this is simply copy that serves to fill a space without actually saying anything meaningful. It's essentially nonsense text, but gives an idea of what real words will look like in the final product."},
        // {key: 2, image: "https://st.depositphotos.com/1751039/1241/i/600/depositphotos_12412183-stock-photo-data-entry.jpg",title:"DC Books",description:"Lorem ipsum is a name for a common type of placeholder text. Also known as filler or dummy text, this is simply copy that serves to fill a space without actually saying anything meaningful. It's essentially nonsense text, but gives an idea of what real words will look like in the final product."}
      ]

      // CategoryList:[
      //   {key:1,category:"Recent"},
      //   {key:2,category:"Liked"},
      //   {key:3,category:"Recommended"},
      //   {key:4,category:"Bakery"},
      //   {key:5,category:"Information"},
      // ]

    };
  }

  

  getTokenData = async () => {
    try {
      const value = await AsyncStorage.getItem('token')
      const valu = await AsyncStorage.getItem('location')
      console.log(valu)
      console.log(value)
      const val=await JSON.parse(value)
      const va=await JSON.parse(valu)
      if(value !== null) {
        //console.log(val.token,"ttooo")
        //console.log(val.customerid,"idddd")
        console.log("lat",va.latitude)
        console.log("lon",va.longitude)

        this.setState({
          token:val.token,customer_id:val.customerid,
          latitude: va.latitude,
          longitude:va.longitude
        });
        //this.GetPostDetails()
      }
    } catch(e) {
      // error reading value
    }
  }

  GetPostDetails=()=>{
    this.props.startLoader();
    const url="https://pravasiconnect.websitepreview.in/api/get-posts"
    
    const params = {
      category: this.props.route.params.categoryid,
      lat: this.props.location.latitude,
      long:this.props.location.longitude
    }

    const header = {
      'Content-Type': 'application/json',
      'X-Requested-With': `XMLHttpRequest`,
      }


      console.log("params",params)
      axios.post(url,params,header)
        .then((response) => {
          this.props.stopLoader();
        console.log(response);
        if(response.data.status==true)
        {
           this.setState({
            PostDetail:response.data.data,
             image: response.data.data.image,
             title:response.data.data.title
           })
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

   componentDidMount=()=>{

    this.GetPostDetails()

    console.log("subcateg",this.props.route.params.sub_category)
    console.log("subcategid",this.props.route.params.subcategory_id)
    console.log("cateid",this.props.route.params.categoryid)

    this.getTokenData()
    
  }


  render() {
    return (
        <SafeAreaView style={{backgroundColor:'#fff',flex:1}}>
         <AppHeader {...this.props}/>
        <ScrollView showsVerticalScrollIndicator={false}
               keyboardShouldPersistTaps="handled">
      <View>
        <Text style={styles.heading}>{this.props.route.params.sub_category}</Text>
      </View>
      
      
      
      <View style={styles.data_category}>
       <Text >Recent</Text>
       <Text style={{marginLeft:windowWidth/13,}}>Liked</Text>
       <Text style={{marginLeft:windowWidth/13,}}>Recommended</Text>
       <Text style={{marginLeft:windowWidth/13,}}>Bakery</Text>
          </View>
      
          {this.state.PostDetail==""?<Text style={styles.casehandle}>No Posts Available</Text>:
          <FlatList
            showsVerticalScrollIndicator={false}
            numColumns={1}
            data={this.state.PostDetail}
            renderItem={({item}) =>
              <View style={styles.container}>
       <Image style={{height:windowHeight/5,width:windowWidth/1.2}} source={{ uri:item.post_image}}>
      </Image>
      <Text style={{fontSize:23,fontWeight:'bold',marginTop:windowHeight/30,}}>{item.title}</Text>
                <Text style={styles.txt_2}>{ item.description}</Text>
            
      <View style={{marginLeft:windowWidth/50,}}>
       <TouchableOpacity style={styles.BtnStyle} onPress={()=> this.props.navigation.navigate("pdscreen")}>
           <Text style={styles.BtnText}>MORE INFO</Text>
       </TouchableOpacity>
       </View>
            <View style={styles.lines}></View>
            
       </View>       
          }
          />
          }
       
       

       
      
      </ScrollView>
      <MainBottom {...this.props} />
      </SafeAreaView>
    );
  }
}


const styles=StyleSheet.create({

    heading:{
        fontSize:18,
        fontWeight:'bold',
        marginTop:windowHeight/90,
        marginLeft:windowWidth/12,
        flexDirection: 'row',
    },

    data_category:{
      flexDirection:'row',
      marginTop: windowHeight/40,
      marginLeft:windowWidth/10,
    },

    container: {
        alignItems: "center",
        justifyContent: "center",
        marginTop: windowHeight/20, 
        },

        txt_2: {
            paddingHorizontal: "10%",
            paddingVertical: 20,
            color: "grey",
            lineHeight:20,
            fontSize: 12,
            justifyContent: "center",
            alignSelf:"center"
        },
        
        lines: {
            width: "80%",
            backgroundColor: '#d9d1d0',
            height: 1,
            justifyContent: "center",
            alignSelf:"center",
            marginTop:10,
        },

        BtnStyle:{
          backgroundColor:'#2051AE',
          width:80,
          height:25,
          textAlign:'center',
          alignItems:'center',
          borderRadius:60,
          justifyContent:'center',
        },

        BtnText:{
          color:'#fff',
          textAlign:'center',
          fontSize:10,
          alignItems:'center',
          fontWeight:'bold',
          justifyContent:'center',
        },

        casehandle:{
          fontSize:18,
          marginTop:windowHeight/3,
          marginLeft:windowWidth/3.2,
          flexDirection: 'row',
        },
})

//export default Dataentryscreen;

const mapStateToProps = (state) => {
  return {
    isLoggedIn:state.global.isLoggedIn,
    location:state.global.location,
    loginData:state.global.loginData,
    userID:state.global.userID,
    Token:state.global.Token
  };
};

export default connect(mapStateToProps, {setLoginData,startLoader,stopLoader,SetToken,SetUserId,setLocation})(
  Dataentryscreen,
);
