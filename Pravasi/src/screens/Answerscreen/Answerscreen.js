import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Modal,
  Dimensions,
  Platform,
  FlatList,
  Alert
} from "react-native";
import AppHeader from '../../components/AppHeader'
import SwitchButton from '../../components/SwitchButton'
import MainBottom from '../../components/MainBottom'

import cal from '../../assest/iconss/calender.png';
import like from '../../assest/iconss/like.png';
import dislike from '../../assest/iconss/dislike.png';
import sort from '../../assest/iconss/sort.png';
import sort2 from '../../assest/iconss/sort2.png';

import axios from 'react-native-axios';
import Toast from '../../components/Toast';
import Root from '../../components/Root';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
class Answerscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {

      isAnswer: false,
      answer:"",
       
        array:[

    //         {id:"1",date:"02-07-2021",note:"Data driven testing is an automation testing framework, which tests the different input values on the AUT. These values are read directly from the data files. The data files may include csv files, excel files, data pools and many more.",
    //     pic:require("../../assest/Icons/Profile/img_avatar.png"),an:"Hemil Prasoon",place:"United Arab Emirates",
    //     like:require("../../assest/Icons/update/Iconfeather-thumbs-up.png")},

    // {id:"2",date:"02-07-2020",note:"Agile testing is software testing, is testing using Agile Methodology. The importance of this testing is that, unlike normal testing process, this testing does not wait for the development team to complete the coding first and then doing testing. The coding and testing both goes simultaneously. It requires continuous customer interaction.",
    // pic:require("../../assest/Icons/Profile/img_avatar.png"),an:"John Doe",place:"United Arab Emirates",
    //   like:require("../../assest/Icons/update/Iconfeather-thumbs-up.png")},
        ]

    };
  }

  componentDidMount=()=>{
    console.log(this.props.route.params.data,"que")

    this.setState({
      array:this.props.route.params.data.answer
    })

    console.log("array",this.state.array)

  }

  PostAnswer=()=>{
    
  }

  render() {
    return (
      <>
        <SafeAreaView style={styles.complete_view}>
            <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled">
            <AppHeader {...this.props} />
            <View style={styles.headinView}>
            <Text style={styles.loginHeading}>{this.props.route.params.data.title}</Text>
          </View>
          
            <View style={{paddingHorizontal: "8%",}}>
            <View style={{ marginTop: windowWidth / 20, marginLeft: windowWidth / 20, flexDirection: 'row',justifyContent:"space-between" }}>
              <View style={{ flexDirection: "row" }}><Text>By </Text>
                <Text style={{ fontWeight: "bold" }}>{this.props.route.params.data.user.name}</Text></View>
              <TouchableOpacity style={{ height: 25, width: 60, backgroundColor: "#080f75", borderRadius: 12 }} onPress={()=>{this.setState({isAnswer:true})}}>
                <Text style={{ color: "white", fontWeight: "bold", fontSize: 10, justifyContent: "center", alignSelf: "center", paddingVertical: 5 }}>Answer</Text>
              </TouchableOpacity>
            </View>

                <View style={{marginTop:windowWidth/20,marginLeft:windowWidth/20,flexDirection:'row',justifyContent:"space-between",marginRight:windowWidth/16}}>
            <Text onPress={()=>this.props.navigation.navigate('Answerscreen')} style={{fontWeight:"600"}}>{this.props.route.params.data.answer_count} Answers</Text>
                <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
            <Image source={sort} style={{alignSelf:'center',marginLeft:windowWidth/16,justifyContent:'center',height:windowHeight/70,width:windowHeight/70,}}></Image>
                  <Text style={{ marginLeft: windowWidth / 90, fontSize: 12,fontWeight:"500" }}>Sort By</Text>
            <Image source={sort2} style={{alignSelf:'center',marginLeft:windowWidth/80,justifyContent:'center',height:windowHeight/70,width:windowHeight/70,}}></Image>      
            </TouchableOpacity>
            {/* <View style={{width:windowWidth/2}}>
            </View> */}
            </View>

          </View>
       
            <FlatList
                data={this.state.array}
                renderItem={({item})=>
                  <View style={{ marginLeft: windowWidth / 35,marginRight: windowWidth/35 }}>

                    <View style={{height:1,backgroundColor:"#d1c5c5",marginVertical:30,}}></View>
                    
                    <View style={{ marginTop: windowWidth / 18, marginLeft: windowWidth / 10, flexDirection: 'row' }}>
                      <Image source={cal} style={{height:10,width: 15,marginVertical:2.8,marginHorizontal:5}}
                      />
            <Text>{item.date}</Text>
                    </View>
                    
            <View style={styles.container}>  
               <Text style={styles.txt_2}>{item.note}</Text>
                    </View>
                    
                    <View style={{ flexDirection: 'row', marginLeft: -25, marginTop: windowHeight / 500, }}>  
                      <View >
                        
      <Image source={item.pic} style={styles.image}></Image>
      </View>
      <View style={styles.chatview}>
          <Text >{item.an}</Text>
          <Text style={styles.msgstyle} numberOfLines={1}>{item.place}</Text>
      </View>
                      <View style={{ flexDirection: "row" }}>
                        <TouchableOpacity>
                        <Image source={like} style={{height:windowHeight/50,width:windowWidth/25,marginLeft:windowWidth/4.2,
                            marginTop: windowHeight / 18,
                          }} /></TouchableOpacity>
                        <TouchableOpacity>
      <Image source={dislike} style={{height:windowHeight/50,width:windowWidth/25,marginLeft:windowWidth/15,
    marginTop:windowHeight/18,}}></Image></TouchableOpacity> 
      </View>
  
      </View>

      </View>
      }
       />
          
          <View style={styles.centeredView}>
      <Modal 
      animationType = {"slide"} 
      transparent = {true} 
      visible = {this.state.isAnswer} 
      onRequestClose = {() =>{ console.log("Modal has been closed.") } }> 
      {/*All views of Modal*/} 
      <View style = {styles.modal}> 
      
      <Text style={{fontSize:13,fontWeight:'bold',marginTop:windowHeight/180,padding:15}}>ANSWER</Text>
      
      <View style={[styles.inputView,]}>
      <TextInput
      style={[styles.TextInput,{fontSize:15,fontWeight:"500"}]}
      placeholder={"Post Your Answers"}
      multiline={true}
      numberOfLines={3}
      placeholderTextColor={"#A5A5A5"}
      color='black'
      keyboardType='email-address'
      onChangeText={text => this.setState({answer:text,uname:true})}
      // onChangeText={(email) => setEmail(email)}
      />
      
      </View>
      
      <TouchableOpacity
      style={[styles.button, styles.buttonClose,{marginTop:windowHeight/50}]}
      onPress={()=>this.PostAnswer()}
      ><Text style={styles.textStyle}>SUBMIT</Text></TouchableOpacity> 
      <Text style={{color:'black',marginTop:windowHeight/45,fontSize:14,fontWeight:'bold'}}
      onPress = {() => { 
      this.setState({ isAnswer:!this.state.isAnswer})}}>CLOSE</Text>
      
      </View> 
      </Modal>
      </View>
            
           

      </ScrollView>
      <MainBottom {...this.props} />
        </SafeAreaView>
        </>
    );
  }
}

const styles = StyleSheet.create({

    complete_view: {
        flex: 1,
        backgroundColor: "white",
    },

    headinView: {
    paddingHorizontal: "10%",
    marginTop: 30,
    // marginBottom: 30,
    

  },

  textstyle:{
    marginLeft:windowWidth/10,
    color:'#000'

  },

  image:{
    width:windowWidth/9,
    height:windowHeight/40,
    borderRadius:60/2,
    marginLeft:52,
    marginTop: 30,
    flex:1,
},

chatview:{
  marginTop: 43,
marginLeft: 20,
width:100,

},

msgstyle:{
 fontSize:13,
 color:'#9A9A9A',
},

  loginHeading: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
    },
  
  container: {
    alignItems: "center",
    justifyContent: "center",
     
    },
  
  socialtxt: {
    fontSize:10,
  },

  registrbtn: {
    color: "#070545",
    fontSize: 10,
    top:0,
    fontWeight: "bold"
    
    },
  
    txt_2: {
        paddingHorizontal: "10%",
        paddingVertical: 20,
        color: "grey",
        lineHeight:20,
        fontSize: 12,
        justifyContent: "center",
      alignSelf: "center",
        
    },
    
    lines: {
        width: "80%",
        backgroundColor: '#d9d1d0',
        height: 1,
        justifyContent: "center",
        alignSelf:"center"
    },

    contact_View: {
        height: 50,
        width:'80%',
        flexDirection: "row",
        backgroundColor: "#fff",
        marginHorizontal: "10%",
        marginBottom:30,
    },

    num_txt: {
        color: "grey",
        fontSize:12
    },

    view_all: {
        width: 90,
        height: 30,
        backgroundColor: "#080f75",
        borderRadius: 15,
        marginHorizontal:"20%"
    },

    txt_view_all: {
        alignSelf: "center",
        justifyContent: "center",
        color: "white",
        fontSize: 10,
        fontWeight:'500',
        paddingVertical:"10%"
    },

    salary_amt: {
        marginHorizontal: "10%",
        marginBottom:30
    },

    switchView: {
        flexDirection: "row",
        paddingHorizontal: 10,
        marginBottom: 30,
    },

    switchView2: {
        flexDirection: "row",
        paddingHorizontal: 10,
        marginBottom: 30,
    
    },
    
    txtSwitch: {
    paddingHorizontal: 15,
    left:10
  },
    
    modal: { 
    justifyContent: 'center', 
    alignItems: 'center', 
    backgroundColor : "#fff", 
    height: windowHeight/1.6 , 
    width: '90%', 
    borderRadius:10, 
    borderWidth: 1, 
    borderColor: '#fff', 
    marginTop: windowHeight/4.4, 
    marginLeft: windowWidth/21, 
 
    shadowColor: "#000",
    shadowOffset: {
    width: 0,
    height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
    
    }, 
 
    centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: windowHeight/2
    },
    
    button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    width:windowWidth/3,
    },
    
  buttonClose:
    {
    backgroundColor: "#070545",
    },
    
    textStyle: {
    color: "white",
    textAlign: "center",
    fontWeight:'bold',
    fontSize:13,
    },
 
    TextInput: {
    height: windowHeight / 2.6,
    paddingTop: 20,
    paddingHorizontal: 10,
    marginHorizontal: 10,
    marginTop:windowWidth/180,
    backgroundColor:"#EBF1FF"
    },
    
    inputView: {
    backgroundColor: "#EBF1FF",
    borderRadius: 12,
    width: "80%",
    height: windowHeight/2.5,
    color:'black'
    },

})

export default Answerscreen;
