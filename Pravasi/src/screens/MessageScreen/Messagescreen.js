import React, { Component } from 'react';
import { View, Text,SafeAreaView,StyleSheet,Image,ScrollView } from 'react-native';
import MainBottom from '../../components/MainBottom';
import AppHeader from '../../components/AppHeader';
import av from '../../assest/Icons/Profile/img_avatar.png';
import av1 from '../../assest/Icons/Profile/av1.png';
import av2 from '../../assest/Icons/Profile/av2.jpeg';
import av3 from '../../assest/Icons/Profile/av3.jpeg'
import addchat from '../../assest/Icons/Messages/Group295.png'
class Messagescreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <SafeAreaView style={{backgroundColor:'#fff',flex:1}}>
        <AppHeader {...this.props} />
        <ScrollView>
      <View style={{flexDirection:'row'}}>
        <Text style={styles.heading}>Messages</Text>
        <Image source={addchat} style={styles.addchat}></Image>
      </View>
      <View style={{flexDirection:'row',marginLeft:-25}}>
      <View>
      <Image source={av} style={styles.image}></Image>
      </View>
      <View style={styles.chatview}>
          <Text>Vivek T P</Text>
          <Text style={styles.msgstyle} numberOfLines={1}>How is going?</Text>
      </View>
      <View style={styles.time}>
          <Text style={{fontSize:13,color:'#9A9A9A'}}>04:40</Text>
      </View>
      </View>


      <View style={{flexDirection:'row',marginLeft:-25}}>
      <View>
      <Image source={av1} style={styles.image}></Image>
      </View>
      <View style={[styles.chatview]}>
          <Text numberOfLines={1}>Connor Frattggdgddhh</Text>
          <Text style={styles.msgstyle} numberOfLines={1}>How is going?</Text>
      </View>
      <View style={styles.time}>
          <Text style={{fontSize:13,color:'#9A9A9A'}}>05:12</Text>
      </View>
      </View>
      

      <View style={{flexDirection:'row',marginLeft:-25}}>
      <View>
      <Image source={av2} style={styles.image}></Image>
      </View>
      <View style={[styles.chatview]}>
          <Text numberOfLines={1}>Josephine Gordon</Text>
          <Text style={styles.msgstyle} numberOfLines={1}>How is going?</Text>
      </View>
      <View style={styles.time}>
          <Text style={{fontSize:13,color:'#9A9A9A'}}>01:22</Text>
      </View>
      </View>


      <View style={{flexDirection:'row',marginLeft:-25}}>
      <View>
      <Image source={av3} style={styles.image}></Image>
      </View>
      <View style={[styles.chatview]}>
          <Text numberOfLines={1}>Timothy Steele</Text>
          <Text style={styles.msgstyle} numberOfLines={1}>How is going?</Text>
      </View>
      <View style={styles.time}>
          <Text style={{fontSize:13,color:'#9A9A9A'}}>10:10</Text>
      </View>
      </View>
     
      </ScrollView>
      <MainBottom {...this.props} />
      </SafeAreaView>
    );
  }
}

export default Messagescreen;


const styles=StyleSheet.create({

    heading:{
        fontSize:25,
        fontWeight:'bold',
        marginTop:30,
        marginLeft:23,
        flexDirection: 'row',
    },

    image:{
        width:60,
        height:60,
        borderRadius:60/2,
        marginLeft:52,
        marginTop:30,
        flex:1,
    },

    addchat:{
     marginTop: 33,
     marginLeft: 170,
    },

chatview:{
  marginTop: 43,
  marginLeft: 20,
  width:100,
  
 },

 msgstyle:{
   fontSize:13,
   color:'#9A9A9A',
 },

 time:{
 marginLeft: 110,
 marginTop: 43,
 
 
 },

 

})