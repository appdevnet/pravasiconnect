import React, { Component } from 'react';
import { View, Text,SafeAreaView,ScrollView,Image,StyleSheet,Dimensions,TouchableOpacity } from 'react-native';
import thankyou from '../../assest/Icons/Thankyou/Group283.png';
import ThankBankbutton from '../../components/ThankBankbutton';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


class Thankyouscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { navigation } = this.props;
    return (
        <SafeAreaView style={{backgroundColor:'#fff',flex:1}}>
        <ScrollView>
      <View >
      <Image source={thankyou} style={styles.content}></Image>
        <Text style={{marginTop:windowHeight/30,marginLeft:windowWidth/6.3,fontWeight:'bold',fontSize:17}}> Thank you for submitting a Post.</Text>
        {/* <Text style={{marginTop:5,marginLeft:105,}}>One of Our Representative</Text>
        <Text style={{marginTop:5,marginLeft:125,}}>will contact you soon. </Text> */}
      </View>
      </ScrollView>
      <TouchableOpacity onPress={()=>this.props.navigation.navigate('abroadservice')}>
          <View style={styles.lcl_bsns_d_regstr_Btn}>
            <Text style={styles.lcl_bsns_d_regstr_txt}>BACK TO HOME</Text>
            </View>
          </TouchableOpacity>
      </SafeAreaView>
    );
  }
}


const styles=StyleSheet.create({
   
    content:{
      flex:1,
      flexDirection:'column',
      justifyContent:'center',
      alignContent:'center',
      marginTop:150,
      marginLeft:120,
      width:150,
      height:150,
    },

    lcl_bsns_d_regstr_Btn: {
      width: windowWidth/1,
      height: windowHeight/13,
      backgroundColor:"#070545"
    },
  
    lcl_bsns_d_regstr_txt: {
      fontSize: 20,
      fontWeight: "bold",
      alignSelf: "center",
      paddingVertical:windowHeight/45,
      color:"white"
    }
})

export default Thankyouscreen;
