import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Platform,
  FlatList,
  Alert
} from "react-native";
import AppHeader from '../../components/AppHeader'
import SwitchButton from '../../components/SwitchButton'
import MainBottom from '../../components/MainBottom'
class P_DCommunicationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <SafeAreaView style={styles.complete_view}>
            <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled">
            <AppHeader {...this.props} />
            <View style={styles.headinView}>
                <Text style={styles.loginHeading}>P&D Communications </Text>
                <View style={styles.socialtxt}>
                    <View style={{flexDirection:'row',marginTop:5}}>
                        <Text style={{fontSize:10}}>Posted on<Text style={styles.registrbtn}> 21-05-2021</Text> by</Text><Text style={styles.registrbtn}> Nidheesh kommath</Text>
            </View>
          </View>
            </View>
            
            <View style={styles.container}>
                <Image
                    style={{height:200,width:"80%"}}
                    source={{ uri: 'https://images.unsplash.com/photo-1554415707-6e8cfc93fe23?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bGVhcm5pbmclMjBjb21wdXRlcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&w=1000&q=80' }} />

                <Text style={styles.txt_2}>Lorem ipsum is a name for a common type of placeholder text. Also known as filler or dummy text, this is simply copy that serves to fill a space without actually saying anything meaningful. It's essentially nonsense text, but gives an idea of what real words will look like in the final product.</Text>
            <View style={styles.lines}></View>
            </View>

            <View style={styles.headinView}>
                <Text style={styles.loginHeading}>Contact Person </Text>
                </View>    
                

                <View style={styles.contact_View}>
                    <View>
                        <Image
                        style={{height:40,width:40,paddingHorizontal:"10%",borderRadius:100}}
                        source={{ uri: 'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4.png' }} />
                    </View>
                    <View>
                        <Text>Nidheesh kommath</Text>
                        {/* <Text style={styles.num_txt}>+91 999 888 7766</Text> */}
                    </View>
                    {/* <View>
                        <View style={styles.view_all}><Text style={styles.txt_view_all}>View Profile</Text></View>    
                    </View>        */}
            </View>
                <View style={styles.lines}></View>
                
                <View style={styles.headinView}>
                <Text style={styles.loginHeading}>Salary Information</Text>
                </View> 

                <View style={styles.salary_amt}>
                    <Text>1500 AED</Text></View>
                
                <View style={styles.lines}></View>

               <View style={styles.headinView}>
                <Text style={styles.loginHeading}>Contact Information</Text>
                </View> 

                <View style={styles.switchView}>
                    <Text style={styles.txtSwitch}>Call me</Text>
                    <View style={{marginHorizontal:"49%"}}>
            <SwitchButton 
            toggleSwitch1 = {this.toggleSwitch1}
            switch1Value={this.state.switch1Value} />
            </View>
          </View>

          <View style={styles.switchView2}>
            <Text style={styles.txtSwitch}>Message me </Text>
            <View style={{marginHorizontal:"40%"}}>
            <SwitchButton 
            toggleSwitch1 = {this.toggleSwitch1}
            switch1Value = {this.state.switch1Value}/>
            </View>
          </View>

      </ScrollView>
      <MainBottom {...this.props} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

    complete_view: {
        flex: 1,
        backgroundColor: "white",
    },

    headinView: {
    paddingHorizontal: "10%",
    marginTop: 30,
    marginBottom: 30,
    

  },

  loginHeading: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
    },
  
  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20, 
    },
  
  socialtxt: {
    fontSize:10,
  },

  registrbtn: {
    color: "#070545",
    fontSize: 10,
    top:0,
    fontWeight: "bold"
    
    },
  
    txt_2: {
        paddingHorizontal: "10%",
        paddingVertical: 20,
        color: "grey",
        lineHeight:20,
        fontSize: 12,
        justifyContent: "center",
        alignSelf:"center"
    },
    
    lines: {
        width: "80%",
        backgroundColor: '#d9d1d0',
        height: 1,
        justifyContent: "center",
        alignSelf:"center"
    },

    contact_View: {
        height: 50,
        width:'80%',
        flexDirection: "row",
        backgroundColor: "#fff",
        marginHorizontal: "10%",
        marginBottom:30,
    },

    num_txt: {
        color: "grey",
        fontSize:12
    },

    view_all: {
        width: 90,
        height: 30,
        backgroundColor: "#080f75",
        borderRadius: 15,
        marginHorizontal:"20%"
    },

    txt_view_all: {
        alignSelf: "center",
        justifyContent: "center",
        color: "white",
        fontSize: 10,
        fontWeight:'500',
        paddingVertical:"10%"
    },

    salary_amt: {
        marginHorizontal: "10%",
        marginBottom:30
    },

    switchView: {
        flexDirection: "row",
        paddingHorizontal: 10,
        marginBottom: 30,
    },

    switchView2: {
        flexDirection: "row",
        paddingHorizontal: 10,
        marginBottom: 30,
    
    },
    
    txtSwitch: {
    paddingHorizontal: 15,
    left:10
  },

})

export default P_DCommunicationScreen;
