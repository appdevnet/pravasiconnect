import React, { Component } from 'react';
import { View, Text,SafeAreaView,Modal,TextInput,Dimensions,StyleSheet,ScrollView,TouchableOpacity,FlatList,Image } from 'react-native';
import AppHeader from '../../components/AppHeader';
import MainBottom from '../../components/MainBottom';

import qn from '../../assest/Icons/update/Iconopen-question-mark.png';
import like from '../../assest/Icons/update/Iconfeather-heart.png';

import axios from 'react-native-axios';
import Toast from '../../components/Toast';
import Root from '../../components/Root';

import {connect} from 'react-redux';
import { setLoginData,startLoader,stopLoader,SetUserId,SetToken,setLocation } from '../../redux/action'
import { storeUserData } from '../../utils/asyncStorage'
var qs = require('qs');

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

import AwesomeAlert from 'react-native-awesome-alerts';

class QAscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAlert:false,
      isPostQuestionModel:false,
      question:"",
      uname:true,


     
      

       Option:[
           {id:"1",value:"Recent"},
           {id:"2",value:"Trending"},
           {id:"3",value:"All Questions"}
       ],
       content:[
        //    {id:"1",qicon:require("../../assest/Icons/update/Iconopen-question-mark.png"),desc:"How can i convert visiting visa to job visa ?",
        // answer:"12",likes:"10",licon:require("../../assest/Icons/update/Iconfeather-heart.png"),n:"john Deo"},
        // {id:"2",qicon:require("../../assest/Icons/update/Iconopen-question-mark.png"),desc:"How can i convert visiting visa to job visa ?",
        // answer:"14",likes:"11",licon:require("../../assest/Icons/update/Iconfeather-heart.png"),n:"Bala subhramaniam"}
        
       ]
        
    };
  }

  Getcontent(item) {
    Alert.alert(item);
  }

  getTokenData = async () => {
    try {
      const value = await AsyncStorage.getItem('token')
      const valu = await AsyncStorage.getItem('location')
      console.log(valu)
      console.log(value)
      const val=await JSON.parse(value)
      const va=await JSON.parse(valu)
      if(value !== null) {
        //console.log(val.token,"ttooo")
        //console.log(val.customerid,"idddd")
        console.log("lat",va.latitude)
        console.log("lon",va.longitude)

        this.setState({
          token:val.token,customer_id:val.customerid,
          latitude: va.latitude,
          longitude:va.longitude
        });
        
      }
    } catch(e) {
      // error reading value
    }
  }

  GetQuestion = () => 
  {
    this.props.startLoader()
    const url = "https://pravasiconnect.websitepreview.in/api/get-question"
    
    const params = {
      latitude: this.props.location.latitude,
      longitude: this.props.location.longitude,
      status:1,
    }

    const header = {
      'Content-Type': 'application/json',
      'X-Requested-With': `XMLHttpRequest`,
      Authorization: 'Bearer '+this.props.Token
    }
    
    console.log("moneeeeeeeeee",params)
      axios.post(url,params,header)
      .then((response) => {
        this.props.stopLoader()
        console.log("qngot",response.data.data);

        if(response.data.status==true)
        {
          this.setState({
            content:response.data.data,
            user:this.state.content.user
          })
        }
       
      })
      .catch((error) => {
        console.log(error);
      });


  }
  
  componentDidMount=()=>{

     this.GetQuestion()

    console.log("userid==========",this.props.userID)
    console.log("token===========",this.props.Token)
    console.log("location======",this.props.location)

    this.getTokenData()


    console.log("qa",this.props.loginData)
    //console.log(params)
    
  }


  PostQuestion=()=>
  {
    //const {question}=this.state;

    if(this.state.question=="")
    {
      this.setState({uname:false})
      return false
    }
    else{

      this.props.startLoader()

      const url="https://pravasiconnect.websitepreview.in/api/add-question"

      const params={
        user_id:this.props.userID,
        latitude:this.props.location.latitude,
        longitude:this.props.location.longitude,
        title:this.state.question,
        
        
      }
      

      console.log("paraaaaa",JSON.stringify(params))

      fetch("https://pravasiconnect.websitepreview.in/api/add-question",
       {
         method: 'POST',
         headers: {
         'Content-Type': 'application/json',
         'X-Requested-With': 'XMLHttpRequest',
         Authorization: 'Bearer '+this.props.Token
         },
         body: JSON.stringify(params),
         })
 
         .then((response) => response.json())
         .then((response) => {
           this.props.stopLoader()
         console.log(response);


         if(response.status==true)
         {
            this.GetQuestion()
             {this.setState({showAlert:true,isPostQuestionModel:false})}
         }
         else if(response.status==false){
           this.setState({
             questionerror:response.data.title[0]
           })
         }

        })
        .catch((error) => {
          console.log(error);
        });

    }
    
  }

  render() {
    return (
    
        <SafeAreaView style={{backgroundColor:'#fff',flex:1}}>  
        <AppHeader isNotify = {false} {...this.props} />
        <ScrollView>
      <View style={[styles.headinView]}>
        <Text style={[styles.loginHeading]}> Q&A </Text>

   
       
      <TouchableOpacity style={[styles.BtnStyle]} onPress={()=>this.props.isLoggedIn?this.setState({isPostQuestionModel:true}):this.props.navigation.navigate("login")} >
           <Text style={styles.BtnText}>POST A QUESTION</Text>
       </TouchableOpacity>
      
      </View>
      <FlatList
          horizontal={true}
          data={this.state.Option}
          renderItem={({item}) =>
            <TouchableOpacity style={{marginTop:windowHeight/45,paddingLeft:windowWidth/20,marginLeft:windowWidth/20}}>
                <Text style={{}}>{item.value}</Text>
            </TouchableOpacity>
          }
      />
  
          <FlatList
            inverted
        data={this.state.content}
        renderItem={({item}) =>
        <View style={{flexDirection:"row",marginTop:windowHeight/40,width:windowWidth/1,height:windowHeight/13,borderRadius:5}}>
            <View >
            <Image source={require("../../assest/Icons/update/Iconopen-question-mark.png")} style={{marginTop:windowHeight/60,alignSelf:'center',marginLeft:windowWidth/16,justifyContent:'center',height:windowHeight/90,width:windowHeight/140,}}></Image>
            </View>
            <View style={{flexDirection:'column',justifyContent:'space-between'}}>
            <View style={{marginLeft:windowWidth/30,height:windowHeight/25}}>
            <Text numberOfLines={3} multiline={true} style={{marginTop:windowHeight/80,marginLeft:windowWidth/80,backgroundColor: "#fff",width:windowWidth/1.25,height:windowHeight/10}}
            onPress={()=>this.props.navigation.navigate('Answerscreen',{data:item})}>{item.title + "?"}</Text>
            </View>
           
            <View style={{marginTop:windowHeight/30,marginLeft:windowWidth/20,flexDirection:'row'}}>
            <Text style={{color:'grey'}}>{item.answer_count} Answers</Text>
            <TouchableOpacity style={{flexDirection:'row',justifyContent:'space-around'}}>
            <Image source={require("../../assest/Icons/update/Iconfeather-heart.png")} style={{alignSelf:'center',marginLeft:windowWidth/16,justifyContent:'center',height:windowHeight/70,width:windowHeight/70,}}></Image>
            <Text style={{marginLeft:windowWidth/90,color:'grey'}}>{item.question_like_count}</Text>
            </TouchableOpacity>
            <View style={{width:windowWidth/2}}>
            <Text style={[styles.textstyle]} numberOfLines={1}>{item.created_date}</Text>
            </View>
            </View>
            </View>
        </View>
        }
      />

<AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title="Success!"
          message="Wait for the Approval"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          progressColor="#fff"
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="No, cancel"
          confirmText="submit"
          confirmButtonColor="#0D3369"
          onCancelPressed={() => {
            console.log('pattichee')
          }}
          onConfirmPressed={() => {
            this.setState({showAlert:false})
          }}
        />

<View style={styles.centeredView}>
<Modal            
  animationType = {"slide"}  
  transparent = {true}  
  visible = {this.state.isPostQuestionModel}  
  onRequestClose = {() =>{ console.log("Modal has been closed.") } }>  
  {/*All views of Modal*/}  
      <View style = {styles.modal}>  

      <Text style={{fontSize:13,fontWeight:'bold',marginTop:windowHeight/180,padding:15}}>POST YOUR QUESTION</Text>

      <View style={[styles.inputView,{borderColor:this.state.uname?null:"red",borderWidth:this.state.uname?0:1}]}>
<TextInput
  style={styles.TextInput}
  placeholder={this.state.uname?"Post Your Question":"Please Provide a Question"}
  multiline={true}
  placeholderTextColor={this.state.uname?"#777777":"red"}
  color='black'
  keyboardType='email-address'
  onChangeText={text => this.setState({question:text,uname:true})}
  // onChangeText={(email) => setEmail(email)}
/>

</View>

<View>
    <Text
        numberOfLines={1}
          style={{
             //backgroundColor:"#fff",
            width:windowWidth/1.3,
            fontSize: windowWidth / 35,
            left:windowWidth/36,
            top: -20,
            color: 'red',
            fontWeight:'bold',
            fontSize:12
          }}>
          * {this.state.questionerror}
        </Text>
    </View>
 
     <TouchableOpacity
      style={[styles.button, styles.buttonClose,{marginTop:windowHeight/50}]}
      onPress={()=>this.PostQuestion()}
    ><Text style={styles.textStyle}>SUBMIT</Text></TouchableOpacity> 
    
    <Text style={{color:'black',marginTop:windowHeight/45,fontSize:14,fontWeight:'bold'}}
    onPress = {() => {  
          this.setState({ isPostQuestionModel:!this.state.isPostQuestionModel})}}>CLOSE</Text>
      
  </View>   
</Modal>
</View> 
      
      
      
      </ScrollView>
  <MainBottom {...this.props} />
      </SafeAreaView>
      
    );
  }
}


const styles=StyleSheet.create({
    headinView: {
        marginTop:windowHeight/50,
       marginLeft:windowWidth/15,
       width:windowWidth/2,
    
      },
    
      loginHeading: {
        fontWeight: "bold",
        fontSize: 20,
        color: "black"
      },


 BtnStyle:{
    backgroundColor:'#070545',
    width:windowWidth/3,
    height:windowHeight/32,
    marginLeft:windowWidth/1.9,
    position:'absolute',
    
    borderRadius:60,
    justifyContent:'center',
  },
  
  BtnText:{
    color:'#fff',
    textAlign:'center',
    fontSize:10,
    alignItems:'center',
    fontWeight:'bold',
    justifyContent:'center',
  },

  textstyle:{
    marginLeft:windowWidth/10,
    color:'grey'

  },
  modal: {  
    justifyContent: 'center',  
    alignItems: 'center',   
    backgroundColor : "#fff",   
    height: windowHeight/1.6 ,  
    width: '90%',  
    borderRadius:10,  
    borderWidth: 1,  
    borderColor: '#fff',    
    marginTop: windowHeight/4.4,  
    marginLeft: windowWidth/21,  

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.75,
    shadowRadius: 4,
    elevation: 5
     
     },  

     centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: windowHeight/2
    },

    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        width:windowWidth/3,
      },
      buttonClose: {
        backgroundColor: "#070545",
      },
      textStyle: {
        color: "white",
        textAlign: "center",
        fontWeight:'bold',
        fontSize:13,
      },

      TextInput: {
        height: windowHeight/2.6,
        paddingHorizontal: 5,
        marginHorizontal: 10,
        marginTop:windowWidth/180,
        backgroundColor:"#EBF1FF"
     
      },

      inputView: {
        backgroundColor: "#EBF1FF",
        borderRadius: 12,
        width: "80%",
        height: windowHeight/2.5,
        
        color:'black'
      },
})

//export default QAscreen;

const mapStateToProps = (state) => {
  return {
    isLoggedIn:state.global.isLoggedIn,
    location:state.global.location,
    loginData:state.global.loginData,
    userID:state.global.userID,
    Token:state.global.Token
  };
};

export default connect(mapStateToProps, {setLoginData,startLoader,stopLoader,SetToken,SetUserId,setLocation})(
  QAscreen,
);
