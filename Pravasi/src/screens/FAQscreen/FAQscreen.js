import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Dimensions,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  FlatList,
  Image,
} from 'react-native';
import AppHeader from '../../components/AppHeader';
import MainBottom from '../../components/MainBottom';
import axios from 'react-native-axios';
import qn from '../../assest/Icons/update/Iconopen-question-mark.png';
import like from '../../assest/Icons/update/Iconfeather-heart.png';
import {getFaq} from '../../constants/Network Constants';
import { viedo_icon } from '../../constants/Icons'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class FAQscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: [
        {
          id: '1',
          qicon: require('../../assest/Icons/update/Iconmaterial-ondemand-video.png'),
          desc: 'How can i convert visiting visa to job visa?',
        },
        {id: '2', desc: 'How can i get visiting visa to job visa?'},
        {id: '3', desc: 'Bank Holidays in UAE'},
        {id: '4', desc: 'How can I create a perfect Resume?'},
        {
          id: '4',
          qicon: require('../../assest/Icons/update/Iconmaterial-ondemand-video.png'),
          desc: 'How can i convert visiting visa to job visa?',
        },
      ],
      data: [
        {
          id: 3,
          title: 'How can i get visiting visa to job visa?',
          description: 'des 123',
          video_link: 'www.google.com',
          image: '',
          sort_order: 122,
          status: 1,
          created_at: '2021-09-06T05:39:53.000000Z',
          updated_at: '2021-09-06T15:04:00.000000Z',
          image_link:
            'http://127.0.0.1:8000/storage/faq-images/2vw6geNx9iDlbeM6sCskfx5NTWUwgvy9b6Zaugrs.png',
        },
        {
          id: 4,
          title: 'Bank Holidays in UAE ',
          description: 'des 123',
          video_link: 'www.google.com',
          image: '',
          sort_order: 122,
          status: 1,
          created_at: '2021-09-06T05:39:53.000000Z',
          updated_at: '2021-09-06T15:04:00.000000Z',
          image_link:
            'http://127.0.0.1:8000/storage/faq-images/2vw6geNx9iDlbeM6sCskfx5NTWUwgvy9b6Zaugrs.png',
        },
      ],
    };
  }
  componentDidMount() {
    console.log('reached');
    this.getFaqData();
  }
  getFaqData() {
    const header = {
      'Content-Type': 'application/json',
      'X-Requested-With': `XMLHttpRequest`,
    };

    axios.post(getFaq, header).then(response => {
      if (response.data.status) {
        this.setState({
          data: response.data.data,
        });
      } else {
        console.log('something went wrong');
      }
      console.log(response.data, 'faq data11');
    });
  }
  render() {
    return (
      <SafeAreaView style={{backgroundColor: '#fff', flex: 1}}>
        <AppHeader isNotify={false} {...this.props} />
        <ScrollView>
          <View style={[styles.headinView]}>
            <Text style={[styles.loginHeading]}> FAQ </Text>
          </View>

          <FlatList
            data={this.state.data}
            renderItem={({item}) => (
              <TouchableOpacity
                style={styles.MainView}
                onPress={() =>
                  this.props.navigation.navigate('FAQDetailscreen',{data:item})
                }>
                <View
                  style={{
                    left: windowWidth / 46,
                    height: '100%',
                    width: '100%',
                    flexDirection: 'row',
                  }}>
                  <Text numberOfLines={1} style={styles.faqText}>
                    {' '}
                    {item.title}{' '}
                  </Text>
                  {item.video_link&&(
                  <View
                    style={{flexDirection: 'column', justifyContent: 'center',left:10}}>
                    <Image
                      resizeMode={'contain'}
                      source={viedo_icon}
                      style={{
                        alignSelf: 'center',
                        justifyContent: 'center',
                        height: windowHeight / 60,
                        width: windowHeight / 60,
                      }}></Image>
                  </View>
                  )}
                </View>
              </TouchableOpacity>
            )}
          />
        </ScrollView>
        <MainBottom {...this.props} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  faqText: {
    // fontWeight:'bold',
    color:"black",
    alignSelf: 'center',
    justifyContent: 'center',
    maxWidth: windowWidth / 1.26,
  },
  MainView: {
    width: windowWidth / 1.1,
    alignSelf: 'center',
    flexDirection: 'row',
    marginTop: windowHeight / 150,
    backgroundColor: '#f2f6ff',
    height: windowHeight / 13,
    borderRadius: 10,
  },
  headinView: {
    marginTop: windowHeight / 50,
    marginLeft: windowWidth / 15,
    width: windowWidth / 2,
    paddingBottom: windowWidth / 20,
  },

  loginHeading: {
    fontWeight: 'bold',
    fontSize: 20,
    color: 'black',
  },
});

export default FAQscreen;
