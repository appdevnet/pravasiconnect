import React, { Component } from 'react';
import { View, Text,SafeAreaView,StyleSheet,Image,ScrollView,TouchableOpacity,Dimensions } from 'react-native';
import { require } from 'yargs';
import taxi from '../../assest/Icons/BookACab/delivery_taxi_13.jpg';
import ex from '../../assest/Icons/BookACab/Iconmaterial-report-problem.png';
import call from '../../assest/Icons/BookACab/Iconzocial-call.png';
import pathcall from '../../assest/Icons/BookACab/Path110.png';
import pathemail from '../../assest/Icons/BookACab/Path50.png';
import location from '../../assest/Icons/BookACab/Iconmaterial-my-location.png';
import verify from '../../assest/Icons/BookACab/Path383.png';
import AppHeader from '../../components/AppHeader';
import Callmessage from '../../components/callmessage';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class BookCabscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { navigation } = this.props;
    return (
      
        <SafeAreaView style={{backgroundColor:'#fff',flex:1}}>
        <AppHeader {...this.props}/>
        
        <ScrollView showsVerticalScrollIndicator={false}
               keyboardShouldPersistTaps="handled">
      <View style={{flexDirection:'row'}}>
        <Text style={styles.heading}> Book a Cab </Text>
        <Image source={verify} style={{marginTop:windowHeight/60,marginLeft:windowWidth/40,}}></Image>
        <Text style={{marginTop:windowHeight/72,marginLeft:windowWidth/50,}}>Verified</Text>
      </View>
      <View>
          <Image source={taxi} style={styles.image}></Image>
      </View>
      <View>
          <Text style={styles.content}>Lorem Ipsum is simply dummy text of the printing and
           typesetting industry Lorem Ipsum has been the industry's standard dummy text ever 
           since the 1500s when an unknown printer took a galley of type and scrambled it to make a 
           type specimen book.</Text>
      </View>
      <View>
          <Text style={styles.contact}>Contact Person & Address</Text>
      </View>
      <View style={styles.view2}>
      <View>
      
          <Text style={styles.contact_name}>Rohith Murali</Text>
          <Text style={styles.contact_number}>Vehicle Number</Text>
          </View>
          <View>
          <Image source={ex} style={styles.report1}>
          </Image>
          </View>
      </View>
      <View style={{padding:10}}>
      <Text style={styles.contact}>Contact Information</Text> 
      </View>
      <View style={styles.view1} >
      <View>
          <Image source={pathcall} style={styles.report}>
          </Image>
          </View>
         <View>
          <Text style={styles.contactinfo_name}>Mobile</Text>
          <Text style={styles.contactinfo_number}>+1 2345 678 90</Text>
          </View>
          <View style={{paddingLeft:50,paddingTop:10}}>
          <TouchableOpacity style={styles.callbutton}><Text style={{color:'#fff',justifyContent:'center',alignItems:'center'}}>call</Text></TouchableOpacity>
          </View>
      </View>

      <View style={styles.view1}>
      <View>
          <Image source={pathemail} style={styles.report}>
          </Image>
          </View>
      <View>
          <Text style={styles.contactinfo_name}>Email</Text>
          <Text style={styles.contactinfo_number}>johndeo@gmail.com</Text>
          </View>
          <View style={{paddingLeft:30,paddingTop:10}}>
          <TouchableOpacity style={styles.messagebutton}><Text style={{color:'#fff',justifyContent:'center',alignItems:'center'}}>message</Text></TouchableOpacity>
          </View>
      </View>

      <View style={styles.view1}>
      <View>
          <Image source={location} style={styles.report}>
          </Image>
          </View>
      <View>
          <Text style={styles.contactinfo_name}>Current Location</Text>
          <Text style={styles.contactinfo_number}>Ramanattukara,Calicut</Text>
          </View>
      </View>
      </ScrollView>
      <Callmessage {...this.props}/>
      </SafeAreaView>
    );
  }
}

export default BookCabscreen;


const styles = StyleSheet.create({
    
    heading:{
        fontSize:20,
        fontWeight:'bold',
        marginTop:windowHeight/140,
        marginLeft:windowWidth/18,
        flexDirection: 'row',
    },

    image:{
      width:windowWidth/1.1,
      height:windowHeight/3.4,
     
      marginTop:windowHeight/50,
      marginLeft:windowWidth/20,
      justifyContent:'center',
      alignItems:'center',
    },

    content:{
      alignContent:'center',
      justifyContent:'center',
      padding:windowWidth/20
    },
    
    contact:{
        fontSize:15,
        fontWeight:'bold',
        marginLeft:windowWidth/15,
    },
    contact_name:{
        fontSize:13,
        marginLeft:windowWidth/7.5,
        paddingLeft:windowWidth/15,
        paddingTop:windowHeight/100,
        fontWeight:'bold',
        color:'#2051AE'
    },
    contact_number:{
        fontSize:11,
        marginLeft:windowWidth/5,
    },

    contactinfo_name:{
      fontSize:13,
        fontWeight:'bold',
        color:'#2051AE'
    },

    contactinfo_number:{
      fontSize:11,
      
    },

    // view:{
    //     padding:10,
    //     backgroundColor:'#EBF1FF',
    //     flex:1,
    //     flexDirection: 'row',
    //     marginLeft:20,
    //     marginRight:20,
        
        
    // },

    view1:{
      padding:windowWidth/25,
      backgroundColor:'#EBF1FF',
      flex:1,
      flexDirection: 'row',
      marginLeft:windowWidth/30,
      marginRight:windowWidth/40,
        marginTop:windowHeight/40,
        
    },

    view2:{
        padding:windowWidth/25,
        backgroundColor:'#FFF',
        flex:1,
        flexDirection:'row',
        marginLeft:windowWidth/18,
        marginRight:windowWidth/40,
        
    },
 report:{
  width:windowWidth/30,
  height:windowHeight/40,
  paddingHorizontal: 10,
  paddingVertical: 10,
  margin:'10%',
  
  
 },

 report1:{
  width:windowWidth/30,
  height:windowHeight/40,
  paddingHorizontal: 10,
  paddingVertical: 10,
  margin:'10%',
  marginLeft: windowWidth/5,
  
  
 },

 callbutton:{
  backgroundColor: "#2051AE",
  width:windowWidth/5,
  borderRadius: 60,
  height:windowHeight/40,
  alignItems:'center',
  justifyContent:'center'
  
  
 },

 messagebutton:{
  backgroundColor: "#2051AE",
  width:windowWidth/5,
  height:windowHeight/40,
  borderRadius:60,
  alignItems:'center',
  justifyContent:'center'
 },
   
   
})