import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Platform,
  FlatList,
  Alert,
  Dimensions,
} from 'react-native';
import AppHeader from '../../components/AppHeader';
import SwitchButton from '../../components/SwitchButton';
import MainBottom from '../../components/MainBottom';
import YouTube from 'react-native-youtube';
import { getYoutubeVideoId } from '../../utils/commonMethods'
// import VideoPlayers from 'react-native-video-players';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
class FAQDetailscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    console.log(this.props.route.params.data, 'details of fag');
    if (this.props?.route?.params?.data) {
      this.setState({
        faq: this.props?.route?.params?.data,
      });
    }
  }
  render() {
    return (
      <SafeAreaView style={styles.complete_view}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled">
          <AppHeader {...this.props} />
          <View style={styles.headinView}>
            <Text style={styles.loginHeading}>
              {this.props?.route?.params?.data?.title}
            </Text>
          </View>

          {/* <View style={styles.container}>
                <Text style={styles.txt_2}>Lorem ipsum is a name for a common type of placeholder text. Also known as filler or dummy text, this is simply copy that serves to fill a space without actually saying anything meaningful. It's essentially nonsense text, but gives an idea of what real words will look like in the final product.</Text>
           
            </View> */}
            {this.props.route.params?.data?.video_link!=null && (
          <View style={[styles.videoContainer, {backgroundColor: 'green'}]}>
            <YouTube
                videoId={getYoutubeVideoId(this.props?.route?.params?.data?.video_link)}// The YouTube video ID
                // videoId="CVq_oH0v8rI"
                apiKey="YOUR_API_KEY"
              play ={true} // control playback of video with true/false
              // fullscreen // control whether the video should play in fullscreen or inline
              loop={true} // control whether the video should loop when ended
              onReady={e => this.setState({isReady: true})}
              onChangeState={e => this.setState({status: e.state})}
              onChangeQuality={e => this.setState({quality: e.quality})}
              onError={e => this.setState({error: e.error})}
              style={{height:"100%",width:"100%"}}
            />
          </View>
            )}
          {this.props.route.params?.data?.image_link && this.props.route?.params?.data?.video_link==null&& (
            <View style={styles.container}>
              <Image
                resizeMode={'contain'}
                style={{height: 200, width: '80%', marginTop: 1}}
                source={{uri: this.props.route?.params?.data?.image_link}}
              />
            </View>
          )}

          <View style={styles.headinView}>
            {/* <Text style={{fontSize:15,fontWeight:'bold'}}>Procedures</Text> */}
          </View>
          <View style={styles.container}>
            <Text style={styles.txt_2}>
              {this.props.route.params.data.description}
            </Text>
            <View style={styles.lines}></View>
          </View>
        </ScrollView>
        <MainBottom {...this.props} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  complete_view: {
    flex: 1,
    backgroundColor: 'white',
  },

  headinView: {
    paddingHorizontal: '10%',
    marginTop: 30,
    marginBottom: 5,
    marginLeft: 1,
  },

  loginHeading: {
    fontWeight: 'bold',
    fontSize: 20,
    color: 'black',
  },

  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  videoContainer: {
    top: windowWidth / 20,
    alignSelf: 'center',
    height: windowWidth / 2,
    width: windowWidth / 1.2,
  },

  socialtxt: {
    fontSize: 10,
  },

  registrbtn: {
    color: '#070545',
    fontSize: 10,
    top: 0,
    fontWeight: 'bold',
  },

  txt_2: {
    paddingHorizontal: '10%',
    paddingVertical: 20,
    color: 'grey',
    lineHeight: 20,
    fontSize: 12,
    justifyContent: 'center',
    alignSelf: 'center',
  },

  lines: {
    width: '80%',
    backgroundColor: '#d9d1d0',
    height: 1,
    justifyContent: 'center',
    alignSelf: 'center',
  },

  contact_View: {
    height: 50,
    width: '80%',
    flexDirection: 'row',
    backgroundColor: '#fff',
    marginHorizontal: '10%',
    marginBottom: 30,
  },

  num_txt: {
    color: 'grey',
    fontSize: 12,
  },

  view_all: {
    width: 90,
    height: 30,
    backgroundColor: '#080f75',
    borderRadius: 15,
    marginHorizontal: '20%',
  },

  txt_view_all: {
    alignSelf: 'center',
    justifyContent: 'center',
    color: 'white',
    fontSize: 10,
    fontWeight: '500',
    paddingVertical: '10%',
  },

  salary_amt: {
    marginHorizontal: '10%',
    marginBottom: 30,
  },

  switchView: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginBottom: 30,
  },

  switchView2: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginBottom: 30,
  },

  txtSwitch: {
    paddingHorizontal: 15,
    left: 10,
  },
});

export default FAQDetailscreen;
