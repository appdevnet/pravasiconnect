import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  Dimensions,
  Alert,
  TouchableOpacity,
  SafeAreaView,
  AsyncStorage
} from "react-native";
import AppHeaderBack from '../../components/AppHeaderBack';
import axios from 'axios';



const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class VerifyOTPscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {

      email:"",
      ema:true,
      timer: 2*60,
      
      
    };
  }

  


  componentDidMount(){
     console.log(this.props.route.params.Userid,"kittypoyi")
    this.interval = setInterval(
      () => this.setState((prevState)=> ({ timer: prevState.timer - 1 })),
      1000
      
    );

    
    
  }
  
  componentDidUpdate(){
    if(this.state.timer === 0){ 
      this.props.navigation.navigate('resetpassword')
      clearInterval(this.interval);

    }
  }
  
  componentWillUnmount(){
   clearInterval(this.interval);
  }

  reset=()=>{
    const {email}=this.state;

    if(email=="")
    {
      // alert("The Email is either empty or Wrong")
      this.setState({ema:false});
      return false;
    }
    else
    {

      // alert("Password Resetted");
      // this.props.navigation.replace('login');
      return true;
    }

  }

  Verify_otp=()=>{
     
    const url= "https://pravasiconnect.websitepreview.in/api/forgot-password-otp-verification";

    if(this.reset())
    {
      const params={
        otp:this.state.email,
        customer_id:this.props.route.params.Userid,
      }

      const header = {
        'Content-Type': 'application/json',
        'X-Requested-With': `XMLHttpRequest`,
        }
        console.log(params)
       
        axios.post(url,params,header)
        .then((response) => {
          console.log(response);


          if(response.data.status)
          {
             this.setState({message:response.data.message,
            })
             Alert.alert(this.state.message)
             
             this.props.navigation.navigate('changepassword',{cus_id:this.props.route.params.Userid,
                 otp:this.state.email
            });
            }
            else{
             Alert.alert("OTP Verification Failed");
    
            }
  
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }





  render() {
    const { navigation } = this.props;

    //const id=navigation.getParam('userid');
    
    return (
      <SafeAreaView style={{backgroundColor:'#fff',flex:1}}>
      <AppHeaderBack {...this.props} />
        <View style={[styles.headinView,{flexDirection:'row'}]}>
          <Text style={styles.resetHeading}>Verify OTP</Text>
          <Text style={{marginLeft:windowWidth/4,marginTop:windowHeight/110}}>Verify Within: {this.state.timer}s</Text>
        </View>

        <View style={styles.container}>
          <View style={[styles.inputView,{borderColor:this.state.ema?null:"red",borderWidth:this.state.ema?0:1}]}>
        <TextInput
          style={styles.TextInput}
          placeholder={this.state.ema?"OTP":"Provide the OTP"}
          placeholderTextColor={this.state.ema?"#A5A5A5":"red"}
          color='black'
          keyboardType='email-address'
          onChangeText={text => this.setState({email:text,ema:true})}
          
        />
          </View>
          
          <TouchableOpacity style={styles.loginBtn}
          onPress={()=> this.Verify_otp()}>
        <Text style={styles.loginText}>VERIFY</Text>
          </TouchableOpacity>
          

        <View style={[styles.socialtxt]}>
        {/* <TouchableOpacity onPress={() => navigation.navigate('login')}>
            <Text>Back to <TouchableOpacity
            onPress={() => navigation.navigate('login')} >
              <Text style={styles.registrbtn}>SIGN IN</Text></TouchableOpacity></Text></TouchableOpacity> */}
          </View>

        
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headinView: {
    paddingHorizontal: windowWidth/20,
    marginTop: windowHeight/70,
    marginBottom: windowHeight/35,
    marginHorizontal: windowWidth/20
  },

  resetHeading: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
  },

  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: windowHeight/20,
    
  },

  inputView: {
    backgroundColor: "#EBF1FF",
    borderRadius: 12,
    width: windowWidth/1.1,
    height: windowHeight/15,
    marginBottom: windowHeight/20,

  },
 
  TextInput: {
    height: windowHeight/15,
    paddingHorizontal: 10,
    marginHorizontal: 10,
  },

  loginBtn: {
    width: windowWidth/1.2,
    borderRadius: 20,
    height: windowHeight/16,
    alignSelf: "center",
    justifyContent: "center",
    marginTop: windowHeight/50,
    backgroundColor: "#070545",
    
  },

  loginText: {
    justifyContent: "center",
    alignSelf: "center",
    fontSize: 17,
    fontWeight: "bold",
    color: "white"
  },

  socialtxt: {
    paddingTop: windowHeight/2.6,
    fontSize:15,
  },

  registrbtn: {
    color: "#070545",
    fontSize: 15,
    top:3,
    fontWeight: "bold"
    
  }
});
export default VerifyOTPscreen;
