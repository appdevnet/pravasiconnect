import React, { Component } from 'react';
import { View, Text,SafeAreaView,StyleSheet,Image, } from 'react-native';
import { Dimensions } from 'react-native';
import img from '../../assest/Icons/Navigation/splash1.png'
import { isLoggedIn,getLocation,getUserData,getUserId,getUserToken } from '../../utils/asyncStorage'
import {connect} from 'react-redux';
import { setLocation,setIsLoggedin,setLoginData,SetUserId,SetToken } from '../../redux/action'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
  
    setTimeout(() => {
        this.initialSetup()
    }, 1000);
  }
  async initialSetup(){
    await isLoggedIn().then((response) => {
      console.log(response,"async get value")
      if(response=='true'){
        this.props.setIsLoggedin(true)
         getUserData().then((response)=>{
          console.log("userdata from async splash",response)
            this.props.setLoginData(response)
        })
         
        getUserId().then((response) =>{
          console.log("userid from asyn",response)
          this.props.SetUserId(response)
        })

        getUserToken().then((response)=>{
          console.log("usertoken from async",response)
          this.props.SetToken(response)
        })

        getLocation().then((response)=>{
          this.props.setLocation(response);
          this.props.navigation.replace("abroadservice");
        })
      }
      else{
        getLocation().then((response)=>{
          if(response!=null&&response!=''){
            this.props.setLocation(response);
            this.props.navigation.replace("abroadservice");
          }
          else{
            this.props.navigation.replace("mapscreen");
          }
        })
      }
    })
  }

  render() {
    return (
      
      <View style={{flex:1,justifyContent:'center'}}>
      <Image source={img} resizeMode="cover" style={{height:windowHeight,width:windowWidth}}
    >
      </Image>
        {/* <Text style={styles.title}>PRAVASI</Text>
        <Text style={styles.connect}>CONNECT</Text>
        <Text style={styles.tap_to_login} onPress={()=>this.props.navigation.navigate('mapscreen')}>Tap to Login</Text> */}
      </View>
      
    );
  }
}



const styles = StyleSheet.create({

  container:{
    flex:1,
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'center',


},
title:{
    textAlign: 'center',
    color: 'black',
    fontSize: 30,
    fontWeight: 'bold',
    marginTop:300,
    
},

tap_to_login:{
  textAlign:'center',
  color:'black',
  fontSize:15,
  marginTop:380,
},
connect:{
  textAlign:'center',
  fontSize:20,
}

})
const mapStateToProps = (state) => {
  return {
  };
};

export default connect(mapStateToProps, {setLocation,setIsLoggedin,setLoginData,SetUserId,SetToken})(
  SplashScreen,
);