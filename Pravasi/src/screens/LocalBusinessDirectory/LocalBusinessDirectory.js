import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView
} from "react-native";
import RadioButton from '../../components/RadioButton'
import SwitchButton from '../../components/SwitchButton'
import AppHeader from '../../components/AppHeader';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const PROP = [
  {
    key: 'Call',
    text: 'Call'
  },
  {
    key: 'Message',
    text: 'Message'
  }
]

class LocalBusinessDirectory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      switch1Value: false,

      businessname:"",
      business:true,
      description:"",
      desc:true,
      city:"",
      ci:true,
      address:"",
      addr:true,
      contactpersonnumber:"",
      contactperson:true,
      contactemail:"",
      conemail:true,
      contactnumber:"",
      connumber:true,
      uploadimg:"",
      upimg:true

    };
  }

  validate_localbusiness=()=>{
    const{businessname,description,city,address,contactpersonnumber,contactemail,contactnumber,uploadimg}=this.state;

    if(businessname=="")
    {
      alert("The Business Name is either empty or Wrong");
      this.setState({business:false})
      return false;
    }
    else if(description=="")
    {
      alert("The Description is either empty or Wrong");
      this.setState({desc:false});
      return false;
    }
    else if (city=="")
    {
      alert("The City is either empty or Wrong");
      this.setState({ci:false});
      return false;
    }
    
    else if (address=="")
    {
      alert("The Address is either empty or Wrong");
      this.setState({addr:false});
      return false;
    }
    else if (contactpersonnumber=="")
    {
      alert("The Person Contact Number is either empty or Wrong");
      this.setState({contactperson:false});
      return false;
    }
    else if(contactemail=="")
    {
      alert("The Contact Email is either empty or Wrong");
      this.setState({conemail:false});
      return false;
    }
    else if (contactnumber=="")
    {
      alert("The Contact Number is either empty or Wrong");
      this.setState({connumber:false});
      return false;
    }
    else if (uploadimg=="")
    {
      alert("The Image is either empty or Wrong");
      this.setState({upimg:false});
      return false;
    }
    else 
    {
      //alert("Registration Success");
    this.props.navigation.replace('thankyouscreen')
    }
  }

  toggleSwitch1 = (value) => {
      this.setState({switch1Value: value})
      console.log('Switch 1 is: ' + value)
   }

  render() {
    return (
      <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
      <AppHeader {...this.props} />
        <ScrollView
        showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled">
          
          <View style={styles.headinView}>
          <Text style={styles.LocalBusinessDirectory}>Local Business Directory</Text>
          </View>

          <View style={styles.container}>

            <View style={[styles.inputView,{borderColor:this.state.business?null:"red",borderWidth:this.state.business?0:1}]}>
        <TextInput
          style={styles.TextInput}
          placeholder="Business Name"
          placeholderTextColor="#424141"
          onChangeText={text => this.setState({businessname:text,business:true})}
          
        />
            </View>

          <View style={[styles.descriotion,{borderColor:this.state.desc?null:"red",borderWidth:this.state.desc?0:1}]}>
        <TextInput
          style={styles.TextInput}
          placeholder="Description"
          placeholderTextColor="#424141"
          onChangeText={text => this.setState({description:text,desc:true})}
          
        />
            </View>
            
          <View style={[styles.inputView,{borderColor:this.state.ci?null:"red",borderWidth:this.state.ci?0:1}]}>
        <TextInput
          style={styles.TextInput}
          placeholder="City"
          placeholderTextColor="#424141"
          onChangeText={text => this.setState({city:text,ci:true})}
          
        />
            </View>
            
          <View style={[styles.inputView,{borderColor:this.state.addr?null:"red",borderWidth:this.state.addr?0:1}]}>
        <TextInput
          style={styles.TextInput}
          placeholder="Address"
          placeholderTextColor="#424141"
          onChangeText={text => this.setState({address:text,addr:true})}
          
        />
            </View>  


          <View style={[styles.inputView,{borderColor:this.state.contactperson?null:"red",borderWidth:this.state.contactperson?0:1}]}>
        <TextInput
          style={styles.TextInput}
          placeholder="Contact Person Number"
          placeholderTextColor="#424141"
          onChangeText={text => this.setState({contactpersonnumber:text,contactperson:true})}
          
        />
            </View>
            

          <View style={[styles.inputView,{borderColor:this.state.conemail?null:"red",borderWidth:this.state.conemail?0:1}]}>
        <TextInput
          style={styles.TextInput}
          placeholder="Contact Email "
          placeholderTextColor="#424141"
          onChangeText={text => this.setState({contactemail:text,conemail:true})}
          
        />
            </View>
            

          <View style={[styles.inputView,{borderColor:this.setState.connumber?null:"red",borderWidth:this.state.connumber?0:1}]}>
        <TextInput
          style={styles.TextInput}
          placeholder="Contact Number "
          placeholderTextColor="#424141"
          onChangeText={text => this.setState({contactnumber:text,connumber:true})}
          
        />
            </View> 
            
          <View style={[styles.inputView,{borderColor:this.setState.upimg?null:"red",borderWidth:this.state.upimg?0:1}]}>
        <TextInput
          style={styles.TextInput}
          placeholder="Upload Image "
          placeholderTextColor="#424141"
          onChangeText={text => this.setState({uploadimg:text,upimg:true})}
          
        />
            </View>  

          </View>

          <View style={styles.ContactheadinView}>
          <Text style={styles.ContactPerson}>Contact Method</Text>
          </View>

          <View style={{marginLeft:windowWidth/18}}>
            <RadioButton PROP={ PROP }/>
          </View>

          <View style={styles.PrivacyheadinView}>
          <Text style={styles.Privacy}>Privacy</Text>
          </View>

          <View style={styles.switchView}>
            <Text style={styles.txtSwitch}>Show Phone Number in Profile</Text>
            <SwitchButton
            toggleSwitch1 = {this.toggleSwitch1}
            switch1Value = {this.state.switch1Value}/>
          </View>

          <View style={styles.switchView}>
            <Text style={styles.txtSwitch}>Show Email Address in Profile </Text>
            <SwitchButton
            toggleSwitch1 = {this.toggleSwitch1}
            switch1Value = {this.state.switch1Value}/>
          </View>

          <TouchableOpacity onPress={()=>this.validate_localbusiness()}>
          <View style={styles.lcl_bsns_d_regstr_Btn}>
            <Text style={styles.lcl_bsns_d_regstr_txt}>REGISTER NOW</Text>
            </View>
          </TouchableOpacity>



        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headinView: {
    paddingHorizontal: windowWidth/10,
    marginTop: windowHeight/100,
    marginBottom: windowWidth/20,
  },

  LocalBusinessDirectory: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
  },

  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: windowHeight/45, 
  },

  descriotion: {
    backgroundColor: "#EBF1FF",
    borderRadius: 12,
    width: windowWidth/1.2,
    height: windowHeight/7,
    marginBottom: windowHeight/50,
  },

  inputView: {
    backgroundColor: "#EBF1FF",
    borderRadius: 12,
    width: windowWidth/1.2,
    height: windowHeight/16,
    marginBottom: windowWidth/23,
  },
 
  TextInput: {
    height: windowHeight/16,
    paddingHorizontal: windowWidth/24,
  },

  ContactheadinView: {
    marginTop: windowHeight/45,
    marginBottom: windowHeight/45,
    marginHorizontal: windowWidth/14
  },

  ContactPerson: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
  },

  switchView: {
    flexDirection: "row",
    
    justifyContent:"space-around",
    marginBottom:windowHeight/35
  },

  PrivacyheadinView: {
    marginTop: windowHeight/45,
    marginBottom: windowHeight/45,
    marginHorizontal: windowWidth/14
  },

  Privacy: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
  },

  txtSwitch: {
    paddingHorizontal: windowWidth/30,
    left:windowWidth/15
  },

  lcl_bsns_d_regstr_Btn: {
    width: windowWidth/1,
    height: windowHeight/13,
    backgroundColor:"#070545"
  },

  lcl_bsns_d_regstr_txt: {
    fontSize: 20,
    fontWeight: "bold",
    alignSelf: "center",
    paddingVertical:windowHeight/45,
    color:"white"
  }

});

export default LocalBusinessDirectory;