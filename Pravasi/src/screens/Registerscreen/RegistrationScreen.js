import React, { Component } from 'react';
import CheckBox from '@react-native-community/checkbox';
import RNPickerSelect from 'react-native-picker-select';
import eye from '../../assest/Icons/Login/eye.png';
import hide from '../../assest/Icons/Login/hide.png';
import CountryJSON from "../../countrycode/CountryPicker/countries.json";
import CountryPicker from 'rn-country-picker';
import { TextInputBox,PasswordInput,PhonenumberInput } from '../../components';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView,
  AsyncStorage,
  Dimensions,
  Alert,
  ScrollView
} from "react-native";
import {connect} from 'react-redux';
import {startLoader,stopLoader} from '../../redux/action'
import AppHeaderBack from '../../components/AppHeaderBack';
import axios from 'axios';
import AppHeader from '../../components/AppHeader';
import AwesomeAlert from 'react-native-awesome-alerts';


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class RegistrationScreen extends Component {
  
  constructor(props) {
    super(props);
    this.state = {    
      country:"",
      name:"",
      mobile:"",
      email:"",
      password:"",
      coun:true,
      nam:"",
      mob:true,
      ema:true,
      pass:true,
      showpassword:false,
      axios_token:"",
      axios_name:"",
      message:"",
      check:false,
      phonenumber:"",
      mCountryCode: "+91",
     isvalid:true,
     nameError:"",
     emailError:"",
    pswderror:"",
     mobError:"",
     ShowAlert:false,
    };

    const userLocaleCountryCode = "";
    // userLocaleCountryCode = DeviceInfo.getDeviceCountry();
 
     try {
       if (userLocaleCountryCode) {
         const newData = CountryJSON.filter(function(item) {
           const itemData = item.name.common.toUpperCase();
           const textData = userLocaleCountryCode.toUpperCase();
           return itemData.startsWith(textData);
         });
         if (newData.length > 0) {
           this.state.mCountryCode = newData[0].callingCode;
         } else {
           this.setState({ mCountryCode: "91" });
         }
       } else {
         this.setState({ mCountryCode: "91" });
       }
     } catch (e) {
       console.error(e.message);
     }
  }

  _selectedValue = index => {
    this.setState({ mCountryCode: index });
  };

 
  


  getData = async () => {
    try {
      const value = await AsyncStorage.getItem('location')
      console.log(value)
      const loc=JSON.parse(value)
      if(value !== null) {
        console.log(loc.place,value)
        this.setState({location:loc,
          country:loc.place,
          latitude:loc.latitude,
          longitude:loc.longitude,
        })
      }
    } catch(e) {
      // error reading value
    }
  }
  
  
    componentDidMount=()=>{
        this.getData()
    }


  reg_validate= async () => {
  
    const {country,name,mobile,email,password,check}=this.state;
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    
   if(country=="")
   {
     //alert("The Country is either empty or Wrong");
     this.setState({coun:false,isvalid:false});
    //  return false;
     
   }
    if(name=="")
   {
    //  this.requiredValidation()
     //alert("The Name is either empty or Wrong");
     
     this.setState({nam:false,nameError:"Please Provide Name",isvalid:false});
    //  return false;
   }
  //  else if(mobile=="")
  //  {
  //    alert("The Mobile is either empty or Wrong");
  //    this.setState({mob:false});
  //    return false;
  //  }
  //  else if(mobile.length<10)
  //  {
  //    alert("The Mobile is Wrong");
  //    return false;
  //  }
    if(email==""&&mobile=="")
   {
     //alert("The Email is either empty or Wrong");
     this.setState({ema:false,mob:false,isvalid:false,emailError:"Please Provide Email Address",mobError:"Please Provide Number "});
    //  return false;
    }
    if (this.state.email) {
      if (reg.test(this.state.email) == false) {
        this.setState({ ema: false, isvalid: false,emailError:"Enter valid email" });
        // return false;
      }
    }
   
    if(password=="")
   {
     //alert("+"+this.state.mCountryCode+this.state.mobile)
     //alert("The Password is either empty or Wrong");
     this.setState({pass:false,isvalid:false,pswderror:"Please Provide  Password"})
    //  return false;
   }
    

  }
  
validate = async () =>{
  await this.reg_validate()
  .then(()=>{
    console.log(this.state.isvalid,"lolol")
    if(!this.state.isvalid){
      this.setState({isvalid:true})
    }
    else{
      this.submit()
    }
  })
}
submit=()=>{
  const url = "https://pravasiconnect.websitepreview.in/api/register"
  console.log("success",this.reg_validate());
  if (this.reg_validate()) {
    if(!this.state.check){
      this.setState({showAlert:true})
      return
    }
    this.props.startLoader()
    
  const  params={
      latitude:this.state.latitude,
      Longitude:this.state.longitude,
      name:this.state.name,
      mobile:this.state.mobile,
      mobile_prefix:this.state.mCountryCode,
      email:this.state.email,
      password:this.state.password,
    }

    const header = {
      'Content-Type': 'application/json',
      'X-Requested-With': `XMLHttpRequest`,
      }
      console.log(JSON.stringify(params))
      axios.post(url,params,header)
      .then((response) => {
        console.log(response);

       this.props.stopLoader()

  
        if(response.data.status)
        {
           //console.log(response.data.data,"ll")
        this.setState({axios_name:response.data.data.name,
          axios_token:response.data.data.token,
          message:response.data.message,
          otp:response.data.data.otp
        })
        //console.log(axios_name)
        //Alert.alert("OTP send to your Mobile or Email");
        this.props.navigation.navigate('verifyotpreg',{Userid:this.state.axios_token});
        }
        else{
         const error = response.data.data
          if(error.name||error?.password||error?.email||error?.mobile_prefix||error?.mobile){
            this.setState({
              nameError:error.name?error?.name[0]:"",
              pswderror:error?.password?error?.password[0]:"",
              emailError:error?.email?error?.email[0]:"",
              mobError:error?.mobile_prefix?error?.mobile_prefix[0]:"",
              mobError:error?.mobile?error?.mobile[0]:""
            })
          }
          else{
            Alert.alert("Registration Failed");
          }
        }
        
      })
        .catch((error) => {
        Alert.alert("Registration Failed");
        console.log(error,"errorrR");
      });
      
     
  } else {
    this.setState(
      {isvalid:true}
    )
  }


}

errorHandling(){

}



  render() {
    const { navigation } = this.props;
    return (
      <SafeAreaView style={{backgroundColor:'#fff',flex:1,}}>
      <AppHeaderBack {...this.props} />
        <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled">
        <View style={styles.headinView}>
          <Text style={styles.registerHeading}>Register</Text>
          </View>

          <View style={styles.container}>

            <View style={[styles.inputView,{borderColor:this.state.coun?null:"red",borderWidth:this.state.coun?0:1}]}>
        <TextInput
          style={[styles.TextInput]}
          value={this.state.country}
          placeholder="Choose Residing Country"
          placeholderTextColor="#777777"
          onChangeText={text => this.setState({country:text,coun:true})}
          
        />
        
            </View>
            <TextInputBox
            value={this.state.name}
            error={this.state.nameError}
            placeholder={"Your Full name"}
            onChangeText={(text)=>{this.setState({name:text,nameError:""})}}
            />
            <PhonenumberInput
            error={this.state.mobError}
            code={this.state.mCountryCode}
            value={this.state.mobile}
            isValidPhone={this.state.isValidPhone}
            selectedCode={(code)=>{this.setState({mCountryCode:code})}}
            onChangeText={(text)=>{this.setState({mobile:text,mobError:"",emailError:""})}}
            />
            <TextInputBox
            value={this.state.email}
            error={this.state.emailError}
            placeholder={"Email Address"}
            onChangeText={(text)=>{this.setState({email:text,emailError:"",mobError:""})}}
            />
            <PasswordInput
            value={this.state.password}
            error={this.state.pswderror}
            placeholder={"Password"}
            onChangeText={(text)=>{this.setState({password:text,pswderror:""})}}
            />
            <View style={{flexDirection:'row',marginTop:-5}}>
                        
              <CheckBox style={{marginLeft:windowWidth/29,marginTop:windowHeight/240}}
                onValueChange={() => this.setState({ check: !this.state.check })}
                tintColors={'#aaaaaa'}
                value={this.state.check}
              >
                
              </CheckBox>
              <Text style={[styles.trmsTxt,{fontSize:15,marginTop:windowHeight/75,marginLeft:windowWidth/27}]}>Accept our Terms & Conditions</Text> 

                      
            </View>
           
            
            <TouchableOpacity style={styles.loginBtn} onPress={()=>this.validate()}>
        <Text style={styles.loginText}>REGISTER NOW</Text>
            </TouchableOpacity>
            
            <View style={[styles.socialtxt,]}>
            <TouchableOpacity onPress={() => navigation.navigate('login')}>
              <Text>Already have an account ? <TouchableOpacity
              onPress={() => navigation.navigate('login')}>
                <Text style={styles.registrbtn}>LOGIN NOW</Text></TouchableOpacity></Text></TouchableOpacity>
          </View>

          <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title="Oops"
          message="Please Accept Terms and Conditions"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          progressColor="#fff"
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="No, cancel"
          confirmText="CONFIRM"
          confirmButtonColor="#0D3369"
          onCancelPressed={() => {
            console.log('pattichee')
          }}
          onConfirmPressed={() => {
            this.setState({showAlert:false})
          }}
        />

          </View>
          </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headinView: {
    paddingHorizontal: windowWidth/37,
    marginTop: windowHeight/70,
    marginBottom: windowHeight/45,
    marginHorizontal: windowWidth/16
  },

  pickerTitleStyle: {
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
    textAlign: "center",
    alignSelf: "center",
    fontWeight: "bold",
    flex: 1,
    marginTop: 5,
    fontSize: 15,
    color: "#000",
  },
  pickerStyle: {
    height: 45,
    width: 115,
    marginBottom:10,
    justifyContent: "center",
    padding: 10,
    borderWidth: 2,
    borderColor: "#EBF1FF",
    backgroundColor: "#EBF1FF"
  },
  selectedCountryTextStyle: {
    paddingLeft: 5,
    paddingRight: 5,
    color: "#000",
    textAlign: "right",
  },

  countryNameTextStyle: {
    paddingLeft: 10,
    color: "#000",
    textAlign: "right"
  },
  searchBarStyle: {
    flex: 1,
    borderRadius: 50,
    borderWidth: 4,

    borderColor: "#D3D3D3",
    justifyContent: "center",
    flexDirection: "row",
    marginTop: 10,
    marginLeft: 8,
    marginBottom: 5,
    marginRight: 12,
    paddingLeft: 20,
    paddingRight: 10
  },

 

  registerHeading: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
  },

  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: windowHeight/40,
    
  },

  inputView: {
    backgroundColor: "#EBF1FF",
    borderRadius: 12,
    width: windowWidth/1.2,
    height: windowHeight/15,
    marginBottom: windowWidth/15,

  },
 
  TextInput: {
    width:windowWidth/1.4,
    height: windowHeight/15,
    paddingHorizontal: 10,
    marginHorizontal: 10,
    color:'black'
    //backgroundColor:"red"
  },
  TextInput2: {
    width:windowWidth/1.4,
    height: windowHeight/15,
    paddingHorizontal: 0,
    marginHorizontal: 10,
    color:'black'
    //backgroundColor:"red"
  },

  trmsTxt: {
    color: "#000",
    
  },

  loginBtn: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignSelf: "center",
    justifyContent: "center",
    marginTop: 10,
    backgroundColor: "#070545",
    
  },

  loginText: {
    justifyContent: "center",
    alignSelf: "center",
    fontSize: 17,
    fontWeight: "bold",
    color: "white"
  },

  socialtxt: {
    paddingTop: windowHeight/40,
    fontSize:15,
  },

  registrbtn: {
    color: "#070545",
    fontSize: 15,
    top:3,
    fontWeight: "bold"
    
  }
});

// export default RegistrationScreen;
const mapStateToProps = (state) => {
  return {
    isLoggedIn:state.global.isLoggedIn,
    location:state.global.location,
    loginData:state.global.loginData
  };
};

export default connect(mapStateToProps, {startLoader,stopLoader})(
  RegistrationScreen,
);