import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Alert,
  Pressable,
} from 'react-native';
import AppHeader from '../../components/AppHeader';
import img from '../../assest/Icons/Profile/Iconmaterial-edit.png';
import CheckBox from '@react-native-community/checkbox';
import {TextInputBox, PhonenumberInput} from '../../components';
import {startLoader, stopLoader} from '../../redux/action';
import {createPost} from '../../constants/Network Constants';
import {connect} from 'react-redux';
import { setFormMap } from '../../redux/action'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class CreatePostScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      switch1Value: false,

      title: '',
      titleError: '',
      description: '',
      descriptionError: '',
      location: '',
      locationError: "",
      mobile: '',
      contactperson: '',
      contactemail: '',
      contactemailError: '',
      websitelink: '',
      weblinkError: '',
      price: '',
      priceError: '',
      CallCheck: true,
      MessageCheck: true,
      BothCheck: false,
      Callalert: 'Call',
      Messagealert: 'Message',
      Bothalert: 'Both',
      Check: '',
      contactmethod: 'both',
      isValidData: true,
    };
  }

  BothCheckchange = () => {
    if (!this.state.BothCheck) {
      this.setState({contactmethod: 'both'});
    } else {
      this.setState({contactmethod: ''});
    }
    console.log('inside', this.state.BothCheck);
    this.setState({BothCheck: !this.state.BothCheck});
  };

  createpost_validation = () => {

    const {
      category,
      subcategory,
      title,
      description,
      location,
      contactemail,
    } = this.state;
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (title == '') {
      this.setState({
        titleError: 'Title Field is Required',
        isValidData: false,
      });
    }
    if (description == '') {
      // alert("description is empty!");
      this.setState({
        descriptionError: 'Description Field is Required',
        isValidData: false,
      });
    }
    if (this.state.mobile == '') {
      this.setState({
        contactperson: 'Enter Contact number',
        isValidData: false,
      });
    }
    if (contactemail == '') {
      this.setState({
        contactemailError: 'Email is Required',
        isValidData: false,
      });
    }
    if (reg.test(this.state.contactemail) == false) {
      this.setState({
        contactemailError: 'Enter Valid Email',
        isValidData: false,
      });
    }
    if (this.state.CallCheck == false && this.state.MessageCheck == false) {
      this.setState({isValidData: false});
      Alert.alert('Contact Method is unspecified');
    } 
  };
  navigateToimageUpload = async ()=>{
    await this.createpost_validation()
      if(this.state.isValidData){
        data = {
          category: this.props.route.params.main_category_id,
          subcategory: this.props.route.params.sub_category_id,
          post_title: this.state.title,
          post_description: this.state.description,
          latitude: this.props.formMap.latitude,
          longitude: this.props.formMap.longitude,
          contact_number: this.state.mobile,
          contact_email: this.state.contactemail,
          website: this.state.websitelink,
          cost: this.state.price,
          method: this.state.contactmethod,
        };
      console.log(data,"create post passing")
      this.props.navigation.navigate("CreatePostImages",{data:data})
      }
      else{
        this.setState({
          isValidData:true
        })
      }
  }
  returnData(details) {
    console.log(details, 'dataaaa');
  }
  componentDidMount() {
    console.log(this.props.location);
    this.props.setFormMap(this.props.location)
    // this.setState({
    //      title: "title post",
    //       description: "post description",
    //       mobile: "8989898976",
    //       contactemail: "v@gmail.com",
    // })
  }

  
  render() {
    return (
      <>
        <SafeAreaView style={{backgroundColor: '#fff', flex: 1}}>
          <AppHeader {...this.props} />
          <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled">
            <View style={styles.headinView}>
              <Text style={styles.CreatePost}>Post Information</Text>
            </View>

            <View style={styles.container}>
              <TextInputBox
                value={this.state.title}
                error={this.state.titleError}
                placeholder={'Title'}
                onChangeText={text => {
                  this.setState({title: text, titleError: ''});
                }}
              />

              <TextInputBox
                multiline={true}
                value={this.state.description}
                error={this.state.descriptionError}
                placeholder={'Description'}
                onChangeText={text => {
                  this.setState({description: text, descriptionError: ''});
                }}
              />
              
          <Pressable>
            <TextInputBox
            //onFocus={()=> this.props.navigation.navigate('mapscreen',{isFormMap:true})}
               editable ={false}
                value={this.props.formMap.place}
                  error={this.state.locationError}
                  
                placeholder={'choose location'}
                onChangeText={(text) => {
                  this.setState({locationError: '',location:text})
                  // this.props.navigation.navigate('location')
                  //this.props.navigation.navigate('mapscreen',{isFormMap:true,},{...this.props}
                }}
              />
              </Pressable>
            </View>

            <View style={styles.headinView}>
              <Text style={styles.CreatePost}>Contact Information</Text>
            </View>

            <View style={styles.container}>
              <PhonenumberInput
                error={this.state.contactperson}
                code={this.state.mCountryCode}
                value={this.state.mobile}
                isValidPhone={this.state.isValidPhone}
                selectedCode={code => {
                  this.setState({mCountryCode: code});
                }}
                onChangeText={text => {
                  this.setState({mobile: text, contactperson: ''});
                }}
              />

              <TextInputBox
                value={this.state.contactemail}
                error={this.state.contactemailError}
                placeholder={'Email'}
                onChangeText={text => {
                  this.setState({contactemail: text, contactemailError: ''});
                }}
              />

              <TextInputBox
                value={this.state.websitelink}
                error={this.state.weblinkError}
                placeholder={'Website Link'}
                onChangeText={text => {
                  this.setState({websitelink: text, weblinkError: ''});
                }}
              />

              <TextInputBox
                value={this.state.price}
                error={this.state.priceError}
                placeholder={'Price'}
                onChangeText={text => {
                  this.setState({price: text, priceError: ''});
                }}
              />
            </View>

            <View style={styles.ContactheadinView}>
              <Text style={styles.ContactPerson}>Contact Method</Text>
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckBox
                style={{
                  marginLeft: windowWidth / 10,
                  marginTop: windowHeight / 250,
                }}
                tintColors={{true: '#0D3369', false: 'black'}}
                onValueChange={() => {
                  if (!this.state.CallCheck) {
                    this.setState({contactmethod:this.state.MessageCheck? 'both':"call"});
                  }
                   else {
                    this.setState({contactmethod:this.state.MessageCheck?'message': ''});
                  }
                  this.setState({CallCheck: !this.state.CallCheck})
                }}
                value={this.state.CallCheck}>

                </CheckBox>
              <Text
                style={[
                  styles.trmsTxt,
                  {
                    fontSize: 15,
                    marginTop: windowHeight / 70,
                    marginLeft: windowWidth / 35,
                  },
                ]}>
                Phone
              </Text>

              <CheckBox
                style={{
                  marginLeft: windowWidth / 10,
                  marginTop: windowHeight / 250,
                }}
                tintColors={{true: '#0D3369', false: 'black'}}
                onValueChange={() => {
                  if (!this.state.MessageCheck) {
                    this.setState({contactmethod:this.state.CallCheck? 'both':"message"});
                  }
                   else {
                    this.setState({contactmethod:this.state.CallCheck?'call': ''});
                  }
                  this.setState({MessageCheck: !this.state.MessageCheck});
                }}
                value={this.state.MessageCheck}></CheckBox>
              <Text
                style={[
                  styles.trmsTxt,
                  {
                    fontSize: 15,
                    marginTop: windowHeight / 70,
                    marginLeft: windowWidth / 35,
                  },
                ]}>
                Message
              </Text>
            </View>

            <TouchableOpacity onPress={() => this.navigateToimageUpload()}>
              <View style={styles.create_post_btn}>
                <Text style={styles.create_post_txt}>NEXT</Text>
              </View>
            </TouchableOpacity>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  headinView: {
    paddingHorizontal: windowWidth / 10,
    marginTop: windowHeight / 100,
    marginBottom: windowWidth / 20,
  },

  pickerIcon: {
    width: windowWidth / 20,
    height: windowHeight / 130,
    resizeMode: 'contain',
    marginTop: windowHeight / 34,
    marginRight: windowWidth / 15,
  },

  CreatePost: {
    fontWeight: 'bold',
    fontSize: 20,
    color: 'black',
  },

  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: windowHeight / 45,
  },

  descriotion: {
    backgroundColor: '#EBF1FF',
    borderRadius: 12,
    width: windowWidth / 1.2,
    height: windowHeight / 7,
    marginBottom: windowHeight / 50,
  },

  inputView: {
    backgroundColor: '#EBF1FF',
    justifyContent: 'center',
    borderRadius: 12,
    width: windowWidth / 1.2,
    height: windowHeight / 14,
    marginBottom: windowWidth / 23,
  },

  inputView2: {
    backgroundColor: '#EBF1FF',
    borderRadius: 12,
    width: windowWidth / 1.2,
    height: windowHeight / 16,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginBottom: windowWidth / 23,
  },

  TextInput: {
    height: windowHeight / 16,
    justifyContent: 'center',
    alignContent: 'center',
    paddingHorizontal: windowWidth / 24,
  },

  descriptionText: {
    height: windowHeight / 7,
    justifyContent: 'center',
    alignContent: 'center',
    paddingHorizontal: windowWidth / 24,
  },

  ContactheadinView: {
    marginTop: windowHeight / 45,
    marginBottom: windowHeight / 45,
    marginHorizontal: windowWidth / 14,
    marginLeft: windowWidth / 10,
  },

  ContactPerson: {
    fontWeight: 'bold',
    fontSize: 20,
    color: 'black',
  },

  switchView: {
    flexDirection: 'row',

    justifyContent: 'space-around',
    marginBottom: windowHeight / 35,
    marginRight: windowWidth / 15,
  },

  PrivacyheadinView: {
    marginTop: windowHeight / 45,
    marginBottom: windowHeight / 45,
    marginHorizontal: windowWidth / 14,
    marginLeft: windowWidth / 10,
  },

  Privacy: {
    fontWeight: 'bold',
    fontSize: 20,
    color: 'black',
  },

  txtSwitch: {
    flex: 2,
    paddingHorizontal: windowWidth / 30,
    left: windowWidth / 10,
  },

  create_post_btn: {
    width: windowWidth / 1,
    height: windowHeight / 13,
    backgroundColor: '#0D3369',
    marginTop: windowHeight / 20,
  },

  create_post_txt: {
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center',
    paddingVertical: windowHeight / 45,
    color: 'white',
  },
});

// export default CreatePostScreen;
const mapStateToProps = state => {
  return {
    isLoggedIn: state.global.isLoggedIn,
    location: state.global.location,
    loginData: state.global.loginData,
    formMap:state.global.formMap
  };
};

export default connect(mapStateToProps, {setFormMap})(CreatePostScreen);
