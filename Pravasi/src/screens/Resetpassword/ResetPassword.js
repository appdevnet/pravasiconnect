import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  AsyncStorage,
  Alert
} from "react-native";
import AppHeaderBack from '../../components/AppHeaderBack';
import axios from 'axios';
import CountryJSON from "../../countrycode/CountryPicker/countries.json";
import CountryPicker from 'rn-country-picker';
import Toast from '../../components/Toast';
import Root from '../../components/Root';
import PhonenumberInput from '../../components/PhonenumberInput';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {

      email:"",
      ema:true,
      message:"",
      useid:"",
      mCountryCode: "91",
      isValidPhone:"",
      phoneNumber:""
    };
  }

  Isreset=()=>{
    const {email}=this.state;
if(this.props.route.params.isMail){
    if(this.state.email=="")
    {
      this.setState({ema:false});
      // Toast.show({
      //   title: 'Error',
      //   text: 'Please Provide Email',
      //   color: '#e74c3c',
      //   timing: 2000,
      //   icon: (
      //     <Image
      //       source={require('../../../assets/close.png')}
      //       style={{ width: 25, height: 25 }}
      //       resizeMode="contain"
      //     />
      //   ),
      // })
      //alert("The Email is either empty or Wrong");
      return false;
    }
    else
    {
      // alert("Password Resetted");
      // this.props.navigation.replace('login');
      return true;
    }
  }
  else{
    console.log("keriii")
    if(this.state.phoneNumber==""){

      this.setState({isValidPhone:"Please provide number"})
      return false
    }
    else{
      return true
    }
  }
  }

 

  Reset=()=>{

    const url="https://pravasiconnect.websitepreview.in/api/forgot-password"
    
    if(this.Isreset())
    {
      const params={
        username:this.state.email
      }


      const header = {
        'Content-Type': 'application/json',
        'X-Requested-With': `XMLHttpRequest`,
        }
        console.log(params)
        axios.post(url,params,header)
        .then((response) => {
          console.log(response);


          if(response.data.status)
          {
             this.setState({message:response.data.message,
            useid:String(response.data.data.customer_id)})
             Alert.alert(this.state.message)
             //Alert.alert(this.state.useid)
             //this.StoreId(response.data.data.customer_id)
             this.props.navigation.navigate('verifyotp',{Userid:response.data.data.customer_id});
            }
            else{
             Alert.alert("Username not Found");
    
            }
  
        })
        .catch((error) => {
          console.log(error);
        });
        


    }

  }
componentDidMount(){
  console.log(this.props.route.paramsisMail)
}
  render() {
    const { navigation } = this.props;
    return (
      <>
      <SafeAreaView style={{backgroundColor:'#fff',flex:1}}>
      <AppHeaderBack {...this.props} />
        <View style={styles.headinView}>
          <Text style={styles.resetHeading}>Forgot Password</Text>
        </View>

        <View style={styles.container}>
        {this.props.route.params.isMail?(
          <View style={[styles.inputView,{borderColor:this.state.ema?null:"red",borderWidth:this.state.ema?0:1}]}>
        <TextInput
          style={styles.TextInput}
          placeholder={this.props.route.params.isMail?"Enter your Email Address":"Enter your Mobile number"}
          placeholderTextColor={this.state.ema?"#777777":"red"}
          color='black'
          keyboardType='email-address'
          onChangeText={text => this.setState({email:text,ema:true})}
          
        />
          </View>):(
        <PhonenumberInput
        error={this.state.isValidPhone}
        value={this.state.phoneNumber}
        selectedCode={(code)=>{console.log(code)}}
        onChangeText={(text)=>{this.setState({phoneNumber:text,isValidPhone:""})}}
        />)}
          <TouchableOpacity style={styles.loginBtn}
          onPress={()=> this.Reset()}>
        <Text style={styles.loginText}>SUBMIT</Text>
          </TouchableOpacity>
          

        <View style={[styles.socialtxt]}>
        <TouchableOpacity onPress={() => navigation.navigate('login')}>
            <Text>Back to <TouchableOpacity
            onPress={() => navigation.navigate('login')} >
              <Text style={styles.registrbtn}>SIGN IN</Text></TouchableOpacity></Text></TouchableOpacity>
          </View>

        
        </View>
      </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  headinView: {
    paddingHorizontal: windowWidth/20,
    marginTop: windowHeight/70,
    marginBottom: windowHeight/35,
    marginHorizontal: windowWidth/22
  },

  resetHeading: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
  },

  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: windowHeight/20,
    
  },

  inputView: {
    backgroundColor: "#EBF1FF",
    borderRadius: 12,
    width: windowWidth/1.2,
    height: windowHeight/15,
    marginBottom: windowHeight/20,

  },
 
  TextInput: {
    height: windowHeight/15,
    paddingHorizontal: 10,
    marginHorizontal: 10,
  },

  loginBtn: {
    width: windowWidth/1.2,
    borderRadius: 20,
    height: windowHeight/16,
    alignSelf: "center",
    justifyContent: "center",
    marginTop: windowHeight/50,
    backgroundColor: "#070545",
    
  },

  loginText: {
    justifyContent: "center",
    alignSelf: "center",
    fontSize: 17,
    fontWeight: "bold",
    color: "white"
  },

  socialtxt: {
    paddingTop: windowHeight/2.6,
    fontSize:15,
  },

  registrbtn: {
    color: "#070545",
    fontSize: 15,
    top:3,
    fontWeight: "bold"
    
  }
});
export default ResetPassword;
