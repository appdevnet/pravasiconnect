import React, { Component,useState } from 'react';
// import Toast from 'react-native-simple-toast';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView,
  AsyncStorage,
  Dimensions,
  Alert,
  Pressable,
  Modal,
  NetInfo
} from "react-native";

import Toast from '../../components/Toast';
import Root from '../../components/Root';
import Popup from '../../components/Popup';
import AwesomeAlert from 'react-native-awesome-alerts';
import {connect} from 'react-redux';
import { setIsLoggedin,setLoginData,startLoader,stopLoader,SetUserId,SetToken } from '../../redux/action'

// import {AsyncStorage} from '@react-native-async-storage/async-storage';
import axios from 'react-native-axios';
import AppHeader from '../../components/AppHeader'
import fb from '../../assest/Icons/Login/Iconawesome-facebook.png';
import insta from '../../assest/Icons/Login/Iconfeather-instagram.png';
import google from '../../assest/Icons/Login/Iconawesome-google.png';
import eye from '../../assest/Icons/Login/eye.png';
import hide from '../../assest/Icons/Login/hide.png';
import AppHeaderBack from '../../components/AppHeaderBack';
import { storeLoggedIn,storeUserData,storeUserId,storeUserToken } from '../../utils/asyncStorage'
import { TextInputBox,PasswordInput, CustomAlert } from '../../components';



const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username:"",
      password:"",
      uname:true,
      pswd:true,
      axios_token:"",
      axios_email:"",
      message:"",
      showpassword:false,
      isVisible:false,
      isvalid:true,
      showAlert:false,
      showNetErorAlert:false,
      unameError:"",
      pswderror:""
    };
  }

  login_validate=async ()=>{
    const {username,password}=this.state;
    if(username=="")
    {
      console.log("insideeeeeeeeeee_nameee")
      this.setState({uname:false,isvalid:false,unameError:"Please Provide Username"})
      // return false;
    }
     if(password=="")
    {
      console.log("insideeeeeeeeeee")
      this.setState({pswd:false,isvalid:false,pswderror:"Please Provide Password"})
      // return false;
    }
      //  alert(" You are Logged In Successfully");
      //  this.props.navigation.replace('abroadservice');
      
    
  }

validate = async ()=>{
  await this.login_validate()
  .then(()=>{
    console.log(this.state.isvalid,"lolol")
    if(!this.state.isvalid){
      this.setState({isvalid:true})
    }
    else{
      this.login_submit()
    }
  })
}
  login_submit=()=>{

    this.props.startLoader()

    const url = "https://pravasiconnect.websitepreview.in/api/login"
      const params={
        username:this.state.username, 
        password:this.state.password,
        lattitude:this.state.latitude,
        longitude:this.state.longitude

      }

      const header = {
        'Content-Type': 'application/json',
        'X-Requested-With': `XMLHttpRequest`,
        }

        console.log(params)
        axios.post(url,params,header)
        .then((response) => {
          this.props.stopLoader()
          console.log(response,"Login responseeeee");

          
  
          if(response.data.status==true)
          {
            storeUserData(response.data.data)
            storeUserId(response.data.data.customer_id)
            storeUserToken(response.data.data.token)
            this.props.setLoginData(response.data.data)
            this.props.SetUserId(response.data.data.customer_id)
            this.props.SetToken(response.data.data.token)
             //console.log(response.data.data,"ll")
          this.setState({
            axios_email:response.data.data.name,
            axios_token:response.data.data.token,
            message:response.data.message,
            cutomerid: response.data.data.customer_id,
            image:response.data.data.profile_image

          })
          storeLoggedIn(true).then((result) =>
              console.log('result looged in', result),
            );
          this.StoreToken(response.data.data.token,response.data.data.customer_id,response.data.data.profile_image)
          this.props.setIsLoggedin(true)
          this.props.navigation.replace('abroadservice');
          }
          else{
            const error = response.data.data
            if(error.username||error?.password){
              this.setState({
                unameError:error.username?error?.username[0]:"",
                pswderror:error?.password?error?.password[0]:"",
              })

            }
            else{
              this.setState({
                error_msg:JSON.stringify(response.data.message)
              })
            
               this.setState({showAlert:true})
            }
            
  
          }
          
        })
        .catch((error) => {
          console.log(error);
          // this.setState({showNetErorAlert:true})
          
        })
  }


  StoreToken = async (tocken, custid, proimage) => {
    console.log(proimage,"rooooooooo")
    try {
      await AsyncStorage.setItem('token',JSON.stringify({token:tocken,customerid:custid,profile:proimage}))
    } catch (e) {
      // saving error
    }
  }

 

 


  getData = async () => {
    try {
      const value = await AsyncStorage.getItem('location')
      console.log(value)
      const loc=JSON.parse(value)
      if(value !== null) {
        console.log(loc.place,value)
        this.setState({location:loc,
          country:loc.place,
          latitude:loc.latitude,
          longitude:loc.longitude,
        })
      }
    } catch(e) {
      // error reading value
    }
  }


  componentDidMount=()=>{
      this.getData()
  }

  render() {
    return (
      <>
      <SafeAreaView style={styles.complete_view}>
        <AppHeaderBack {...this.props}/>
        <View style={styles.headinView}>
          <Text style={styles.loginHeading}>Login</Text>
        </View>
      <View style={styles.container}>
      <TextInputBox
            value={this.state.username}
            error={this.state.unameError}
            placeholder={"Email or Mobile"}
            onChangeText={(text)=>{this.setState({username:text,unameError:""})}}
            />
      <PasswordInput
            value={this.state.password}
            error={this.state.pswderror}
            placeholder={"Password"}
            onChangeText={(text)=>{this.setState({password:text,pswderror:""})}}
            />
          
          <View style={{marginLeft:windowWidth/1.8,height:windowHeight/18.5,width:windowWidth/1.7}}
          >          
          <TouchableOpacity
           onPress = {() => {this.setState({ isVisible: true})}}  >
            
              <Text style={styles.forgot_button}>Forgot your Password?</Text>
            
      </TouchableOpacity>
      </View>

 
          <TouchableOpacity style={styles.loginBtn}
           onPress={()=>this.validate()}>
        <Text style={styles.loginText}>LOGIN</Text>
      </TouchableOpacity>
          
          
          <View style={[styles.socialtxt,{alignItems:'center'}]}>
            <Text>Or login with Social Media</Text>
          </View>

          <View style={{flexDirection:'row',marginTop:windowHeight/45,}}>
          
          <TouchableOpacity style={{backgroundColor:'#3A5AA5',height:windowHeight/18,width:windowHeight/18,borderRadius:100,flexDirection:'column',justifyContent:'center',}}
           >
           <Image source={fb} style={{alignSelf:'center',}}></Image>
          </TouchableOpacity>

          <TouchableOpacity style={{backgroundColor:'#EC480A',height:windowHeight/18,width:windowHeight/18,borderRadius:100,flexDirection:'column',justifyContent:'center',marginLeft:8,}}>
           <Image source={insta} style={{alignSelf:'center',}}></Image>
          </TouchableOpacity>
        
          <TouchableOpacity style={{backgroundColor:'#3E80EB',height:windowHeight/18,width:windowHeight/18,borderRadius:100,flexDirection:'column',justifyContent:'center',marginLeft:8,}}>
           <Image source={google} style={{alignSelf:'center',}}></Image>
          </TouchableOpacity>
          
          </View>
          <View style={[styles.socialtxt,{alignItems:'center'}]}
          onPress={() => this.props.navigation.navigate('register')}>
          <TouchableOpacity  onPress={() =>this.props.navigation.navigate('register')}>
            <Text>Don't have an account ? <TouchableOpacity
            onPress={() => this.props.navigation.navigate('register')}>
              <Text style={styles.registrbtn}>REGISTER NOW</Text></TouchableOpacity></Text></TouchableOpacity>
          </View>
        
        </View>
        <View style={styles.centeredView}>
        <Modal            
          animationType = {"slide"}  
          transparent = {true}  
          visible = {this.state.isVisible}  
          onRequestClose = {() =>{ console.log("Modal has been closed.") } }>  
          {/*All views of Modal*/}  
              <View style = {styles.modal}>  
              <TouchableOpacity
              style={[styles.button, styles.buttonClose]}
               onPress={() =>{this.props.navigation.navigate('resetpassword',{isMail:true}),this.setState({ isVisible:!this.state.isVisible})}}
            ><Text style={styles.textStyle}>CHANGE WITH EMAIL</Text></TouchableOpacity> 
             <TouchableOpacity
              style={[styles.button, styles.buttonClose,{marginTop:windowHeight/50}]}
              onPress={() =>{this.props.navigation.navigate('resetpassword',{isMail:false}),this.setState({ isVisible:!this.state.isVisible})}}
            ><Text style={styles.textStyle}>CHANGE WITH MOBILE</Text></TouchableOpacity> 
            <Text style={{color:'black',marginTop:windowHeight/22,fontSize:15,fontWeight:'bold'}}
            onPress = {() => {  
                  this.setState({ isVisible:!this.state.isVisible})}}>CLOSE</Text>
              {/* <Button style={{marginTop:windowHeight/5}} title="Click To Close" onPress = {() => {  
                  this.setState({ isVisible:!this.state.isVisible})}}/>   */}
          </View>  
        </Modal>
        {/* <CustomAlert showAlert={this.state.showAlert}/> */}
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title="Oops"
          message="Invalid username or password"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          progressColor="#fff"
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="No, cancel"
          confirmText="Cancel"
          confirmButtonColor="#0D3369"
          onCancelPressed={() => {
            console.log('pattichee')
          }}
          onConfirmPressed={() => {
            this.setState({showAlert:false})
          }}
        />

         {/* <AwesomeAlert
          show={this.state.showNetErorAlert}
          showProgress={false}
          title="Oops"
          message="Network Error"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          progressColor="#fff"
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="No, cancel"
          confirmText="Cancel"
          confirmButtonColor="#0D3369"
          onCancelPressed={() => {
            console.log('pattichee')
          }}
          onConfirmPressed={() => {
            this.setState({showNetErorAlert:false})
          }}
        /> */}
        </View>  
        </SafeAreaView>
        </>
    );
  }
}

const styles = StyleSheet.create({

  complete_view: {
    flex: 1,
    backgroundColor: "white",

  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    width:windowWidth/2,
  },
  buttonClose: {
    backgroundColor: "#070545",
  },
  textStyle: {
    color: "white",
    textAlign: "center",
    fontWeight:'bold',
    fontSize:13,
  },
  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: windowHeight/75,
    //backgroundColor:'red',
    
  },

  image:{
   
    
    height:60,
    width:60,
    borderRadius:100,

  },

  

  headinView: {
    paddingHorizontal: 10,
    marginTop: 40,
    marginBottom: 30,
    marginHorizontal: windowWidth/12

  },

  loginHeading: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
  },

 
  inputView: {
    backgroundColor: "#F6F8FC",
    borderRadius: 12,
    width: "80%",
    height: 50,
    marginBottom: 20,
    color:'black'
  },
 
  TextInput: {
    height: 50,
    // color: "#777777",
    paddingHorizontal: 10,
    marginHorizontal: 10,
 
  },

  // forgot_button: {
  //   marginTop: 10,
  //   marginBottom: 20,
  //   left:"20%"
      
  // },
 
  loginBtn: {
    width: "80%",
    borderRadius: 20,
    height: 50,
    alignSelf: "center",
    justifyContent: "center",
    marginTop:10,
    backgroundColor: "#070545",
    
  },

  loginText: {
    justifyContent: "center",
    alignSelf: "center",
    fontSize: 17,
    fontWeight: "bold",
    color: "white"
  },

  socialtxt: {
    marginTop: windowHeight/10,
    fontSize:15,
    height:windowHeight/30,
    width:windowWidth/1.3,
    justifyContent: 'center',
    
  },

  registrbtn: {
    color: "#070545",
    fontSize: 15,
    top:3,
    fontWeight: "bold"
    
  },
  modal: {  
    justifyContent: 'center',  
    alignItems: 'center',   
    backgroundColor : "#fff",   
    height: 250 ,  
    width: '80%',  
    borderRadius:10,  
    borderWidth: 1,  
    borderColor: '#fff',    
    marginTop: windowHeight/2.5,  
    marginLeft: 40,  

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
     
     },  

     centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 22
    },
     text: {  
        color: '#3f2949',  
        marginTop: 10  
     }  
});

// export default LoginScreen;
const mapStateToProps = (state) => {
  return {
  };
};

export default connect(mapStateToProps, {setIsLoggedin,setLoginData,startLoader,stopLoader,SetUserId,SetToken})(
  LoginScreen,
);