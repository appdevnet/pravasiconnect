import React, { Component } from 'react';
import { View, Text,TextInput,AsyncStorage,SafeAreaView,StyleSheet,Image,ScrollView,Modal,TouchableOpacity,Dimensions, Alert } from 'react-native';
import AppHeader from '../../components/AppHeader';
import MainBottom from '../../components/MainBottom';
import phone from '../../assest/Icons/Profile/Path110.png';
import mail from '../../assest/Icons/Profile/Path50.png';
import whatsapp from '../../assest/Icons/Profile/Iconionic-logo-whatsapp.png';
import skype from '../../assest/Icons/Profile/Iconawesome-skype.png';
import acc_sttings from '../../assest/Icons/Profile/Iconionic-md-settings.png';
import edit from '../../assest/Icons/Profile/Iconmaterial-edi.png';
import avatar from '../../assest/Icons/Profile/img_avatar.png';
import useid from '../../assest/Icons/Profile/Iconmaterial-verified-user.png';
import plus from '../../assest/iconss/plus.png'
import PhonenumberInput from '../../components/PhonenumberInput';
import axios from 'react-native-axios';
import Toast from '../../components/Toast';
import Root from '../../components/Root';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {connect} from 'react-redux';
import { setLoginData,startLoader,stopLoader } from '../../redux/action'
import { storeUserData } from '../../utils/asyncStorage'
import AwesomeAlert from 'react-native-awesome-alerts';

var qs = require('qs');
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const options = {
  title: 'Select Avatar',
  maxWidth:windowHeight,
  maxHeight:windowHeight,
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class UserProfilescreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAlert:false,
        isEdtMobVisible:false,
        isEdtEmailVisible:false,
        isEdtUserIDVisible:false,
        isEdtWhatsAppVisible:false,
        isEdtSkypeVisible:false,
        isCheckOTP:false,
        isValidPhone:"",
        isValidWhatsapp:"",
      phoneNumber: "",
      tempwhatsapp:"",
        number:"",
        message:"",
       emailid:"",
       ismobile:true,
       otpid:"",
       skypeid:"",
       userId:"",
       name:"",
      
      code:"",
       filepath: {
        data: '',
        path: '',
        type:'',
        filename:'',
      },
      fileData: '',
      fileUri: ''
      
    };
  }


  EmailUpdate=()=>{
    const{emailid}=this.state;
    if(emailid=="")
    {
      
      return false
    }
    else{
      this.props.startLoader()
       const params={
        customer_id:this.state.customer_id,
        email:this.state.emailid
       }
       
  
       fetch("https://pravasiconnect.websitepreview.in/api/update-email",
       {
         method: 'POST',
         headers: {
         'Content-Type': 'application/x-www-form-urlencoded',
         'X-Requested-With': 'XMLHttpRequest',
         Authorization: 'Bearer '+this.state.token,
         },
         body: qs.stringify(params),
         })
 
         .then((response) => response.json())
         .then((response) => {
           this.props.stopLoader()
         console.log(response);
         if(response.status==true)
         {

           this.setState({message:response.message})
           Alert.alert(
             "Message",
             response.message,
             [
               {
                 text: "OK",
                 onPress: () => {this.setState({isEdtEmailVisible:false}),this.props.navigation.navigate("verifyotpprofile",{isotp:false})},
                 style: "cancel",
               },
             ],
             {
               cancelable: true,
               onDismiss: () =>
                 Alert.alert(
                   "This alert was dismissed by tapping outside of the alert dialog."
                 ),
             }
           );
           
         }
         else
         {
           Alert.alert("Check the number entered")
         }

        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  OtpVerification=()=>{
    const url="https://pravasiconnect.websitepreview.in/api/verify-mobile"
  }

  NameUpdate=()=>{
    const {Name}=this.state

    if(Name=="")
    {
      // Toast.show({
      //   title: 'Error',
      //   text: 'Please Provide the Skype ID',
      //   color: '#e74c3c',
      //   timing: 2000,
      //   icon: (
      //     <Image
      //       source={require('../../../assets/close.png')}
      //       style={{ width: 25, height: 25 }}
      //       resizeMode="contain"
      //     />
      //   ),
      // })
      return false
    }
    else{
      this.props.startLoader()

      const params={
        name:this.state.Name,
        
       }

       console.log("nameeeee",params)
  
  
       fetch("https://pravasiconnect.websitepreview.in/api/update-name",
       {
         method: 'POST',
         headers: {
         'Content-Type': 'application/json',
         'X-Requested-With': 'XMLHttpRequest',
         Authorization: 'Bearer '+this.state.token,
         },
         body: JSON.stringify(params),
         })

         
      
         .then((response) => response.json())
         .then((response) => {
           this.props.stopLoader()
         console.log(response);
  
         if(response.status==true)
         {
           this.setState({
             username: this.state.name,
             isEdtSkypeVisible:false,
             message: response.message
           })
           this.GetProfile()
          //  Alert.alert(
          //    "Message",
          //    response.message,
          //    [
          //      {
          //        text: "OK",
          //        onPress: () => {this.setState({isEdtSkypeVisible:false})},
          //        style: "cancel",
          //      },
          //    ],
          //    {
          //      cancelable: true,
          //      onDismiss: () =>
          //        Alert.alert(
          //          "This alert was dismissed by tapping outside of the alert dialog."
          //        ),
          //    }
          //  );
           
         }
         else
         {
           //Alert.alert(response.message)
         }
      
   })
   .catch((error) => {
     console.log(error);
   });
  

    }
  }


  WpNumberUpdate=()=>{
    const {phoneNumber}=this.state;
    if(phoneNumber=="")
    {
      this.setState({
      isValidWhatsapp:"Provide whatsapp number"
      })

     return false
    }
    else{

     const params={
      whatsapp_number:this.state.tempwhatsapp,
       customer_id:this.state.customer_id,
       skype_id:"",
       flag:"whatsapp"
       //mobile_prefix:this.state.code
     }


     fetch("https://pravasiconnect.websitepreview.in/api/update-social-ids",
     {
       method: 'POST',
       headers: {
       'Content-Type': 'application/x-www-form-urlencoded',
       'X-Requested-With': 'XMLHttpRequest',
       Authorization: 'Bearer '+this.state.token,
       },
       body: qs.stringify(params),
       })

       .then((response) => response.json())
       .then((response) => {
       console.log(response);

       if(response.status==true)
       {
         this.setState({
          whatsapp_number:this.state.tempwhatsapp,
           message:response.data.message
          })
         Alert.alert(
           "Message",
           response.data.message,
           [
             {
               text: "OK",
               onPress: () => {this.setState({isEdtWhatsAppVisible:false})},
               style: "cancel",
             },
           ],
           {
             cancelable: true,
             onDismiss: () =>
               Alert.alert(
                 "This alert was dismissed by tapping outside of the alert dialog."
               ),
           }
         );
         
       }
       else
       {
         Alert.alert(response.message)
       }
    
 })
 .catch((error) => {
   console.log(error);
 });

        
      
    }
  }

  NumberUpdate=()=>{
     const {phoneNumber}=this.state;
     if(phoneNumber=="")
     {
       this.setState({
        isValidPhone:"Please provide mobile"
    })

      return false
     }
      else if(phoneNumber.length<10){
      this.setState({
      isValidPhone:"provide valid mobile number"
    })
    }
     else{

      const params={
        mobile:this.state.phoneNumber,
        customer_id:this.state.customer_id,
        mobile_prefix:this.state.code
      }


      fetch("https://pravasiconnect.websitepreview.in/api/update-mobile",
      {
        method: 'POST',
        headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest',
        Authorization: 'Bearer '+this.state.token,
        },
        body: qs.stringify(params),
        })

        .then((response) => response.json())
        .then((response) => {
        console.log(response);

        if(response.status==true)
        {
          this.setState({
            message:response.message
          })
          Alert.alert(
            "Message",
            response.message,
            [
              {
                text: "OK",
                onPress: () => {this.setState({isEdtMobVisible:false}),this.props.navigation.navigate("verifyotpprofile",{isotp:true})},
                style: "cancel",
              },
            ],
            {
              cancelable: true,
              onDismiss: () =>
                Alert.alert(
                  "This alert was dismissed by tapping outside of the alert dialog."
                ),
            }
          );
          
        }
        else
        {
          Alert.alert("Check the number entered")
        }
     
  })
  .catch((error) => {
    console.log(error);
  });

         
       
     }
  }

  getTokenData = async () => {
    try {
      const value = await AsyncStorage.getItem('token')
      const valu = await AsyncStorage.getItem('location')
      console.log(valu)
      console.log(value)
      const val=await JSON.parse(value)
      const va=await JSON.parse(valu)
      if(value !== null) {
        //console.log(val.token,"ttooo")
        //console.log(val.customerid,"idddd")
        console.log(va.place)

        this.setState({
          token:val.token,customer_id:val.customerid,
          place:va.place
        });
        this.GetProfile()
      }
    } catch(e) {
      // error reading value
    }
  }
  setFocus() {
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.getTokenData();
    });
  }
  componentDidMount=()=>{
    
    this.setFocus()
  }

   GetProfile=()=>{
     this.props.startLoader()
    // const url="https://pravasiconnect.websitepreview.in/api/get-profile"

    var params={
       customer_id:this.state.customer_id
      
    }

      fetch("https://pravasiconnect.websitepreview.in/api/get-profile", {
 method: 'POST',
 headers: {
 'Content-Type': 'application/x-www-form-urlencoded',
 'X-Requested-With': 'XMLHttpRequest',
 Authorization: 'Bearer '+this.state.token,
 },
 body: qs.stringify(params),
 }) 
 .then((response) => response.json())
 .then((response) => {
   this.props.stopLoader()
    console.log(response,"get profileeee");
   
    if(response.status==true)
    {
      storeUserData(response.data)
      this.props.setLoginData(response.data)
      this.setState({
        email:response.data.email,
        emailid:response.data.email,
        mobile_prefix:response.data.mobile_prefix,
        code:response.data.mobile_prefix,
        mobile:response.data.mobile,
        phoneNumber:response.data.mobile,
        Name:response.data.name,
        usename:response.data.name,
        userid:response.data.user_id,
        userId:response.data.user_id,
        skype_id:response.data.skype_id,
        skypeid:response.data.skype_id,
        whatsapp_number:response.data.whatsapp_number,
        tempwhatsapp:response.data.whatsapp_number,
        image:response.data.profile_image
     
      })

      console.log(this.state.image,"gg")
    }
    


  })
  .catch((error) => {
    console.log(error);
  });
      
   }

   

   OpenPicker=()=>
   {
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = { uri: response.uri };

        
        this.setState({
          path:response.assets[0].uri,
          type:response.assets[0].type,
          filename:response.assets[0].fileName
        })

        this.props.startLoader()

        const url="https://pravasiconnect.websitepreview.in/api/update-profile-picture"
        const Data=new FormData();
        // const file = {
        //   uri: Platform.OS === 'android' ? path : this.state.path,
        //   type: this.state.type,
        //   name:
        //     Platform.OS === 'android' ? fileName : this.state.path.replace(/^.*[\\\/]/, ''),
         
        // };
        //console.log(Data.append('profile_image',file)) ;
      Data.append('profile_image',{
        uri:this.state.path,
        type:this.state.type,
        name:this.state.filename
      });
      Data.append('customer_id',this.state.customer_id)

       

    console.log("form",Data)
    fetch(url, {
 method: 'POST',
 headers: {
  Accept: 'application/json',
  Authorization: 'Bearer ' + this.state.token,
  'Content-Type': 'multipart/form-data',
  enctype: 'multipart/form-data',
 },
 body:Data,
 }) 
 .then((response) => response.json())
 .then((response) => {
   this.props.stopLoader()
    console.log(response,"image upload responseeee");


   if (response.status == true) {
      this.GetProfile()
      this.setState({showAlert:true})

    }
    else{
      //Alert.alert("")
    }


      
  })
  .catch((error) => {
    console.log(error);
  });
         //You can also display the image using data:
         //const sourceq = { uri: 'data:image/jpeg;base64,' + file };
         //alert(JSON.stringify(source));
         //const val=JSON.stringify(sourceq)
         //console.log('response', val);
        
        

      }
    });

   

   }


   UserIDUpdate=()=>{

    const {userId}=this.state;
    if(userId=="")
    {
      // Toast.show({
      //   title: 'Error',
      //   text: 'Please Provide the User ID',
      //   color: '#e74c3c',
      //   timing: 2000,
      //   icon: (
      //     <Image
      //       source={require('../../../assets/close.png')}
      //       style={{ width: 25, height: 25 }}
      //       resizeMode="contain"
      //     />
      //   ),
      // })
      return false
    }
    else{
     
      const params={
        user_id:this.state.userId,
        customer_id:this.state.customer_id,
      }


      fetch("https://pravasiconnect.websitepreview.in/api/update-userid",
      {
        method: 'POST',
        headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-Requested-With': 'XMLHttpRequest',
        Authorization: 'Bearer '+this.state.token,
        },
        body: qs.stringify(params),
        })

        .then((response) => response.json())
        .then((response) => {
        console.log(response);

        if(response.status==true)
        {
          this.setState({
            userid:this.state.userId,
            message:response.message})
          Alert.alert(
            "Message",
            response.message,
            [
              {
                text: "OK",
                onPress: () => {this.setState({isEdtUserIDVisible:false})},
                style: "cancel",
              },
            ],
            {
              cancelable: true,
              onDismiss: () =>
                Alert.alert(
                  "This alert was dismissed by tapping outside of the alert dialog."
                ),
            }
          );
          
        }
        else
        {
          Alert.alert("Check the number entered")
        }
     
  })
  .catch((error) => {
    console.log(error);
  });

    }

   }

  render() {
    const { navigation } = this.props;
    return (
      <>
        <SafeAreaView style={{backgroundColor:'#fff',flex:1}}>
        <AppHeader {...this.props}/>
        {/* <ScrollView showsVerticalScrollIndicator={false}
               keyboardShouldPersistTaps="handled"> */}
      <View>
        <Text style={styles.heading}>Profile</Text>
      </View>
      <View style={{flexDirection:"row"}}>    
              <TouchableOpacity onPress={()=> this.OpenPicker()} style={{flexDirection:"row"}}>
                {!this.state.image?
                  <Image resizeMode={'cover'} source={{ uri: 'https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png' }} style={{ width: windowHeight / 16, height: windowHeight / 16, borderRadius: 60 / 2, marginLeft: windowWidth / 7, marginTop: windowHeight / 60, }}></Image>
                  :
                  <Image resizeMode={'cover'} source={{ uri: this.state.image }} style={{ width: windowHeight / 16, height: windowHeight / 16, borderRadius: 60 / 2, marginLeft: windowWidth / 7, marginTop: windowHeight / 60, }}></Image>}
          
          <Image source={plus} style={{marginTop:windowHeight/18,width:windowWidth/18,height:windowWidth/18,marginLeft:-8}}></Image>
          </TouchableOpacity>
              <View style={{ flexDirection: 'column' }}>
                <View style={{flexDirection:"row",}}>
                  <Text numberOfLines={2} style={styles.profile_view}>{this.state.usename}</Text>
                  <TouchableOpacity onPress={()=> {this.setState({isEdtSkypeVisible:true})}}>
                  <Image source={edit} style={{marginTop:30}}></Image>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection:"row",}}>
                  <Text numberOfLines={1} style={styles.profile}>{this.state.userid}</Text>
                  <TouchableOpacity onPress={()=> {this.setState({isEdtUserIDVisible:true})}}>
                  <Image source={edit} style={{marginTop:22}}></Image>
                  </TouchableOpacity>
                </View>
              </View>
      </View>

      <View>
          <Text style={styles.contact}>Contact Information</Text>
      </View>

      <TouchableOpacity style={styles.view} onPress = {() => {this.setState({ isEdtMobVisible: true})}}>
          <View>
           <Image resizeMode="contain" source={phone} style={[styles.report]}></Image>
          </View>
            <View style={{ width: windowWidth / 2 }}>
              <Text style={styles.job1}>Mobile</Text>
              {this.state.mobile != "" &&
                <Text numberOfLines={1} style={styles.jobdesc}>{this.state.mobile_prefix + " " + this.state.mobile}</Text>}
          </View>
          <View style={{width:windowWidth/10,marginLeft:windowWidth/12,marginTop:windowHeight/80,height:windowHeight/25}}>
              <Image source={edit} style={{margin:'5%',}}></Image>
          </View>
      </TouchableOpacity>

      <TouchableOpacity style={styles.view} onPress = {() => {this.setState({ isEdtEmailVisible: true})}}>
          <View>
           <Image resizeMode="contain" source={mail} style={styles.report}></Image>
          </View>
          <View style={{width:windowWidth/2,}}>
              <Text style={styles.job1}>Email</Text>
              <Text numberOfLines={1} style={styles.jobdesc}>{this.state.email}</Text>
          </View>
          <View style={{width:windowWidth/10,marginLeft:windowWidth/12,marginTop:windowHeight/80,height:windowHeight/25}}>
              <Image source={edit} style={{margin:'5%',}}></Image>
          </View>
      </TouchableOpacity>

      {/* <TouchableOpacity style={[styles.view,]} onPress = {() => {this.setState({isEdtUserIDVisible : true})}}>
          <View>
           <Image resizeMode="contain" source={useid} style={styles.report}></Image>
          </View>
          <View style={{width:windowWidth/2}}>
              <Text style={styles.job1}>User ID</Text>
              <Text numberOfLines={1} style={styles.jobdesc}>{this.state.userid}</Text>
          </View>
          <View style={{width:windowWidth/10,marginLeft:windowWidth/12,marginTop:windowHeight/80,height:windowHeight/25}}>
              <Image source={edit} style={{margin:'5%',}}></Image>
          </View>
      </TouchableOpacity> */}

      <TouchableOpacity style={styles.view} onPress = {() => {this.setState({ isEdtWhatsAppVisible: true})}}>
          <View>
           <Image resizeMode="contain" source={whatsapp} style={styles.report}></Image>
          </View>
          <View style={{width:windowWidth/2}}>
              <Text style={styles.job1}>Whatsapp</Text>
              <Text numberOfLines={1} style={styles.jobdesc}>{this.state.whatsapp_number}</Text>
          </View>
          <View style={{width:windowWidth/10,marginLeft:windowWidth/12,marginTop:windowHeight/80,height:windowHeight/25}}>
              <Image source={edit} style={{margin:'5%',}}></Image>
          </View>
      </TouchableOpacity>

      {/* <TouchableOpacity style={styles.view} onPress = {() => {this.setState({ isEdtSkypeVisible: true})}}>
          <View>
           <Image resizeMode="contain" source={skype} style={styles.report}></Image>
          </View>
          <View style={{width:windowWidth/2}}>
              <Text style={styles.job1}>Skype</Text>
              <Text numberOfLines={1} style={styles.jobdesc}>{this.state.skype_id}</Text>
          </View>
          <View style={{width:windowWidth/10,marginLeft:windowWidth/12,marginTop:windowHeight/80,height:windowHeight/25}}>
              <Image source={edit} style={{margin:'5%',}}></Image>
          </View>
      </TouchableOpacity> */}

      <TouchableOpacity style={styles.view}
      onPress={() => this.props.navigation.navigate("settingscreen")}>
          <View>
           <Image resizeMode="contain" source={acc_sttings} style={styles.report}></Image>
          </View>
          <View>
              <Text style={styles.job2}>Account Settings</Text>
              
          </View>
          {/* <View style={{width:windowWidth/10,marginLeft:windowWidth/3.5,marginTop:windowHeight/80,height:windowHeight/25}}>
              <Image source={edit} style={{margin:'5%',}}></Image>
          </View> */}
          </TouchableOpacity>
          
          
      {/* mobilenumber modal */}
<View style={styles.centeredView}>
<Modal            
  animationType = {"slide"}  
  transparent = {true}  
  visible = {this.state.isEdtMobVisible}  
  onRequestClose = {() =>{ console.log("Modal has been closed.") } }>  
  {/*All views of Modal*/}  
      <View style = {styles.modal}>  

      <Text style={{fontSize:13,fontWeight:'bold',marginTop:windowHeight/100,padding:15}}>Edit the Mobile</Text>

      <PhonenumberInput
      code={this.state.code}
        value={this.state.phoneNumber}
        error={this.state.isValidPhone}
        selectedCode={(code)=>{this.setState({code:code})}}
        onChangeText={(text)=>{this.setState({phoneNumber:text,isValidPhone:""})}}
 />
    <View style={{flexDirection:'row',justifyContent:'space-around',width:windowWidth/1.2}}>

<TouchableOpacity
      style={[styles.button, styles.buttonClose,{marginTop:windowHeight/50}]}
      onPress={()=>this.NumberUpdate()} 
    ><Text style={styles.textStyle}>UPDATE</Text></TouchableOpacity> 
    </View>
    <Text style={{color:'black',marginTop:windowHeight/45,fontSize:15,fontWeight:'bold'}}
    onPress = {() => {  
          this.setState({ isEdtMobVisible:!this.state.isEdtMobVisible})}}>CLOSE</Text>
      
  </View>   
</Modal>
</View>  

{/* email modal */}

<View style={styles.centeredView}>
<Modal            
  animationType = {"slide"}  
  transparent = {true}  
  visible = {this.state.isEdtEmailVisible}  
  onRequestClose = {() =>{ console.log("Modal has been closed.") } }>  
  {/*All views of Modal*/}  
      <View style = {styles.modal}>  

      <Text style={{fontSize:13,fontWeight:'bold',marginTop:windowHeight/100,padding:15}}>Edit the Email</Text>

      <View style={[styles.inputView,]}>
<TextInput
  style={styles.TextInput}
  placeholder={"Enter email"}
  placeholderTextColor={"#A5A5A5"}
  color='black'
  value={this.state.emailid}
  keyboardType='email-address'
  onChangeText={text => this.setState({emailid:text,uname:true})}
  // onChangeText={(email) => setEmail(email)}
/>

</View>
    <View style={{flexDirection:'row',justifyContent:'space-around',width:windowWidth/1.2}}>
    
<TouchableOpacity
      style={[styles.button, styles.buttonClose,{marginTop:windowHeight/50}]}
      onPress={() =>this.EmailUpdate()}
    ><Text style={styles.textStyle}>UPDATE</Text></TouchableOpacity> 
    </View>
    <Text style={{color:'black',marginTop:windowHeight/45,fontSize:15,fontWeight:'bold'}}
    onPress = {() => {  
          this.setState({ isEdtEmailVisible:!this.state.isEdtEmailVisible})}}>CLOSE</Text>
      
  </View>   
</Modal>
</View>  

{/* userid modal */}

<View style={styles.centeredView}>
<Modal            
  animationType = {"slide"}  
  transparent = {true}  
  visible = {this.state.isEdtUserIDVisible}  
  onRequestClose = {() =>{ console.log("Modal has been closed.") } }>  
  {/*All views of Modal*/}  
      <View style = {styles.modal}>  

      <Text style={{fontSize:13,fontWeight:'bold',marginTop:windowHeight/100,padding:15}}>Edit the UserID</Text>

      <View style={[styles.inputView,]}>
<TextInput
  style={styles.TextInput}
  placeholder={"Enter User ID"}
  placeholderTextColor={"#A5A5A5"}
  value={this.state.userId}
  color='black'
  keyboardType='email-address'
  onChangeText={text => this.setState({userId:text,uname:true})}
  // onChangeText={(email) => setEmail(email)}
/>

</View>
    
     <TouchableOpacity
      style={[styles.button, styles.buttonClose,{marginTop:windowHeight/50}]}
      onPress={()=>this.UserIDUpdate()}
    ><Text style={styles.textStyle}>UPDATE</Text></TouchableOpacity> 
    <Text style={{color:'black',marginTop:windowHeight/45,fontSize:15,fontWeight:'bold'}}
    onPress = {() => {  
          this.setState({ isEdtUserIDVisible:!this.state.isEdtUserIDVisible})}}>CLOSE</Text>
      
  </View>   
</Modal>
</View>  

{/* whatsapp modal */}

<View style={styles.centeredView}>
<Modal            
  animationType = {"slide"}  
  transparent = {true}  
  visible = {this.state.isEdtWhatsAppVisible}  
  onRequestClose = {() =>{ console.log("Modal has been closed.") } }>  
  {/*All views of Modal*/}  
      <View style = {styles.modal}>  

      <Text style={{fontSize:13,fontWeight:'bold',marginTop:windowHeight/100,padding:15}}>Edit the Mobile</Text>

      <PhonenumberInput
 value={this.state.tempwhatsapp}
 error={this.state.isValidWhatsapp}
 selectedCode={(code)=>{this.setState({code:code})}}
 onChangeText={(text)=>{this.setState({tempwhatsapp:text,isValidWhatsapp:"",number:text})}}
/>
    
<View style={{flexDirection:'row',justifyContent:'space-around',width:windowWidth/1.2}}>
<TouchableOpacity
      style={[styles.button, styles.buttonClose,{marginTop:windowHeight/50}]}
      onPress={()=>this.WpNumberUpdate()} 
    ><Text style={styles.textStyle}>UPDATE</Text></TouchableOpacity> 
    </View>
    <Text style={{color:'black',marginTop:windowHeight/45,fontSize:15,fontWeight:'bold'}}
    onPress = {() => {  
          this.setState({ isEdtWhatsAppVisible:!this.state.isEdtWhatsAppVisible})}}>CLOSE</Text>
      
  </View>   
</Modal>
</View>  

{/* skype modal */}

<View style={styles.centeredView}>
<Modal            
  animationType = {"slide"}  
  transparent = {true}  
  visible = {this.state.isEdtSkypeVisible}  
  onRequestClose = {() =>{ console.log("Modal has been closed.") } }>  

      <View style = {styles.modal}>  

      <Text style={{fontSize:13,fontWeight:'bold',marginTop:windowHeight/100,padding:15}}>Edit the Name</Text>

      <View style={[styles.inputView,]}>
<TextInput
  style={styles.TextInput}
  placeholder={"Enter Name"}
  placeholderTextColor={"#A5A5A5"}
  value={this.state.Name}
  color='black'
  keyboardType='email-address'
  onChangeText={text => this.setState({Name:text,uname:true})}
  
/>

</View>
    
     <TouchableOpacity
      style={[styles.button, styles.buttonClose,{marginTop:windowHeight/50}]}
      onPress={()=>this.NameUpdate()}
    ><Text style={styles.textStyle}>UPDATE</Text></TouchableOpacity> 
    <Text style={{color:'black',marginTop:windowHeight/45,fontSize:15,fontWeight:'bold'}}
    onPress = {() => {  
          this.setState({ isEdtSkypeVisible:!this.state.isEdtSkypeVisible})}}>CLOSE</Text>
      
  </View>   
</Modal>

<AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title="Success!"
          message="Profile Image Updated"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          progressColor="#fff"
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="No, cancel"
          confirmText="OK"
          confirmButtonColor="#0D3369"
          onCancelPressed={() => {
            console.log('pattichee')
          }}
          onConfirmPressed={() => {
            this.setState({showAlert:false})
          }}
        />

</View>

  
      {/* </ScrollView> */}
      
      
      
        </SafeAreaView>
        <MainBottom {...this.props} />

      </>
    );
  }
}


const styles=StyleSheet.create({
   
    heading:{
        fontSize:20,
        fontWeight:'bold',
        marginTop:25,
        marginLeft:22,
        flexDirection: 'row',
    },

  profile_view: {
    
    
        marginTop:windowHeight/50,
    fontWeight: 'bold',
        fontSize: 18,
        marginLeft:windowHeight/28,

    },
    profile:{
        marginTop:windowHeight/70,
      fontWeight: '400',
        fontSize: 12,
        marginLeft:windowHeight/28,
    },
    contact:{
        fontSize:18,
        fontWeight:'bold',
        marginLeft:20,
        marginTop:40,
    },

    view:{
        padding:windowHeight/60,
        backgroundColor:'#F6F8FC',
        height:windowWidth/6,
        flexDirection: 'row',
        marginLeft:windowWidth/20,
        marginRight:windowWidth/20,
        marginTop:windowHeight/50,
          
          
    },

    report:{
        width:windowWidth/23,
        height:windowHeight/50,
        margin:'10%',
     
       
        
       },
       job1:{
        fontSize:11,
       marginLeft:windowHeight/100,
         fontWeight: 'bold',
        
        color:'#2051AE'
       },
       jobdesc:{
         fontSize: 13,
         marginVertical:windowHeight/75,
         marginLeft: windowWidth / 50,
       
        color:'#000',
       },

    report1:{
        margin:'5%',
        marginLeft: windowWidth/10,
      
       },
   
       job2:{
        fontSize:13,
       textAlign:'center',
        fontWeight:'bold',
         color: '#2051AE',
         marginTop: windowHeight/80,
        marginLeft:windowHeight/100,
        
       },

       modal: {  
        justifyContent: 'center',  
        alignItems: 'center',   
        backgroundColor : "#fff",   
        height: 250 ,  
        width: '90%',  
        borderRadius:10,  
        borderWidth: 1,  
        borderColor: '#fff',    
        marginTop: windowHeight/2.5,  
        marginLeft: windowWidth/21,  
    
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
         
         },  
    
         centeredView: {
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          marginTop: 22
        },

        button: {
            borderRadius: 20,
            padding: 10,
            elevation: 2,
            width:windowWidth/3,
          },
          buttonClose: {
            backgroundColor: "#070545",
          },
          textStyle: {
            color: "white",
            textAlign: "center",
            fontWeight:'bold',
            fontSize:13,
          },

          TextInput: {
            height: 50,
            paddingHorizontal: 5,
            marginHorizontal: 10,
            marginTop:windowWidth/180,
            backgroundColor:"#EBF1FF"
         
          },

          inputView: {
            backgroundColor: "#EBF1FF",
            borderRadius: 12,
            width: "80%",
            height: 50,
            
            color:'black'
          },

})
const mapStateToProps = (state) => {
  return {
  };
};

export default connect(mapStateToProps, {setLoginData,startLoader,stopLoader})(
  UserProfilescreen
);