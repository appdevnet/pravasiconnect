import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Platform,
  Dimensions,
  AsyncStorage,
  FlatList,
  Alert
} from "react-native";
import AppHeader from '../../components/AppHeader'
import MainBottom from '../../components/MainBottom'
import sample from '../../assest/Icons/Navigation/sample.png'
import axios from 'react-native-axios';
import {connect} from 'react-redux';
import {startLoader,stopLoader,SetUserId,SetToken} from '../../redux/action'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class AbroadServiceScreen extends Component {
  constructor(props) {
    super(props);
      this.state = {
        GridListItems: [
          { key: "Jobs",icon: require('../../assest/Icons/AvailableJobs/Layer1.png'),screen:"CreatePost" },
          { key: "Rooms",icon: require('../../assest/Icons/AbroadServices/room.png') },
          { key: "Sales/Rent",icon: require('../../assest/Icons/AbroadServices/sales_rent.png') },
          { key: "Restaurants",icon: require('../../assest/Icons/AbroadServices/restaurants.png') },
          { key: "Exchange",icon: require('../../assest/Icons/AbroadServices/Exchange.png') },
          { key: "Saloons",icon: require('../../assest/Icons/AbroadServices/saloons.png') },
          { key: "Taxi",icon: require('../../assest/Icons/AbroadServices/taxi.png') },
          { key: "Shopping Mall",icon: require('../../assest/Icons/AbroadServices/shopping_mall.png') },
          { key: "Supermarket",icon: require('../../assest/Icons/AbroadServices/supermarket.png') },
        ]
    };
  }
    
    GetGridViewItem(item) {
    Alert.alert(item);
  }

  getTokenData = async () => {
    try {
      const value = await AsyncStorage.getItem('token')
      console.log(value)
      const val=JSON.parse(value)
      if(value !== null) {
        console.log(val.token,value)
        console.log(val.customerid,value)

        this.setState({
          token:val.token,customer_id:val.customerid
        });

        this.GetCategory()
        this.GetBanner()
        
      }
    } catch(e) {
      // error reading value
    }
  }

  componentDidMount=()=>{
    //console.log("userid==========",this.props.userID)
    //console.log("token===========",this.props.Token)
    console.log(this.props.location,"location froom redux",this.props.loginData)
    this.GetCategory()
    this.GetBanner()
    
  }

  GetBanner=()=>{
    this.props.startLoader()
    const url="https://pravasiconnect.websitepreview.in/api/get-banners"

    const header = {
      'Content-Type': 'application/json',
      'X-Requested-With': `XMLHttpRequest`,
      }


      axios.post(url,header)
      .then((response) => {
        this.props.stopLoader()
        console.log("Banner==",response);

         //response.data.data[0].banner.banner_image[0].image
         if(response.data.status==true)
         {
           this.setState({
             banner_image:response.data.data[0].banner.banner_image[0].image
           })

           console.log("json_banner",JSON.stringify(this.state.banner_image))

         }
         else{
           console.log("Error bannerrrrr")
         }
      })
      .catch((error) => {
        console.log(error);
      });


  }

  GetCategory=()=>{
    this.props.startLoader()
    const url="https://pravasiconnect.websitepreview.in/api/get-categories"
    
    const params={
      type:1,
      parent:true
    }

    const header = {
      'Content-Type': 'application/json',
      'X-Requested-With': `XMLHttpRequest`,
      }


      console.log(params)
      axios.post(url,params,header)
      .then((response) => {
        this.props.stopLoader()
        console.log("category==",response);
       
        if(response.data.status==true)
        {
           this.setState({
            GridListItems:response.data.data,
             image:response.data.data.image,
             
           })

    

        }

     

      })
      .catch((error) => {
        console.log(error);
      });


  }



  render() {
    return (
      <SafeAreaView style={[styles.complete_view]}>
      <AppHeader isNotify = {true} {...this.props} />
            
                
            
            <View style={{position:"relative",width:windowWidth,backgroundColor:"#F6F8FC"}}>
          <Image
            resizeMode="cover"
                    style={{height:windowHeight/2.5,width:windowWidth,marginTop:windowHeight/190}}
                    source={{uri:this.state.banner_image}}
                />

<TouchableOpacity style={styles.BtnStyle} onPress={()=>this.props.navigation.navigate(this.props.isLoggedIn?"CreatePostCategory":'login')}>
           <Text style={styles.BtnText}>CREATE A POST</Text>
       </TouchableOpacity>
            </View>

            <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled">
                
            <View style={styles.headinView}>
          <Text style={styles.headnTxt}>Available Services in UAE</Text>
                </View>
                
            <View style={styles.container}>
            <FlatList 
            showsVerticalScrollIndicator={false}
            data={ this.state.GridListItems }
            renderItem={ ({item}) =>
                <TouchableOpacity 
                onPress={()=>this.props.navigation.navigate('availablejobscreen',{category:item.title,category_id:item.id})}
                style={styles.GridViewContainer}>
                <Image
                  resizeMode={'contain'}
                  style={{ height: windowHeight/31, width: windowWidth/13, marginTop: 4 }}
                        source={{uri:item.image} }
                    />
               <Text style={styles.GridViewTextLayout} > {item.title} </Text>
              </TouchableOpacity> }
            numColumns={3}
         />
       </View>
        </ScrollView>
        <MainBottom {...this.props} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

    complete_view: {
      flex: 1,
      backgroundColor: "white",
    },

    headinView: {  
      marginTop: windowHeight/30,
      marginBottom: windowHeight/70,
      marginHorizontal: windowHeight/24.5
  },

  headnTxt: {
    fontWeight: "bold",
    fontSize: 15,
    color: "black"
    },

    BtnStyle:{
      backgroundColor:'#0D3369',
      width:windowWidth/3,
      height:windowHeight/28,
      marginLeft:windowWidth/7,
      position:'absolute',
      marginTop:windowHeight/4,
      textAlign:'center',
      alignItems:'center',
      borderRadius:60,
      justifyContent:'center',
    },
    
    BtnText:{
      color:'#fff',
      textAlign:'center',
      fontSize:12,
      alignItems:'center',
      fontWeight:'700',
      justifyContent:'center',
    },
  
  container: {
      width: windowWidth,
      height: windowHeight/2.9,
      justifyContent: "center",
      alignItems: 'center',
      backgroundColor: "#fff"
  },
  
  GridViewContainer: {
      marginTop: windowHeight/55,
      justifyContent: 'center',
      alignItems: 'center',
      height: windowHeight/10,
      width:windowWidth/3.8,
      marginHorizontal: windowWidth/50,
      borderColor: "#EEEEEE",
      borderWidth: 1,
      borderRadius:12,
      backgroundColor:"#fff"
},
GridViewTextLayout: {
      fontSize: 10,
      color:"#0e0440",
      fontWeight: "500",
      justifyContent: 'center',
      marginTop:windowHeight/100
 }
    
})

// export default AbroadServiceScreen;
const mapStateToProps = (state) => {
  return {
    isLoggedIn:state.global.isLoggedIn,
    location:state.global.location,
    loginData:state.global.loginData,
    userID:state.global.userID,
    Token:state.global.Token
    
  };
};

export default connect(mapStateToProps, {startLoader,stopLoader,SetUserId,SetToken})(
  AbroadServiceScreen,
);