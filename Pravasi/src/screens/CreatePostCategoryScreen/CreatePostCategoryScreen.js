import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Platform,
  Dimensions,
  AsyncStorage,
  FlatList,
  Alert
} from "react-native";
import axios from 'react-native-axios';
import { navigate } from '../../RootNavigation';
import AppHeader from '../../components/AppHeader'
import { setLoginData, startLoader, stopLoader } from '../../redux/action'
import {connect} from 'react-redux';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


class CreatePostCategoryScreen extends Component {
  constructor(props) {
    super(props);
      this.state = {
        GridListItems: [
          { key: "Jobs",icon: require('../../assest/Icons/AvailableJobs/Layer1.png'),screen:"CreatePost" },
          { key: "Rooms",icon: require('../../assest/Icons/AbroadServices/room.png') },
          { key: "Sales/Rent",icon: require('../../assest/Icons/AbroadServices/sales_rent.png') },
          { key: "Restaurants",icon: require('../../assest/Icons/AbroadServices/restaurants.png') },
          { key: "Exchange",icon: require('../../assest/Icons/AbroadServices/Exchange.png') },
          { key: "Saloons",icon: require('../../assest/Icons/AbroadServices/saloons.png') },
          { key: "Taxi",icon: require('../../assest/Icons/AbroadServices/taxi.png') },
          { key: "Shopping Mall",icon: require('../../assest/Icons/AbroadServices/shopping_mall.png') },
          { key: "Supermarket",icon: require('../../assest/Icons/AbroadServices/supermarket.png') },
        ]
    };
  }
    
    GetGridViewItem(item) {
    Alert.alert(item);
  }
    
    // getTokenData = async () => {
    // try {
    //   const value = await AsyncStorage.getItem('token')
    //   console.log(value)
    //   const val=JSON.parse(value)
    //   if(value !== null) {
    //     console.log(val.token,value)
    //     console.log(val.customerid,value)

    //     this.setState({
    //       token:val.token,customer_id:val.customerid
    //     });

    //     this.GetCategory()
        
    //   }
    // } catch(e) {
    //   // error reading value
    // }
    // }
    
  GetCategory = () => {
      
     this.props.startLoader()
    
    const url="https://pravasiconnect.websitepreview.in/api/get-categories"
    
    const params={
      type:1,
      parent:true
    }

    const header = {
      'Content-Type': 'application/json',
      'X-Requested-With': `XMLHttpRequest`,
      }


      console.log(params)
      axios.post(url,params,header)
        .then((response) => {
        this.props.stopLoader()
        console.log(response);
       
        if(response.data.status==true)
        {
           this.setState({
            GridListItems:response.data.data,
             image:response.data.data.image,
           })

    

        }

     

      })
      .catch((error) => {
        console.log(error);
      });


    }
    
    componentDidMount=()=>{
    
    this.GetCategory()
    
  }


  render() {
    return (
      <SafeAreaView style={[styles.complete_view]}>
        <AppHeader isNotify = {false} {...this.props} />
            <ScrollView
            showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled">
          
          <View style={styles.headinView}>
          <Text style={styles.headnTxt}>Choose Category</Text>
                </View>
                
                <View style={styles.container}>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={ this.state.GridListItems }
                        renderItem={({ item}) => 
                          <TouchableOpacity onPress={()=>this.props.navigation.navigate('CreatePostSubCategory',{category:item.title,category_id:item.id})}
                            style={styles.GridViewContainer}>
                                    <Image resizeMode="contain"
                                style={{ height: windowHeight/31, width: windowWidth/13, marginTop:windowHeight/40 }}
                                source={{uri:item.image}}
                                    />
                                    <Text style={styles.GridViewTextLayout} > {item.title} </Text>

                                </TouchableOpacity>
                        }
                        numColumns={3}
                    />
                </View>

            </ScrollView>
            
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

    complete_view: {
      flex: 1,
      backgroundColor: "white",
  },
  
  headinView: {
      
      marginTop: windowHeight/30,
      marginBottom: windowHeight/70,
      marginHorizontal: windowHeight/20
  },

  headnTxt: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
    },

    container: {
      width: windowWidth,
      height: windowHeight/1,
      justifyContent: "center",
      alignItems: 'center',
      backgroundColor: "#fff"
    },
    
  GridViewContainer: {
      marginTop: windowHeight / 40, 
      marginLeft: windowWidth/100,
      justifyContent: 'center',
      alignItems: 'center',
      height: windowHeight/8,
      width:windowWidth/3.8,
      
      borderColor: "#d9d1d0",
      borderWidth: 1,
      
      
      backgroundColor:"#fff"
    },

    GridViewTextLayout: {
      fontSize: 10,
      color:"black",
      fontWeight: "300",
      justifyContent: 'center',
      padding: 10,
      marginTop:windowHeight/65
 }

})

const mapStateToProps = (state) => {
  return {
  };
};

export default connect(mapStateToProps, {setLoginData,startLoader,stopLoader})(
  CreatePostCategoryScreen
);
