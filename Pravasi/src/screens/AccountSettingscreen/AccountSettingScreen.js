import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
    TextInput,
  FlatList,
  Button,
  TouchableOpacity,
  SafeAreaView,
  ScrollView
} from "react-native";
import AppHeader from '../../components/AppHeader'
import {removeLoginDetails} from '../../utils/asyncStorage'
import {CommonActions} from '@react-navigation/native';
import {connect} from 'react-redux';
import { removeLoginData } from '../../redux/action'
class AccountSettingScreen extends Component {
  constructor(props) {
    super(props);
      this.state = {
        GridListItems: [
        { id: 1,key: "Language",icon: require('../../assest/Icons/Settings/Iconmetro-language.png') },
        { id: 2,key: "Notifications",icon: require('../../assest/Icons/Settings/Iconionic-md-notifications-outline.png') },
        { id: 3,key: "Change Locations",icon: require('../../assest/Icons/Settings/Iconmaterial-my-location.png') },
        { id: 4,key: "Settings",icon: require('../../assest/Icons/Settings/Iconfeather-settings.png') },
        { id: 5,key: "Logout",icon: require('../../assest/Icons/Settings/Iconmetro-exit.png') },     
      ]
    };
  }
  menuAction(type){
    if(type=="Logout"){
        this.Logout()
    }
  } 
  Logout = async function(){
    console.log("popopo-logout")
    this.props.removeLoginData()
      await removeLoginDetails()
      .then(()=>{
        this.props.navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [{name: 'mapscreen'}],
            }),
          );
      })
  } 
  render() {
    return (
        <SafeAreaView style={styles.complete_view}>
            <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled">
            <AppHeader {...this.props} />
            <View style={styles.headinView}>
                    <Text style={styles.txt_settin}>Settings </Text>
                </View>

                <FlatList 
                    scrollEnabled={false}
                    showsVerticalScrollIndicator={false}
                    data={this.state.GridListItems}
                    renderItem={({ item }) =>
                        <SafeAreaView style={{flex: 1,}}>
                        <TouchableOpacity onPress={()=>this.menuAction(item.key)} style={styles.list_view}>
                            <View style={styles.image_view}>
                                    <Image
                                        resizeMode={'contain'}
                                    style={{ height: 25, width: 25, }}
                                    source={item.icon}
                                /></View>
                            <View style={styles.txt_view}><Text style={styles.txt_format}>{item.key} </Text></View>
                                <View style={styles.arrow_move_view}>
                                    <TouchableOpacity >
                                <Image
                                    style={{ height: 10, width: 10,alignSelf:"center" }}
                                            source={require('../../assest/Icons/AvailableJobs/Iconionic-ios-arrow-forward.png')} />
                                        </TouchableOpacity>
                            </View>                        
                            </TouchableOpacity>
                            <View style={styles.lines}></View>  
                        </SafeAreaView>} 
         />
            </ScrollView>
            </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

    complete_view: {
        flex: 1,
        backgroundColor: "white",
    },

    headinView: {
        paddingHorizontal: "10%",
        marginTop: 10,
        marginBottom: 20,
    },

    txt_settin: {
        fontWeight: "bold",
        fontSize: 20,
        color: "black"
    },

    list_view: {
        flex: 1,
        backgroundColor:"#fff",
        flexDirection: 'row',
        justifyContent:"space-between",
        paddingHorizontal: "10%",
        marginBottom:20
    },

    image_view: {
        height: 50,
        width: 50,
        flex: 0.5,
        backgroundColor: "#F6F8FC",
        borderRadius: 50,
        flexDirection:"column",
        justifyContent: "center",
        paddingHorizontal: "5%"

    },
    
    txt_view: {
        paddingHorizontal: "12%",
        flex: 2.5,
        // top: 10,
        flexDirection: "column",
        justifyContent:"center",
        // backgroundColor:"blue"
    },

    txt_format: {
        fontWeight: "400",
        fontSize:13
    },

    arrow_move_view: {
        // marginHorizontal: "15%",
        // top: 10,
        flex: 1,
        flexDirection: "column",
        justifyContent:"center",
        // backgroundColor:"green"
    },

    lines: {
        width: "80%",
        backgroundColor: '#d9d1d0',
        height: 1,
        marginBottom:20,
        justifyContent: "center",
        alignSelf:"center"
    },

  
})

// export default AccountSettingScreen;
const mapStateToProps = (state) => {
  return {
  };
};

export default connect(mapStateToProps, {removeLoginData})(
  AccountSettingScreen,
);