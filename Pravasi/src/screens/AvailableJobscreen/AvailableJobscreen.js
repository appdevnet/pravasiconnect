import React, { Component } from 'react';
import { View, Text,SafeAreaView,ScrollView,StyleSheet,Image,TouchableOpacity,Dimensions,FlatList,AsyncStorage,Alert } from 'react-native';
import AppHeader from '../../components/AppHeader';
import dataentry from '../../assest/Icons/AvailableJobs/Layer6.png';
import forward_arrow from '../../assest/Icons/AvailableJobs/Iconionic-ios-arrow-forward.png';
import web_designer from '../../assest/Icons/AvailableJobs/Layer1.png';
import doctor from '../../assest/Icons/AvailableJobs/Layer2.png';
import backend from '../../assest/Icons/AvailableJobs/Layer3.png';
import graphics from '../../assest/Icons/AvailableJobs/Layer4.png';
import security from '../../assest/Icons/AvailableJobs/Layer5.png';

import {connect} from 'react-redux';
import {startLoader,stopLoader,SetUserId,SetToken} from '../../redux/action'
import MainBottom from '../../components/MainBottom';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

import axios from 'react-native-axios';
class AvailableJobscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        ListItem:[
            // {id:"1",text:"Data Entry Job",img:require('../../assest/Icons/AvailableJobs/Layer6.png'),posts:"",forward:require('../../assest/Icons/AvailableJobs/Iconionic-ios-arrow-forward.png')},
            // {id:"2",text:"Data Entry Jobs",img:require('../../assest/Icons/AvailableJobs/Layer6.png'),posts:"",forward:require('../../assest/Icons/AvailableJobs/Iconionic-ios-arrow-forward.png')}
        ]
    };
  }

  GetListItem(item) {
    Alert.alert(item);
  }

  componentDidMount = ()=>{
    console.log(this.props.route.params.category)
    console.log(this.props.route.params.category_id)
    //console.log(this.props.route.params.otp)
    this.GetSubCategory()

 }

//  getTokenData = async () => {
//     try {
//       const value = await AsyncStorage.getItem('token')
//       console.log(value)
//       const val=JSON.parse(value)
//       if(value !== null) {
//         console.log(val.token,value)
//         console.log(val.customerid,value)

//         this.setState({
//           token:val.token,customer_id:val.customerid
//         });

//         this.GetSubCategory()
        
//       }
//     } catch(e) {
//       // error reading value
//     }
//   }

  GetSubCategory=()=>{

    this.props.startLoader()

    const url="https://pravasiconnect.websitepreview.in/api/get-sub-category"
    
    const params={
      category_id:this.props.route.params.category_id
    }

    const header = {
      'Content-Type': 'application/json',
      'X-Requested-With': `XMLHttpRequest`,
      }


      console.log(params)
      axios.post(url,params,header)
      .then((response) => {
        this.props.stopLoader()
        console.log(response.data);


        if(response.data.status==true)
        {
           this.setState({
            ListItem:response.data.data,
            image:response.data.data.image,
           })
        }



    })
    .catch((error) => {
      console.log(error);
    });
  }

  render() {
    const { navigation } = this.props;
    return (
        <SafeAreaView style={{backgroundColor:'#fff',flex:1}}>
        <AppHeader {...this.props}/>
        <ScrollView showsVerticalScrollIndicator={false}
               keyboardShouldPersistTaps="handled">
      <View>
        <Text style={styles.heading}> Available {this.props.route.params.category} </Text>
      </View>

      {this.state.ListItem==""?<Text style={styles.casehandle}>No Data Available</Text>:
      
      <FlatList
       data={this.state.ListItem}
       renderItem={({item})=>
       <TouchableOpacity  style={styles.view} onPress={()=> this.props.navigation.navigate("dataentry",{sub_category:item.title,subcategory_id:item.id,categoryid:this.props.route.params.category_id})}>
       <View>
           <Image source={{uri:item.image}} style={styles.report}></Image>
          </View>
          <View style={{width:windowWidth/3.2}}>
              <Text style={styles.job1}>{item.title}</Text>
              <Text style={styles.jobdesc}>{item.posts}</Text>
          </View>
          <TouchableOpacity style={{height:windowHeight/25,width:windowWidth/4,marginLeft:windowWidth/6,alignSelf:'center'}}>
              <Image source={forward_arrow} style={[styles.report1,{justifyContent:'center'}]}></Image>
          </TouchableOpacity>
        </TouchableOpacity>
       }
      />
      }
          
      
      </ScrollView>
      <MainBottom {...this.props}/>
      </SafeAreaView>
    );
  }
}


const styles=StyleSheet.create({
    heading:{
        fontSize:22,
        fontWeight:'bold',
        marginTop:windowHeight/90,
        marginLeft:windowWidth/12,
        flexDirection: 'row',

    },

    casehandle:{
      fontSize:18,
      marginTop:windowHeight/3,
      marginLeft:windowWidth/3.5,
      flexDirection: 'row',
    },

    view:{
        padding:windowHeight/120,
        backgroundColor:'#fff',
        flex:1,
        width:windowWidth/1,
        alignSelf:'center',
        flexDirection: 'row',
        marginLeft:windowWidth/30,
          marginTop:windowHeight/40,
          
    },

    report:{
        width:windowWidth/10,
        height:windowWidth/15,
       
        margin:'10%',
     
       
        
       },
       job1:{
        fontSize:13,
       marginLeft:windowHeight/500,
        fontWeight:'bold',
        color:'#2051AE'
       },
       jobdesc:{
        fontSize:11,
       marginLeft:windowHeight/500,
        color:'#000',
       },

    report1:{
        margin:'5%',
        marginLeft: windowWidth/8,
      
       },
})




//export default AvailableJobscreen;

const mapStateToProps = (state) => {
  return {
    isLoggedIn:state.global.isLoggedIn,
    location:state.global.location,
    loginData:state.global.loginData,
    userID:state.global.userID,
    Token:state.global.Token
    
  };
};

export default connect(mapStateToProps, {startLoader,stopLoader,SetUserId,SetToken})(
  AvailableJobscreen,
);
