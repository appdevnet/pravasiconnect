export const increase=()=>({
    type: 'INCREASE'
  });

export const setCustomerId=(customerId)=>({
    type: 'SET_CUSTOMER_ID',
    payload:customerId
  });

export const setLocation=(data)=>({
    type: 'SET_Location',
    payload:data
  });

export const setIsLoggedin=(value)=>({
    type: 'SET_ISLOGGED',
    payload:value
  });

  export const removeLoginData =() =>({
    type: 'RESET_REDUX',
  })

  export const setLoginData = (data) =>({
    type: 'SET_LOGIN_DATA',
    payload:data
  })

  export const startLoader = () =>({
    type: 'START_LOADER',
  })
  export const stopLoader = () =>({
    type: 'STOP_LOADER',
  })

  export const setFormMap = (data) =>({
    type: 'SET_FORM_MAP',
    locationData:data
    })


  export const SetUserId = (data) =>({
    type:'SET_USER_ID',
    payload:data
  })

  export const SetToken = (data) =>({
    type: 'SET_USER_TOKEN',
    payload:data
  })