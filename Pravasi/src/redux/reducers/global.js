import { isLoggedIn } from "../../utils/asyncStorage";

const INITAL_STATE={
    value: 0,
    customerId:0,
    location:{},
    isLoggedIn:false,
    loginData:"",
    isLoding:false,
    formMap:"",
    userID:"",
    Token:"",
   };
   export default (state = INITAL_STATE, action) => {
    console.log(state)
    switch(action.type){
     case 'SET_CUSTOMER_ID':
         console.log(action.payload,"inside reducerrrr")
      return {...state, customerId: action.payload};
     case 'SET_Location':
      return {...state, location: action.payload};
     case 'SET_ISLOGGED':
      return {...state, isLoggedIn: action.payload};
     case 'RESET_REDUX':
      return {...state,isLoggedIn:false,customerId:0,location:{},loginData:""};
     case 'SET_LOGIN_DATA':
      return {...state,loginData:action.payload};
     case 'START_LOADER':
      return {...state,isLoding:true};
     case 'STOP_LOADER':
      return {...state,isLoding:false};
     case 'SET_FORM_MAP':
      return {...state,formMap:action.locationData};
     case 'SET_USER_ID':
       return {...state,userID:action.payload};
     case 'SET_USER_TOKEN':
       return {...state,Token:action.payload};
     default:
      return state; 
    }
   }