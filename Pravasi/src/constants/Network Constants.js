const BASE_URL = "https://pravasiconnect.websitepreview.in/"

export const createPost = BASE_URL + 'api/add-post';
export const getFaq = BASE_URL + 'api/get-faqs';