import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../screens/Loginscreen'
import ResetPassword from '../screens/Resetpassword'
import SplashScreen from '../screens/Splashscreen'
import RegistrationScreen from '../screens/Registerscreen'
import LocalBusinessDirectory from '../screens/LocalBusinessDirectory'
import CreatePostScreen from '../screens/CreatePostScreen'
import AppHeader from '../components/AppHeader'
import AbroadServiceScreen from '../screens/AbroadServicescreen'
import MapScreen from '../screens/MapScreen'
import AccountSettingscreen from '../screens/AccountSettingscreen'
import Dashboardscreen from '../screens/Dashboardscreen'
import KeralaServicescreen from '../screens/KeralaServicescreen'
import Pdscreen from '../screens/P&DScreen'
import profile from '../screens/Profilescreen'

import { navigationRef } from '../../src/RootNavigation';
import BookCabscreen from '../screens/BookCabscreen/BookCabscreen';
import callmessage from '../components/callmessage';
import MainBotton from '../components/MainBottom';
import AvailableJobscreen from '../screens/AvailableJobscreen';
import UserProfilescreen from '../screens/UserProfilescreen';
import Thankyouscreen from '../screens/Thankyouscreen';
import Dataentryscreen from '../screens/DataEntryScreen';
import Messagescreen from '../screens/MessageScreen';
import QAscreen from '../screens/QAScreen';
import Notificationscreen from '../screens/Notificationscreen';
import FAQscreen from '../screens/FAQscreen';
import FAQDetailscreen from '../screens/FAQDetailscreen';
import Answerscreen from '../screens/Answerscreen';
import TestMap from '../screens/TestMap/TestMap';
import VerifyOTPscreen from '../screens/VerifyOTPscreen';
import ChangePasswordscreen from '../screens/ChangePasswordscreen';
import verifyotpreg from '../screens/VerifyOTPForRegscreen';
import VerifyOtpProfilescreen from '../screens/VerifyOtpProfilescreen';
import CreatePostCategoryScreen from '../screens/CreatePostCategoryScreen';
import CreatePostSubCategoryScreen from '../screens/CreatePostSubCategoryScreen';
import CreatePostImageScreen from '../screens/CreatePostImageScreen';
import LocationScreen from '../screens/LocationScreen'


const Stack = createStackNavigator();

const index = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator screenOptions={{headerShown: false, }}>
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="login" component={LoginScreen} />
        <Stack.Screen name="resetpassword" component={ResetPassword} />
        <Stack.Screen name="register" component={RegistrationScreen} />
        <Stack.Screen name="localBusinessDirectory" component={LocalBusinessDirectory} /> 
        <Stack.Screen name="CreatePost" component={CreatePostScreen} />
        <Stack.Screen name="CreatePostImages" component={CreatePostImageScreen} />
        <Stack.Screen name="CreatePostCategory" component={CreatePostCategoryScreen} />
        <Stack.Screen name="CreatePostSubCategory" component={CreatePostSubCategoryScreen} />
        <Stack.Screen name="abroadservice" component={AbroadServiceScreen} /> 
        <Stack.Screen name="mapscreen" component={MapScreen} />
        <Stack.Screen name="bookcabscreen" component={BookCabscreen} />
        <Stack.Screen name="availablejobscreen" component={AvailableJobscreen} />
        <Stack.Screen name="settingscreen" component={AccountSettingscreen} />
        <Stack.Screen name="thankyouscreen" component={Thankyouscreen} />
        <Stack.Screen name="userprofilescreen" component={UserProfilescreen} />
        <Stack.Screen name="pdscreen" component={Pdscreen} />
        <Stack.Screen name="keralascreen" component={KeralaServicescreen} />
        <Stack.Screen name="profile" component={profile} />
        <Stack.Screen name="dataentry" component={Dataentryscreen} />
        <Stack.Screen name="messagescreen" component={Messagescreen} />
        <Stack.Screen name="qascreen" component={QAscreen} />
        <Stack.Screen name="notificationscreen" component={Notificationscreen} />
        <Stack.Screen name="FAQscreen" component={FAQscreen} />
        <Stack.Screen name="FAQDetailscreen" component={FAQDetailscreen} />
        <Stack.Screen name="Answerscreen" component={Answerscreen} /> 
        <Stack.Screen name="verifyotp" component={VerifyOTPscreen} />
        <Stack.Screen name="changepassword" component={ChangePasswordscreen} />
        <Stack.Screen name="verifyotpreg" component={verifyotpreg} />
        <Stack.Screen name="verifyotpprofile" component={VerifyOtpProfilescreen} />
        <Stack.Screen name="location" component={LocationScreen} />
        {/* <Stack.Screen name="testscreen" component={TestMap} />  */}
              </Stack.Navigator>
    </NavigationContainer>
  );
};


export default index;
