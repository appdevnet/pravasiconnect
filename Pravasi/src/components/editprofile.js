{/* mobilenumber modal */}
<View style={styles.centeredView}>
<Modal            
  animationType = {"slide"}  
  transparent = {true}  
  visible = {this.state.isEdtMobVisible}  
  onRequestClose = {() =>{ console.log("Modal has been closed.") } }>  
  {/*All views of Modal*/}  
      <View style = {styles.modal}>  

      <Text style={{fontSize:13,fontWeight:'bold',marginTop:windowHeight/100,padding:15}}>Edit the Mobile</Text>

      <PhonenumberInput
isValidPhone={this.state.isValidPhone}
selectedCode={(code)=>{console.log(code)}}
onChangeText={(text)=>{this.setState({phoneNumber:text,isValidPhone:true})}}
/>
    
     <TouchableOpacity
      style={[styles.button, styles.buttonClose,{marginTop:windowHeight/50}]}
      onPress={() =>{this.props.navigation.navigate('resetpassword',{isMail:false}),this.setState({ isVisible:!this.state.isVisible})}}
    ><Text style={styles.textStyle}>VERIFY</Text></TouchableOpacity> 
    <Text style={{color:'black',marginTop:windowHeight/45,fontSize:15,fontWeight:'bold'}}
    onPress = {() => {  
          this.setState({ isEdtMobVisible:!this.state.isEdtMobVisible})}}>CLOSE</Text>
      
  </View>   
</Modal>
</View>  

{/* email modal */}

<View style={styles.centeredView}>
<Modal            
  animationType = {"slide"}  
  transparent = {true}  
  visible = {this.state.isEdtEmailVisible}  
  onRequestClose = {() =>{ console.log("Modal has been closed.") } }>  
  {/*All views of Modal*/}  
      <View style = {styles.modal}>  

      <Text style={{fontSize:13,fontWeight:'bold',marginTop:windowHeight/100,padding:15}}>Edit the Email</Text>

      <View style={[styles.inputView,]}>
<TextInput
  style={styles.TextInput}
  placeholder={"Enter email"}
  placeholderTextColor={"#A5A5A5"}
  color='black'
  keyboardType='email-address'
  onChangeText={text => this.setState({username:text,uname:true})}
  // onChangeText={(email) => setEmail(email)}
/>

</View>
    
     <TouchableOpacity
      style={[styles.button, styles.buttonClose,{marginTop:windowHeight/50}]}
      onPress={() =>{this.props.navigation.navigate('resetpassword',{isMail:false}),this.setState({ isVisible:!this.state.isVisible})}}
    ><Text style={styles.textStyle}>VERIFY</Text></TouchableOpacity> 
    <Text style={{color:'black',marginTop:windowHeight/45,fontSize:15,fontWeight:'bold'}}
    onPress = {() => {  
          this.setState({ isEdtEmailVisible:!this.state.isEdtEmailVisible})}}>CLOSE</Text>
      
  </View>   
</Modal>
</View>  

{/* userid modal */}

<View style={styles.centeredView}>
<Modal            
  animationType = {"slide"}  
  transparent = {true}  
  visible = {this.state.isEdtUserIDVisible}  
  onRequestClose = {() =>{ console.log("Modal has been closed.") } }>  
  {/*All views of Modal*/}  
      <View style = {styles.modal}>  

      <Text style={{fontSize:13,fontWeight:'bold',marginTop:windowHeight/100,padding:15}}>Edit the UserID</Text>

      <View style={[styles.inputView,]}>
<TextInput
  style={styles.TextInput}
  placeholder={"Enter User ID"}
  placeholderTextColor={"#A5A5A5"}
  color='black'
  keyboardType='email-address'
  onChangeText={text => this.setState({username:text,uname:true})}
  // onChangeText={(email) => setEmail(email)}
/>

</View>
    
     <TouchableOpacity
      style={[styles.button, styles.buttonClose,{marginTop:windowHeight/50}]}
      onPress={() =>{this.props.navigation.navigate('resetpassword',{isMail:false}),this.setState({ isVisible:!this.state.isVisible})}}
    ><Text style={styles.textStyle}>SUBMIT</Text></TouchableOpacity> 
    <Text style={{color:'black',marginTop:windowHeight/45,fontSize:15,fontWeight:'bold'}}
    onPress = {() => {  
          this.setState({ isEdtUserIDVisible:!this.state.isEdtUserIDVisible})}}>CLOSE</Text>
      
  </View>   
</Modal>
</View>  

{/* whatsapp modal */}

<View style={styles.centeredView}>
<Modal            
  animationType = {"slide"}  
  transparent = {true}  
  visible = {this.state.isEdtWhatsAppVisible}  
  onRequestClose = {() =>{ console.log("Modal has been closed.") } }>  
  
      <View style = {styles.modal}>  

      <Text style={{fontSize:13,fontWeight:'bold',marginTop:windowHeight/100,padding:15}}>Edit the WhatsApp Number</Text>

      <View style={[styles.inputView,]}>
<TextInput
  style={styles.TextInput}
  placeholder={"Enter Whatsapp Number"}
  placeholderTextColor={"#A5A5A5"}
  color='black'
  keyboardType='email-address'
  onChangeText={text => this.setState({username:text,uname:true})}
  
/>

</View>
    
     <TouchableOpacity
      style={[styles.button, styles.buttonClose,{marginTop:windowHeight/50}]}
      onPress={() =>{this.props.navigation.navigate('resetpassword',{isMail:false}),this.setState({ isVisible:!this.state.isVisible})}}
    ><Text style={styles.textStyle}>SUBMIT</Text></TouchableOpacity> 
    <Text style={{color:'black',marginTop:windowHeight/45,fontSize:15,fontWeight:'bold'}}
    onPress = {() => {  
          this.setState({ isEdtWhatsAppVisible:!this.state.isEdtWhatsAppVisible})}}>CLOSE</Text>
      
  </View>   
</Modal>
</View>  

{/* skype modal */}

<View style={styles.centeredView}>
<Modal            
  animationType = {"slide"}  
  transparent = {true}  
  visible = {this.state.isEdtSkypeVisible}  
  onRequestClose = {() =>{ console.log("Modal has been closed.") } }>  

      <View style = {styles.modal}>  

      <Text style={{fontSize:13,fontWeight:'bold',marginTop:windowHeight/100,padding:15}}>Edit the Skype ID</Text>

      <View style={[styles.inputView,]}>
<TextInput
  style={styles.TextInput}
  placeholder={"Enter skype ID"}
  placeholderTextColor={"#A5A5A5"}
  color='black'
  keyboardType='email-address'
  onChangeText={text => this.setState({username:text,uname:true})}
  
/>

</View>
    
     <TouchableOpacity
      style={[styles.button, styles.buttonClose,{marginTop:windowHeight/50}]}
      onPress={() =>{this.props.navigation.navigate('resetpassword',{isMail:false}),this.setState({ isVisible:!this.state.isVisible})}}
    ><Text style={styles.textStyle}>SUBMIT</Text></TouchableOpacity> 
    <Text style={{color:'black',marginTop:windowHeight/45,fontSize:15,fontWeight:'bold'}}
    onPress = {() => {  
          this.setState({ isEdtSkypeVisible:!this.state.isEdtSkypeVisible})}}>CLOSE</Text>
      
  </View>   
</Modal>
</View>