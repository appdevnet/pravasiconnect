import React, { Component } from 'react';
import { View, Text,StyleSheet,Dimensions,TextInput} from 'react-native';
 import CountryJSON from "../countrycode/CountryPicker/countries.json";
 import CountryPicker from 'rn-country-picker';


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class PhonenumberInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
        mCountryCode: "91"
    };

    const userLocaleCountryCode = "";
    // userLocaleCountryCode = DeviceInfo.getDeviceCountry();
 
     try {
       if (userLocaleCountryCode) {
         const newData = CountryJSON.filter(function(item) {
           const itemData = item.name.common.toUpperCase();
           const textData = userLocaleCountryCode.toUpperCase();
           return itemData.startsWith(textData);
         });
         if (newData.length > 0) {
           this.state.mCountryCode = newData[0].callingCode;
         } else {
           this.setState({ mCountryCode: "91" });
         }
       } else {
         this.setState({ mCountryCode: "91" });
       }
     } catch (e) {
       console.error(e.message);
     }
  }

  _selectedValue = index => {
    this.setState({ mCountryCode: index });
    this.props.selectedCode("+"+index)
  };
componentDidMount(){
  if(this.props.code){
    this.setState({
      mCountryCode:this.props?.code.substring(1)
    })
  }

  if(this.props.value){
    this.setState({value:this.props.value})
  }
}
  render() {
    return (
        <>
        <View style={{width:windowWidth,flexDirection:'column',left:windowWidth/12.5}}>
        <View style={[styles.inputView,{flexDirection:'row'},{borderColor:this.props.error==""?null:"red",borderWidth:this.props.error==""?0:1}]}>
        <View style={{justifyContent:'center',height:windowHeight/10,alignSelf:'center',marginTop:windowHeight/75,marginLeft:windowWidth/60,color:"#EBF1FF"}}>
      <CountryPicker
      disable={false}
      animationType={'slide'}
      containerStyle={styles.pickerStyle}
      pickerTitleStyle={styles.pickerTitleStyle}
      dropDownImage={require("../countrycode/res/ic_drop_down.png")}
      selectedCountryTextStyle={styles.selectedCountryTextStyle}
      countryNameTextStyle={styles.countryNameTextStyle}
      pickerTitle={"Country Picker"}
      searchBarPlaceHolder={"Search......"}
      hideCountryFlag={false}
      hideCountryCode={false}
      searchBarStyle={styles.searchBarStyle}
      backButtonImage={require("../countrycode/res/ic_back_black.png")}
      searchButtonImage={require("../countrycode/res/ic_search.png")}
      countryCode={this.state.mCountryCode}
      selectedValue={this._selectedValue}
    /></View>
    <TextInput
      
      style={[styles.TextInput,{width:windowWidth/2}]}
      value={this.props.value}
      placeholder={this.props.error==""?"Mobile Number":this.props.error}
      placeholderTextColor={this.props.error==""?"#777777":"red"}
      keyboardType='phone-pad'
      onChangeText={text => {this.setState({value:text}),this.props.onChangeText((text))}}
      
    />
        </View>
        {this.props.error!=""&&this.props.value!=""?(
        <Text
        numberOfLines={1}
          style={{
            fontSize: windowWidth / 35,
            left:windowWidth/20,
            top: -20,
            color: 'red',
          }}>
          * {this.props.error}
        </Text>):null
        }
        </View>
        </>
    );
  }
}
const styles=StyleSheet.create({
    inputView: {
        backgroundColor: "#EBF1FF",
        borderRadius: 12,
        width: windowWidth/1.2,
        height: windowHeight/15,
        marginBottom: windowWidth/15,
    
      },
      TextInput: {
        width:windowWidth/1.4,
        height: windowHeight/15,
        paddingHorizontal: 10,
        marginHorizontal: 10,
        color:'#000000'
        //backgroundColor:"red"
      },

      pickerTitleStyle: {
        justifyContent: "center",
        flexDirection: "row",
        alignItems: "center",
        textAlign: "center",
        alignSelf: "center",
        fontWeight: "bold",
        flex: 1,
        marginTop: 5,
        fontSize: 15,
        color: "#000",
      },
      pickerStyle: {
        height: windowHeight/25,
        width: windowWidth/3.8,
        marginBottom:10,
        justifyContent: "center",
        padding: 10,
        borderWidth: 2,
        borderColor: "#EBF1FF",
        backgroundColor: "#EBF1FF"
      },
      selectedCountryTextStyle: {
        paddingLeft: 5,
        paddingRight: 5,
        color: "#000",
        textAlign: "right",
      },
    
      countryNameTextStyle: {
        paddingLeft: 10,
        color: "#000",
        textAlign: "right"
      },
      searchBarStyle: {
        flex: 1,
        borderRadius: 50,
        borderWidth: 4,
    
        borderColor: "#D3D3D3",
        justifyContent: "center",
        flexDirection: "row",
        marginTop: 10,
        marginLeft: 8,
        marginBottom: 5,
        marginRight: 12,
        paddingLeft: 20,
        paddingRight: 10
      },
})
export default PhonenumberInput;
