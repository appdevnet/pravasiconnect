import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView,
  AsyncStorage,
  Dimensions,
  Alert,
  Pressable,
  Modal
} from "react-native";

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class CustomRadio extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{flex:1,flexDirection:"row",marginBottom: 20,}}>
            <View style={styles.uncoloredStyle}></View>
            <Text style={{ marginTop: windowWidth/120,marginHorizontal: windowWidth/20, }}>Call</Text>


            <View style={styles.coloredStyle}>
                <View style={styles.insideColor}></View>
            </View>
            <Text style={{ marginTop: windowWidth/120,marginHorizontal: windowWidth/20, }}>Message</Text>
        </View>
        
    );
  }
}

const styles = StyleSheet.create({
    uncoloredStyle: {
        height: windowHeight / 38,
        width: windowHeight / 38,
        backgroundColor: "#d2d6d3",
        borderRadius: 100,
        borderWidth: 0.5,
        borderColor: "#d2d6d3",
        marginLeft: windowWidth / 10,
        marginTop: windowHeight / 250
    },

    coloredStyle:{
        height: windowHeight / 38,
        width: windowHeight / 38,
        backgroundColor: "#fff",
        borderRadius: 100,
        borderWidth: 0.5,
        borderColor: "#0D3369",
        justifyContent: "center",
        marginLeft: windowWidth / 15,
        marginTop: windowHeight / 250
    },

    insideColor: {
        height: windowHeight / 52,
        width: windowHeight / 52,
        backgroundColor: "#0D3369",
        justifyContent: "center",
        alignSelf: "center",
        borderRadius: 30
    }
})
export default CustomRadio;
