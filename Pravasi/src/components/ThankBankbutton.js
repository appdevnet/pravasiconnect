import React, { Component } from 'react';
import { View, Text,StyleSheet,TouchableOpacity } from 'react-native';

class ThankBankbutton extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.TabBarMainContainer} >

      <View style={{height: 50, backgroundColor: '#fff', width: 3}} />


      <View style={{height: 50, backgroundColor: '#fff', width: 3}} />

      <TouchableOpacity  activeOpacity={0.6} style={styles.button} >
      
        <Text style={styles.TextStyle}>BACK TO HOME</Text>

      </TouchableOpacity>

  </View>
    );
  }
}


const styles = StyleSheet.create({
 
  TabBarMainContainer :{
    justifyContent: 'space-around', 
    height: 50, 
    flexDirection: 'row',
    width: '100%',
  },
   
  button: {
    height: 60,
    marginTop:300,
    marginLeft:-7,
    backgroundColor: '#2051AE',
    justifyContent: 'center', 
    alignItems: 'center', 
    flexGrow: 2,
  },
   
  TextStyle:{
      color:'#fff',
      textAlign:'center',
      fontSize: 17,
      fontWeight:'bold',
  }
   
  });

export default ThankBankbutton;
