import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import arrow from '../assest/iconss/back_arrow.png'


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class AppHeaderBack extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

    render() {
      
    return (
        <View style={[styles.appHedr]}>
        <TouchableOpacity style={{height:windowWidth/15,width:windowWidth/20,marginTop:windowWidth/15}}
        onPress={()=>this.props.navigation.goBack()}>
            <TouchableOpacity 
           onPress={()=>this.props.navigation.goBack()} >
            <Image
                style={styles.image_size}
                source={arrow}
            />
            </TouchableOpacity>
            </TouchableOpacity>
            {/* <View style={styles.loctn_txt_gap}>
                <Image
                style={styles.image_size}
                source={require('../assest/iconss/locations.jpeg')}
            />
                <Text style={styles.txt}>Sharjah</Text>
            </View> */}
            
      </View>
    );
  }
}

const styles = StyleSheet.create({
    appHedr: {
        width: windowWidth,
        paddingHorizontal:windowWidth/13,
        flexDirection: "row",
        justifyContent: "space-between",
    },

    image_size: {
       
        height: windowHeight/50,
       width:windowHeight/50
    },
   
    txt: {
        fontSize: 12,
        right: windowWidth/60,
        left: windowWidth/50,
        color:"black"
    },
    
    locatn_icon: {
        height: windowHeight/30,
        width:windowWidth/30
    },
    
    loctn_txt_gap: {
        flexDirection: 'row',
        right:windowWidth/22
    }
    
})

export default AppHeaderBack;
