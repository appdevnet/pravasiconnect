import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
    TextInput,
  FlatList,
  Button,
  TouchableOpacity,
  SafeAreaView,
  ScrollView
} from "react-native";

class ContactInformationProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <SafeAreaView>
            <View style={styles.container}>

                <View style={styles.contact_listitem}>
                    
                    <View style={styles.image_view}>
                        <Image
                            resizeMode={'contain'}
                            style={styles.image}
                            source={require('../assest/Icons/Profile/Path110.png') }/>
                    </View>
                    
                    <View style={styles.txt_view}>
                        <Text style={styles.txt1}>Mobile</Text>
                        <Text style={styles.txt2}>+123 456 789 234</Text>
                    </View>

                    <View style={styles.edit_icon_view}>
                        <Image
                            resizeMode={'contain'}
                            style={styles.edit_icon}
                            source={require('../assest/Icons/Profile/Iconmaterial-edi.png')}
                        />
                    </View>
                </View>

                <View style={styles.contact_listitem}>
                    
                    <View style={styles.image_view}>
                        <Image
                            resizeMode={'contain'}
                            style={styles.image}
                            source={require('../assest/Icons/Profile/Path50.png') }/>
                    </View>
                    
                    <View style={styles.txt_view}>
                        <Text style={styles.txt1}>Email</Text>
                        <Text style={styles.txt2}>Nidheeshkommath@gmail.com</Text>
                    </View>

                    <View style={styles.edit_icon_view}>
                        <Image
                            resizeMode={'contain'}
                            style={styles.edit_icon}
                            source={require('../assest/Icons/Profile/Iconmaterial-edi.png')}
                        />
                    </View>
                </View>

                <View style={styles.contact_listitem}>
                    
                    <View style={styles.image_view}>
                        <Image
                            resizeMode={'contain'}
                            style={styles.image}
                            source={require('../assest/Icons/Profile/Iconmaterial-my-location.png') }/>
                    </View>
                    
                    <View style={styles.txt_view}>
                        <Text style={styles.txt1}>Country Name</Text>
                        <Text style={styles.txt2}>Australia</Text>
                    </View>

                    <View style={styles.edit_icon_view}>
                        <Image
                            resizeMode={'contain'}
                            style={styles.edit_icon}
                            source={require('../assest/Icons/Profile/Iconmaterial-edi.png')}
                        />
                    </View>
                </View>

                <View style={styles.contact_listitem}>
                    
                    <View style={styles.image_view}>
                        <Image
                            resizeMode={'contain'}
                            style={styles.image}
                            source={require('../assest/Icons/Profile/Iconawesome-facebook-f.png') }/>
                    </View>
                    
                    <View style={styles.txt_view}>
                        <Text style={styles.txt1}>Facebook</Text>
                        <Text style={styles.txt2}>/nidheeshkommath</Text>
                    </View>

                    <View style={styles.connect_view}>
                        <Text style={styles.connect_txt}>CONNECT</Text>
                    </View>
                </View>

                <View style={styles.contact_listitem}>
                    
                    <View style={styles.image_view}>
                        <Image
                            resizeMode={'contain'}
                            style={styles.image}
                            source={require('../assest/Icons/Profile/Iconmetro-instagram.png') }/>
                    </View>
                    
                    <View style={styles.txt_view}>
                        <Text style={styles.txt1}>Instagram</Text>
                        <Text style={styles.txt2}>/nidheeshkommath</Text>
                    </View>

                    <View style={styles.connect_view}>
                        <Text style={styles.connect_txt}>CONNECT</Text>
                    </View>
                </View>
                
      </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginVertical:20,
        
    },

    contact_listitem: {
        flexDirection: "row",
        justifyContent:"space-around",
        marginHorizontal: "10%",
        paddingHorizontal: 10,
        backgroundColor: "#e4ebea",
        height: 70,
        borderRadius:12,
        width: "80%",
        marginBottom:20,

    },

    image_view: {
        flex: 0.5,
        flexDirection: "column",
        justifyContent:"center"
    },

    image: {
        height: 20,
        width:30
    },

    txt1: {
        fontSize: 10,
        color: "#04086e",
        fontWeight:"600"
    },

    txt2: {
        fontSize:12
    },


    txt_view: {
        flex: 2,
        flexDirection: "column",
        justifyContent:"center"
        
    },

    edit_icon: {
        height: 20,
        width:30
    },

    edit_icon_view: {
        flex: 0.5,
        flexDirection: "column",
        justifyContent:"center"
    },

    connect_view: {
        backgroundColor: "#04086e",
        width: 80,
        height: 30,
        justifyContent: "center",
        alignSelf:"center",
        flexDirection: "column",
        borderRadius:12
    },

    connect_txt: {
        fontSize: 10,
        color: "#fff",
        justifyContent: "center",
        alignSelf:"center",
        fontWeight:"500"
    }
})

export default ContactInformationProfile;
