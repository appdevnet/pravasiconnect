import React, {Component} from 'react';
import {Text, View,Dimensions,StyleSheet,TextInput} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
export class TextInputBox extends Component {
  constructor(props){
    super(props);

  }
  render() {
    return (
      <>
      <View style={{width:windowWidth,flexDirection:'column',left:windowWidth/12.5,}}>
        <View
          style={[
            styles.inputView,
            {
              borderColor: this.props.error=="" ? null : 'red',
              borderWidth: this.props.error=="" ? 0 : 1,
              flexDirection: 'column',
              height: this.props.multiline?windowHeight/7:windowHeight / 15,
            },
          ]}>
            <TextInput
            editable={this.props.editable?this.props.editable:true}
            onFocus={this.props?.onFocus}
            multiline={this.props.multiline?true:false}
            style={[styles.TextInput,{top:this.props.multiline?10:0,height:this.props.multiline?windowHeight/9: windowHeight/15}]}
            value={this.props.value}
            placeholder={
                this.props?.error=="" ? this.props.placeholder : this.props.error
            }
            // placeholder={this.props.placeholder}
            placeholderTextColor={this.props?.error=="" ? '#777777' : 'red'}
            // placeholderTextColor={"#777777"}
            onChangeText={text => this.props?.onChangeText(text)}
          />
         
        </View>
        {this.props.error!=""&&this.props.value!=""?(
        <Text
        numberOfLines={1}
          style={{
            // backgroundColor:"red",
            width:windowWidth/1.3,
            fontSize: windowWidth / 35,
            left:windowWidth/36,
            top: -20,
            color: 'red',
          }}>
          * {this.props.error}
        </Text>
        ):null
        } 
        </View>
        </>
    );
  }
}
const styles = StyleSheet.create({
    TextInput: {
        width:windowWidth/1.29,
        paddingHorizontal: 10,
        marginHorizontal: 10,
    color: 'black'
        // windowHeight/7,
        //backgroundColor:"red"
      },
      inputView: {
        backgroundColor: "#EBF1FF", /* #F6F8FC */ /* #EBF1FF */
        borderRadius: 12,
        width: windowWidth/1.2,
        height: windowHeight/15,
        marginBottom: windowWidth/15,
    
      },
})
export default TextInputBox;
