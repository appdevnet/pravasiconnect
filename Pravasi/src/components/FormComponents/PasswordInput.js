import React, {Component} from 'react';
import {
  Text,
  View,
  Dimensions,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import {eyeOpen, eyeClosed} from '../../constants/Icons';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
class PasswordInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showpassword: false,
    };
  }
  componentDidMount() {
    console.log('passsworddddprops', this.props.error);
  }
  render() {
    return (
      <>
        <View
          style={{
            width: windowWidth,
            flexDirection: 'column',
            left: windowWidth / 12.5,
          }}>
          <View
            style={[
              styles.inputView,
              {
                borderColor: this.props.error == '' ? null : 'red',
                borderWidth: this.props.error == '' ? 0 : 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
              },
            ]}>
            <TextInput
              style={[styles.TextInput, {width: windowWidth / 1.45}]}
              value={this.props.value}
            placeholder={
                this.props?.error=="" ? this.props.placeholder : this.props.error
            }
              secureTextEntry={this.state.showpassword ? false : true}
              placeholderTextColor={this.props.error == '' ? '#777777' : 'red'}
              onChangeText={text => this.props.onChangeText(text)}
            />
            <TouchableOpacity
              onPress={() =>
                this.setState({showpassword: !this.state.showpassword})
              }
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                marginRight: 50,
              }}>
              {this.state.showpassword ? (
                <Image
                  source={eyeOpen}
                  style={{
                    height: windowHeight / 45,
                    width: windowWidth / 20,
                    marginTop: windowHeight / 43,
                  }}
                />
              ) : (
                <Image
                  source={eyeClosed}
                  style={{
                    height: windowHeight / 45,
                    width: windowWidth / 20,
                    marginTop: windowHeight / 43,
                  }}
                />
              )}
            </TouchableOpacity>
          </View>
          {this.props.error != '' && this.props.value != '' ? (
            <Text
              numberOfLines={1}
              style={{
                fontSize: windowWidth / 35,
                left: windowWidth / 20,
                top: -20,
                color: 'red',
              }}>
              * {this.props.error}
            </Text>
          ) : null}
        </View>
      </>
    );
  }
}
const styles = StyleSheet.create({
  TextInput: {
    width: windowWidth / 1.4,
    height: windowHeight / 15,
    paddingHorizontal: 10,
    marginHorizontal: 10,
    color: 'black',
  },
  inputView: {
    backgroundColor: '#EBF1FF',
    borderRadius: 12,
    width: windowWidth / 1.2,
    height: windowHeight / 15,
    marginBottom: windowWidth / 15,
  },
});
export default PasswordInput;
