import React, { Component } from 'react';
import { View, Text ,StyleSheet,TouchableOpacity,SafeAreaView,Image,Dimensions } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import call from '../assest/Icons/BookACab/Iconzocial-call.png';
import msg from '../assest/Icons/BookACab/Iconawesome-envelope-open.png';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
class callmessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <SafeAreaView>
        <View style={styles.TabBarMainContainer} >
 
        <TouchableOpacity  activeOpacity={0.6} style={styles.button} >
        <View style={{marginLeft:windowWidth/6,marginTop:windowHeight/30, flexDirection:'row'}}>
            <Image source={call} style={styles.callstyle}></Image>
          <Text style={styles.TextStyle}> CALL </Text>
          </View> 
        </TouchableOpacity>
 
        <View style={{height: windowHeight/10, backgroundColor: '#fff', width: 2}} />
 
        <TouchableOpacity  activeOpacity={0.6} style={styles.button1} >
        
        <View style={{marginLeft:windowWidth/16,marginTop:windowHeight/30,flexDirection:'row'}}>
            <Image source={msg} style={styles.msgstyle}></Image>
          <Text style={styles.TextStyle1}> MESSAGE </Text>
          </View>
 
        </TouchableOpacity>

    </View>
    </SafeAreaView>
    );
  }
}




const styles = StyleSheet.create({
 
    TabBarMainContainer :{
      justifyContent: 'space-between', 
      height: windowHeight/10, 
      flexDirection: 'row',
      width: windowWidth,
      
    },
     
    button: {
      height: windowHeight/10,
      
      backgroundColor:'#2051AE',
      flexGrow: 1,
      
    },

    button1: {
        height: windowHeight/10,
        
        backgroundColor:'#fff',
        flexGrow: 1,
        
      },
     
    TextStyle:{
        color:'#fff',
        textAlign:'center',
        fontSize: 20,
        fontWeight:'bold',
        marginLeft: windowWidth/180,

    },

    TextStyle1:{
        color:'#2051AE',
        textAlign:'center',
        fontWeight:'bold',
        fontSize: 20,
        
    },
    callstyle:{
      width:windowWidth/20,
  height:windowHeight/45,
  marginTop: windowHeight/220,
  
  
    },
    msgstyle:{
      width:windowWidth/20,
      height:windowHeight/45,
      marginTop: windowHeight/220,
    },
     
    });

    export default callmessage;