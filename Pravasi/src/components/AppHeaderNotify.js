import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity,Dimensions } from 'react-native';

import icon from '../assest/Icons/update/Iconionic-ios-arrow-down.png';
import av1 from '../assest/Icons/Profile/av1.png';
import notify from '../assest/Icons/update/Iconmaterial-notifications-none.png'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class AppHeaderNotify extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

    render() {
      
    return (
        <View style={[styles.appHedr]}>
        <TouchableOpacity style={{height:windowHeight/14,width:windowWidth/7,}}
        onPress={()=>this.props.navigation.goBack()}>
            <TouchableOpacity
           >
            <Image
                style={styles.image_size}
                source={notify}
            />
            </TouchableOpacity>
            </TouchableOpacity>
            <View style={styles.loctn_txt_gap}>
                <Image
                style={styles.image_size}
                source={require('../assest/iconss/locations.jpeg')}
            />
                <Text style={styles.txt}>Sharjah</Text>
                <Image source={icon} style={styles.loc_icon}></Image>
            </View>
            <View>
                <Image source={av1} style={styles.image}></Image>
            </View>
            
      </View>
    );
  }
}

const styles = StyleSheet.create({
    appHedr: {
        paddingVertical: windowHeight/60,
        width:windowWidth,
        paddingHorizontal:windowWidth/13,
        flexDirection: "row",
        justifyContent: "space-between",
    },

   image_size: {
        height: windowHeight/80,
       width:windowHeight/80,
       marginTop:windowHeight/80
    },
   
    txt: {
        fontSize: 12,
        right: windowWidth/60,
        left: windowWidth/50,
        color:"black",
        marginTop:windowHeight/85
    },
    
    locatn_icon: {
        height: windowHeight/30,
        width:windowWidth/30
    },
    
    loctn_txt_gap: {
        flexDirection: 'row',
        right:windowWidth/50
    },

    image:{
        width:windowWidth/8,
        height:windowHeight/60,
        borderRadius:60/2,
        marginLeft:52,
        marginTop:windowHeight/50,
        flex:1,
    },

    loc_icon:{
        marginTop:windowHeight/55,
        marginLeft:windowWidth/30
    }
    
})

export default AppHeaderNotify;
