import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Dimensions,AsyncStorage } from 'react-native';


import icon from '../assest/Icons/update/Iconionic-ios-arrow-down.png';
import back from '../assest/iconss/icon_arrow_left.png';
import av1 from '../assest/Icons/Profile/av1.png';
import notify from '../assest/iconss/notification.png'
import location from '../assest/iconss/icon_location.png'
import arrow from '../assest/iconss/back_arrow.png'
import {connect} from 'react-redux';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
class AppHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
       
    };
  }

  componentDidMount=()=>{
    this.setState({
      place:this.props.location.place
    })
  }

    render() {
      
    return (
      <View style={[styles.appHedr, {backgroundColor: this.props.isNotify ?"#F6F8FC":"#fff" }]}>
        <TouchableOpacity style={{height:windowHeight/14,width:windowWidth/7,flexDirection:'column',justifyContent:'center',flex:0.5,}}
        onPress={()=> this.props.isNotify?this.props.navigation.navigate('notificationscreen'):this.props.navigation.goBack()}>
            <Image
                style={[styles.noti,{alignSelf:"center"}]}
                source={this.props.isNotify?notify:arrow}
            />
            </TouchableOpacity>
            <TouchableOpacity style={{flexDirection:"row",justifyContent:'center',flex:5,}}>
            <View style={[styles.loctn_txt_gap,{alignSelf:'center',marginTop:windowHeight/90}]}>
                <Image
                style={styles.image_size}
                source={location}
            />
                <Text style={styles.txt}>{this.state.place}</Text>
                {/* <Image source={icon} style={styles.loc_icon}></Image> */}
            </View>
            </TouchableOpacity>
            <TouchableOpacity style={{flexDirection:'column',justifyContent:"center",flex:1,}}
          onPress={() => this.props.navigation.navigate(this.props.isLoggedIn?'userprofilescreen':'login')}>
          {!this.props.loginData?.profile_image?
            <Image resizeMode="cover" source={{ uri: 'https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png' }} style={[styles.image, { alignSelf: "center" }]}></Image> :
            <Image resizeMode="cover" source={{ uri: this.props.loginData.profile_image }} style={[styles.image, { alignSelf: "center" }]}></Image>}
            </TouchableOpacity>
            
            
      </View>
    );
  }
}

const styles = StyleSheet.create({
  appHedr: {
      
        paddingVertical: windowHeight/60,
        width:"100%",
        paddingHorizontal:windowWidth/13,
        flexDirection: "row",
        justifyContent: 'space-evenly',
        height: windowHeight / 10,
        
    },

   image_size: {
        height: windowHeight/60,
       width:windowHeight/60,
       marginTop:windowHeight/81,
  },
   
  noti: {
     height: windowHeight/45,
       width:windowHeight/45,
       marginTop:windowHeight/55,
   },
   
    txt: {
        fontSize: 12,
        right: windowWidth/60,
        left: windowWidth/50,
        color:"black",
        marginTop:windowHeight/85
    },
    
    locatn_icon: {
        height: windowHeight/30,
        width:windowWidth/30
    },
    
    loctn_txt_gap: {
        flexDirection: 'row',
        right:windowWidth/50
    },

    image:{
        width:windowWidth/13,
        height:windowWidth/13,
      borderRadius: 60,
        marginTop:windowHeight/60
    },

    loc_icon:{
        marginTop:windowHeight/55,
        marginLeft:windowWidth/30
    }
    
})
const mapStateToProps = (state) => {
  return {
    isLoggedIn:state.global.isLoggedIn,
    location:state.global.location,
    loginData:state.global.loginData
  };
};

export default connect(mapStateToProps, {})(
  AppHeader,
);