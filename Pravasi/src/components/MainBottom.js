import React, { Component } from 'react';
import { View, Text,Dimensions ,StyleSheet,TouchableOpacity,SafeAreaView,Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import home from '../assest/Icons/Navigation/home.png';
import abroad from '../assest/Icons/Navigation/Group293.png';
import kerala from '../assest/Icons/Navigation/kerala.png';
import message from '../assest/Icons/Navigation/message.png';
import profile from '../assest/Icons/Navigation/Path33.png';
import qa from '../assest/Icons/Navigation/qa.png';
import fq from '../assest/Icons/Navigation/faq.png';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
class callmessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { navigation } = this.props;
    return (
        
        <View style={styles.TabBarMainContainer} >
 
 <TouchableOpacity  activeOpacity={0.6} style={styles.button} 
 onPress={()=> navigation.navigate('abroadservice')}>
            <Image source={home} resizeMode="contain" source={home} style={{height:windowWidth/20,width:windowWidth/20}}/>
   <Text style={styles.TextStyle} > Home </Text>

 </TouchableOpacity>

 <View style={{height: 50, backgroundColor: '#fff', width: 2}} />
 
 <TouchableOpacity activeOpacity={0.6} style={styles.button}
 onPress={() => navigation.navigate('keralascreen')} >
 <Image source={kerala} resizeMode="contain" style={{height:windowWidth/20,width:windowWidth/20}}></Image>
   <Text style={styles.TextStyle}> kerala </Text>

 </TouchableOpacity>

 <View style={{height: 50, backgroundColor: '#fff', width: 2}} />
 
 <TouchableOpacity  activeOpacity={0.6} style={styles.button} 
 onPress={() => navigation.navigate('qascreen')}>
 <Image source={qa} resizeMode="contain" style={{height:windowWidth/20,width:windowWidth/20}}></Image>
   <Text style={styles.TextStyle}> Q&A </Text>

 </TouchableOpacity>

 <View style={{height: 50, backgroundColor: '#fff', width: 2}} />
 
 <TouchableOpacity  activeOpacity={0.6} style={styles.button} 
 onPress={()=>navigation.navigate('messagescreen')}>
 <Image source={message} resizeMode="contain" style={{height:windowWidth/20,width:windowWidth/20}}></Image>
   <Text style={styles.TextStyle}> Messages </Text>

 </TouchableOpacity>

 <View style={{height: 50, backgroundColor: '#fff', width: 2}} />
 
 <TouchableOpacity  activeOpacity={0.6} style={styles.button} 
 onPress={()=>navigation.navigate('FAQscreen')}>
 <Image source={fq} resizeMode="contain" style={{height:windowWidth/20,width:windowWidth/20}}></Image>
   <Text style={styles.TextStyle}> FAQ </Text>

 </TouchableOpacity>

</View>

    
    );
  }
}




const styles = StyleSheet.create({
 
    TabBarMainContainer :{
        justifyContent: 'space-around', 
        height: 50, 
    flexDirection: 'row',
    borderTopWidth: 0.5,
        borderColor:"#EEEEEE",
        width: '100%',
       
      },
       
      button: {
        height: 50,
        paddingTop:5,
        paddingBottom:5,
        backgroundColor: '#fff',
        justifyContent: 'center', 
        alignItems: 'center', 
        flexGrow: 1
      },
       
      TextStyle:{
          color:'#585859',
          textAlign:'center',
        fontSize: 10,
          marginTop:3
      }
     
    });

    export default callmessage;