import React, { Component } from "react";
import { ActivityIndicator, StyleSheet, Text, View } from "react-native";
import {connect} from 'react-redux';
class Loader extends Component {
  render() {
    return (
      <>
      {this.props.isLoding&&(
      <View style={[styles.container, styles.horizontal]}>
        {/* <ActivityIndicator /> */}
        <ActivityIndicator size="large" color="#0000ff"/>
        {/* <ActivityIndicator size="small" color="#0000ff" /> */}
        {/* <ActivityIndicator size="large" color="#00ff00" /> */}
      </View>
      )}
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#00000080',
          position: 'absolute',
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});
const mapStateToProps = (state) => {
  return {
    isLoding:state.global.isLoding,
    
  };
};

export default connect(mapStateToProps, {})(
  Loader,
);