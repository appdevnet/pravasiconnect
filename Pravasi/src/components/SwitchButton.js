import { View, Switch, StyleSheet } from 'react-native'

import React, { Component } from 'react';


class SwitchButton extends Component {
  constructor(props) {
    super(props);
      this.state = {
          isEnabled: false       
    };
  }
    
toggleSwitch()
     {  
        this.setState({isEnabled: !this.state.isEnabled})
     }
    render() {
    return (
      <View style = {styles.container}>
            <Switch
            style={{ transform: [{ scaleX: 0.6 }, { scaleY: 0.6 }] }}
            onValueChange={() => this.toggleSwitch()}
            trackColor={{ false: "#767577", true: "#cbcfd4" }}
            thumbColor={this.state.isEnabled ? "#1b0e5c" : "#b2c0d1"}   
            // ios_backgroundColor="#3e3e3e"
            value = {this.state.isEnabled}
            />
      </View>  
    );
  }
}

export default SwitchButton;

const styles = StyleSheet.create ({
   container: {
        flex: 1,
        marginTop:-7,
        left: 20,
    },
    
})



