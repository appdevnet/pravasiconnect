//import AsyncStorage from '@react-native-community/async-storage';
import { AsyncStorage } from 'react-native'

export const storeLoggedIn = async function (value) {
    try {
      await AsyncStorage.setItem('isLoggedIn', JSON.stringify(value));
    } catch (e) {
      return false;
    }
    return true;
  };

export const isLoggedIn = async function () {
    let value = '';
    try {
      value = await AsyncStorage.getItem('isLoggedIn');
    } catch (e) {
      console.log('Error in asynstorage', e);
    }
    if (value == null) {
      value = 'false';
    }
    return value;
  };

  export const removeLoginDetails = async function () {
    try {
      await AsyncStorage.clear();
      await AsyncStorage.setItem('isLoggedIn', JSON.stringify('false'));
    } catch (e) {
      // remove error
      return false;
    }
    console.log('Removed Item');
    return true;
  };

  export const getLocation = async () =>{
    let value = ''
    try{
      value = await AsyncStorage.getItem('location')
    }
    catch{
      return false
    }
    return JSON.parse(value)
  }


  export const storeUserData = async (data) =>{
    let value = ''
    try{
      value = await AsyncStorage.setItem('userData',JSON.stringify(data))
    }
    catch{
      return false
    }
    return true
  }


  export const getUserData = async () =>{
    try{
      value = await AsyncStorage.getItem('userData')
    }
    catch{
      return false
    }
    return JSON.parse(value)
  }


  export const storeUserId = async (data) =>{
    let value = ''
    try{
      value = await AsyncStorage.setItem('userId',JSON.stringify(data))
    }
    catch{
      return false
    }
    return true
  }

  export const getUserId = async () =>{
    try{
      value = await AsyncStorage.getItem('userId')
    }
    catch{
      return false
    }
    return JSON.parse(value)
  }

  export const storeUserToken = async (data) =>{
    let value = ''
    try{
      value = await AsyncStorage.setItem('userToken',JSON.stringify(data))
    }
    catch{
      return false
    }
    return true
  }


  export const getUserToken = async () =>{
    try{
      value = await AsyncStorage.getItem('userToken')
    }
    catch{
      return false
    }
    return JSON.parse(value)
  }


