import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Index from './src/Routes';
import {Provider} from 'react-redux';
import reduxStore from './src/redux/strore'
import Loader from './src/components/Loader'
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Provider store={reduxStore}>
         
         <Index/>
         <Loader/>
        {/* </Loader> */}
      </Provider>
    );
  }
}

export default App;
